package stanchfield.scott.speech

enum class Direction {
    NORTH, SOUTH, EAST, WEST
}
