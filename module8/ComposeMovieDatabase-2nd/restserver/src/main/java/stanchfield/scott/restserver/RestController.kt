package stanchfield.scott.restserver

import jakarta.ws.rs.Consumes
import jakarta.ws.rs.DELETE
import jakarta.ws.rs.Produces

import jakarta.ws.rs.GET
import jakarta.ws.rs.POST
import jakarta.ws.rs.PUT
import jakarta.ws.rs.Path
import jakarta.ws.rs.PathParam
import jakarta.ws.rs.core.Context
import jakarta.ws.rs.core.MediaType
import jakarta.ws.rs.core.Response
import jakarta.ws.rs.core.UriInfo
import java.util.UUID

private val movies = mutableMapOf(
    "m1" to Movie("m1", "The Transporter", "Jason Statham kicks a guy in the face"),
    "m2" to Movie("m2", "Transporter 2", "Jason Statham kicks a bunch of guys in the face"),
    "m3" to Movie("m3", "Hobbs and Shaw", "Cars, Explosions and Stuff"),
    "m4" to Movie("m4", "Jumanji", "The Rock smolders")
)

private val notFoundMovie = Movie("-", "NOT FOUND", "NOT FOUND")

@Path("movie")
class RestController {
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    fun getMovies() =
        movies.values.sortedBy { it.title }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    fun getMovie(@PathParam("id") id: String) =
        movies[id]?.let { Response.status(Response.Status.OK).entity(it).build() } ?:
        Response.status(Response.Status.NOT_FOUND).entity(notFoundMovie).build()

    @PUT
    @Path("{id}")
    @Produces(MediaType.TEXT_PLAIN)
    @Consumes(MediaType.APPLICATION_JSON)
    fun updateMovie(@PathParam("id") id: String, movie: Movie): Response {
        movies[id] = movie
        return Response.status(Response.Status.OK).entity(1).build()
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    fun createMovie(movie: Movie): Response {
        val id = UUID.randomUUID().toString()
        val newMovie = movie.copy(id = id)
        movies[id] = newMovie
        return Response.status(Response.Status.CREATED).entity(newMovie).build()
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    fun deleteMovie(@PathParam("id") id: String) =
        if (movies[id] == null) {
            Response.status(Response.Status.NOT_FOUND).entity(0).build()
        } else {
            movies.remove(id)
            Response.status(Response.Status.OK).entity(1).build()
        }
}