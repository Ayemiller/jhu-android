package stanchfield.scott.common

import android.util.Log
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.gestures.detectTapGestures
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.ColumnScope
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.RowScope
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CornerSize
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Card
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.input.key.Key
import androidx.compose.ui.input.key.key
import androidx.compose.ui.input.key.onKeyEvent
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.dp
import stanchfield.scott.composemoviedatabase.R

interface HasId {
    val id: String
}

class Selection(
    val multiSelectMode: Boolean = false,
    val selections: Set<String> = emptySet()
) {
    private fun Set<String>.toggle(item: String) =
        if (item in this) {
            this - item
        } else {
            this + item
        }

    fun multiToggle(id: String) =
        if (multiSelectMode) {
            val newSelections = selections.toggle(id)
            Log.d("!!!multiToggle", newSelections.toString())
            Selection(newSelections.isNotEmpty(), newSelections)
        } else {
            Log.d("!!!multiToggle", setOf(id).toString())
            Selection(true, setOf(id))
        }

    fun toggle(id: String): Selection {
        val newSelections =
            if (multiSelectMode) {
                selections.toggle(id)
            } else {
                setOf(id)
            }
        Log.d("!!!toggle", newSelections.toString())
        return Selection(multiSelectMode && newSelections.isNotEmpty(), newSelections)
    }
}

@Composable
fun <T: HasId> BasicList(
    items: List<T>,
    getCardText: T.() -> String,
    @DrawableRes iconId: Int,
    @StringRes iconDescription: Int,
    selection: Selection,
    onSelectionChanged: (Selection) -> Unit
) {
    LazyColumn {
        items(
            items = items,
            key = { it.id } // ensures equivalent items in new/old lists are matched up properly
        ) { item ->
            BasicListCard(
                selection = selection,
                itemId = item.id,
                itemText = item.getCardText(),
                iconId = iconId,
                iconDescription = iconDescription,
                onSelectionChanged = onSelectionChanged
            )
        }
    }
}

@Composable
fun BasicListCard(
    selection: Selection,
    itemId: String,
    itemText: String,
    @DrawableRes iconId: Int,
    @StringRes iconDescription: Int,
    onSelectionChanged: (Selection) -> Unit
) {
    Card(
        elevation = 2.dp,
        backgroundColor =
        if (selection.multiSelectMode && itemId in selection.selections) {
            MaterialTheme.colors.primary
        } else {
            MaterialTheme.colors.surface
        },
        shape = RoundedCornerShape(corner = CornerSize(16.dp)),
        modifier = Modifier
            .padding(8.dp)
            .fillMaxWidth()
            .pointerInput(selection) {
                detectTapGestures(
                    onLongPress = {
                        onSelectionChanged(selection.multiToggle(itemId))
                    },
                    onTap = {
                        onSelectionChanged(selection.toggle(itemId))
                    }
                )
            }
    ) {
        Row(verticalAlignment = Alignment.CenterVertically) {
            Image(
                painter = painterResource(id = iconId),
                colorFilter =
                if (selection.multiSelectMode && itemId in selection.selections) {
                    ColorFilter.tint(MaterialTheme.colors.onPrimary)
                } else {
                    ColorFilter.tint(MaterialTheme.colors.onSurface)
                },
                contentDescription = stringResource(id = iconDescription),
                modifier = Modifier
                    .padding(8.dp)
                    .size(48.dp)
                    .clickable {
                        onSelectionChanged(selection.multiToggle(itemId))
                    }
            )
            Text(
                text = itemText,
                color =
                if (selection.multiSelectMode && itemId in selection.selections) {
                    MaterialTheme.colors.onPrimary
                } else {
                    MaterialTheme.colors.onSurface
                }
            )
        }
    }
}

@Composable
fun ScrollingColumn(content: @Composable ColumnScope.() -> Unit) =
    Column(modifier = Modifier.verticalScroll(rememberScrollState()), content = content)

@Composable
fun Label(
    @StringRes stringId: Int
)  = CommonText(text = stringResource(stringId), style = MaterialTheme.typography.h6, indent = false)

@Composable
fun Display(
    text: String?
) = CommonText(text = text, style = MaterialTheme.typography.h5, indent = true)

@Composable
fun CommonText(
    text: String?,
    style: TextStyle,
    indent: Boolean
) {
    Text(
        text = text ?: "",
        style = style,
        modifier = Modifier
            .fillMaxWidth()
            .padding(start = if (indent) 16.dp else 8.dp, end = 8.dp, top = 8.dp, bottom = 8.dp)
    )
}

@ExperimentalComposeUiApi
@Composable
fun CommonTextField(
    value: String?,
    @StringRes label: Int,
    onBack: () -> Unit,
    onValueChange: (String) -> Unit
) =
    TextField(
        value = value ?: "",
        label = { Text(text = stringResource(label)) },
        onValueChange = onValueChange,
        modifier = Modifier
            .padding(8.dp)
            .fillMaxWidth()
            // workaround for bug: https://issuetracker.google.com/issues/192433071
            // for the moment, we'll just force it to consume the Back key in onKeyEvent
            //    and explicitly pop the stack in the navigator
            // a fix for the bug has been submitted but per the comments it sounds like it won't make the 1.0.0 release
            // NOTE: the "Back" key reference is currently experimental API and could change
            .onKeyEvent {
                if (it.key == Key.Back) {
                    onBack()
                    true
                } else {
                    false
                }
            }
    )

@Composable
fun String?.valueOr(@StringRes resourceId: Int) =
    if (isNullOrEmpty()) {
        stringResource(id = resourceId)
    } else {
        this
    }

@Composable
fun SelectionTopBar(
    @StringRes title: Int,
    selection: Selection,
    onSelectionChanged: (Selection) -> Unit,
    multiSelectionActions: @Composable RowScope.() -> Unit,
    singleSelectionActions: @Composable RowScope.() -> Unit
) {
    if (selection.multiSelectMode) {
        TopAppBar(
            navigationIcon = {
                IconButton(onClick = {
                    onSelectionChanged(Selection())
                }) {
                    Icon(Icons.Filled.ArrowBack, stringResource(id = R.string.cancel_multi_select))
                }
            },
            title = { Text(selection.selections.size.toString()) },
            actions = multiSelectionActions
        )
    } else {
        TopAppBar(
            title = { Text(stringResource(id = title)) },
            actions = singleSelectionActions
        )
    }
}
