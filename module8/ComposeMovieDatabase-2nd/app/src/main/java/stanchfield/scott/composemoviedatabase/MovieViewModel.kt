package stanchfield.scott.composemoviedatabase

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import stanchfield.scott.common.Selection
import stanchfield.scott.composemoviedatabase.data.Movie

class MovieViewModel(application: Application) : AndroidViewModel(application) {
    private val movieApiService = MovieApiService.create()

    // return Flow<List<Movie>> from the view model (which gets them from the database
    // exposed as functions to be consistent with the functions defined below that
    private val _allMovies = MutableStateFlow<List<Movie>>(emptyList())
    val allMovies: Flow<List<Movie>> = _allMovies

    private fun updateMovies() {
        viewModelScope.launch(Dispatchers.IO) {
            _allMovies.value =
                try {
                    movieApiService.getAllMovies()
                } catch (e: Throwable) {
                    Log.e("!!!ERROR", "Error updating movies", e)
                    emptyList()
                }
        }
    }

    init {
        updateMovies()
    }

    private val selection0 = MutableStateFlow(Selection())
    val selection: Flow<Selection> = selection0
    val message = MutableStateFlow<String?>(null)

    suspend fun deleteMovie(id: String) = withContext(Dispatchers.IO) {
        movieApiService.deleteMovie(id)
        updateMovies()
    }
    suspend fun insert(movie: Movie) = withContext(Dispatchers.IO) {
        movieApiService.createMovie(movie)
        updateMovies()
    }

    suspend fun deleteSelectedMovies() = withContext(Dispatchers.IO) {
        val numMovies = selection0.value.selections.size
        selection0.value.selections.forEach {
            movieApiService.deleteMovie(it)
        }
        selection0.value = Selection()
        message.value = "$numMovies deleted"
        delay(2000)
        message.value = null
    }
    suspend fun createMovie() = withContext(Dispatchers.IO) {
        Movie().apply {
            movieApiService.createMovie(this)
        }
    }
    suspend fun onMovieChanged(movie: Movie) = withContext(Dispatchers.IO) {
        movieApiService.updateMovie(movie.id, movie)
        selection0.value = Selection(false, setOf(movie.id))
        updateMovies()
    }

    suspend fun getMovie(id: String?) =
        id?.let {
            withContext(Dispatchers.IO) {
                movieApiService.getMovie(it)
            }
        }

    fun updateSelection(selection: Selection) {
        selection0.value = selection
    }
}