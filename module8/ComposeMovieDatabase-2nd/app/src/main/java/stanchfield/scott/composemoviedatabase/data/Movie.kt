package stanchfield.scott.composemoviedatabase.data

import stanchfield.scott.common.HasId
import java.util.UUID

data class Movie(
    override var id: String = UUID.randomUUID().toString(),
    var title : String = "",
    var description : String = ""

// toString
// equals
// hashCode
// copy
// component1, component2...
): HasId