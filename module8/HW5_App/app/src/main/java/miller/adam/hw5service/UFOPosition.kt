package miller.adam.hw5service

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

/* Portions borrowed from EN605.686 sample projects. Apache 2.0 license */
@Parcelize
data class UFOPosition(
    var ship: Int,
    var lat: Double,
    var lon: Double,
) : Parcelable