package miller.adam.hw5client

import android.annotation.SuppressLint
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Surface
import androidx.compose.runtime.*
import androidx.core.content.ContextCompat
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.libraries.maps.CameraUpdateFactory
import com.google.android.libraries.maps.GoogleMap
import com.google.android.libraries.maps.model.BitmapDescriptor
import com.google.android.libraries.maps.model.LatLng
import com.google.android.libraries.maps.model.LatLngBounds
import com.google.android.libraries.maps.model.Marker
import com.google.maps.android.ktx.addMarker
import com.google.maps.android.ktx.addPolyline
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import miller.adam.hw5client.theme.HW5_AppTheme
import miller.adam.hw5service.AlienService
import miller.adam.hw5service.AlienServiceReporter
import miller.adam.hw5service.UFOPosition

/* Portions borrowed from EN605.686 sample projects. Apache 2.0 license */
class MainActivity : ComponentActivity() {
    private val _ufoPositions = MutableStateFlow<List<UFOPosition>>(emptyList())
    private val ufoPositions: Flow<List<UFOPosition>> = _ufoPositions

    private val _positionMap: Map<Int, List<Marker>> = mutableMapOf()

    private lateinit var googleMap: GoogleMap
    private lateinit var ufoIcon: BitmapDescriptor
    private val reporter = object : AlienServiceReporter.Stub() {
        override fun report(positions: MutableList<UFOPosition>) {
            _ufoPositions.value = positions
        }
    }

    private var savedInstanceState: Bundle? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.savedInstanceState = savedInstanceState

        GoogleApiAvailability.getInstance().makeGooglePlayServicesAvailable(this)
            .addOnSuccessListener {
                startLocationAndMap(ufoPositions = ufoPositions, positionMap = _positionMap)
            }.addOnFailureListener(this) {
                Toast.makeText(
                    this,
                    "Google Play services required (or upgrade required)",
                    Toast.LENGTH_SHORT
                ).show()
                finish()
            }
    }

    @SuppressLint("MissingPermission")
    private fun startLocationAndMap(
        ufoPositions: Flow<List<UFOPosition>>,
        positionMap: Map<Int, List<Marker>>
    ) {

        setContent {
            val ufoPos by ufoPositions.collectAsState(initial = emptyList())
            var posMap by remember { mutableStateOf(positionMap) }
            posMap = positionMap
            val scope = rememberCoroutineScope()
            //remove ufo markers that weren't updated in this report
            removeUfoMarkers(
                ufoReport = ufoPos,
                positionMap = posMap as MutableMap<Int, List<Marker>>
            )

            // add a marker for each new ship
            ufoPos.forEach {
                addUfoMarker(
                    ufo = it,
                    positionMap = posMap as MutableMap<Int, List<Marker>>
                )
            }

            HW5_AppTheme {
                Surface(color = MaterialTheme.colors.background) {
                    Scaffold(
                        topBar = { },
                        content = {
                            ComposeMap(
                                savedInstanceState = savedInstanceState,
                                startService = this::startService
                            ) { googleMap ->
                                this.googleMap = googleMap

                                ufoIcon = loadBitmapDescriptor(R.drawable.ic_ufo_flying)
                                run {
                                    binder?.reset()

                                    // update the camera bounds
                                    var latLngBounds: LatLngBounds? = null
                                    fun LatLng.addToBounds() {
                                        latLngBounds =
                                            latLngBounds?.including(this) ?: LatLngBounds(
                                                this,
                                                this
                                            )
                                    }
                                    // add the initial bounds specified in the assignment description
                                    LatLng(38.9073, -77.0365).addToBounds()
                                    for ((_, v) in positionMap) {
                                        v.forEach {
                                            it.position.addToBounds()
                                        }
                                    }
                                    val padding =
                                        resources.getDimension(R.dimen.bounds_padding)
                                    googleMap.animateCamera(
                                        CameraUpdateFactory.newLatLngBounds(
                                            latLngBounds,
                                            padding.toInt()
                                        )
                                    )
                                }
                            }
                        }
                    )
                }

            }
        }
    }


    private fun removeUfoMarkers(
        ufoReport: List<UFOPosition>,
        positionMap: Map<Int, List<Marker>>
    ) {
        // remove marker for ships that are not in the current report
        val shipsInReport: List<Int> = ufoReport.map { it.ship }
        val ufosNotInReport = positionMap.filterKeys { !shipsInReport.contains(it) }
        ufosNotInReport.forEach { (k, _) ->
            positionMap[k]?.last()?.remove()
        }
    }

    private fun Context.addUfoMarker(
        ufo: UFOPosition,
        positionMap: MutableMap<Int, List<Marker>>
    ) {
        if (positionMap.containsKey(ufo.ship)) {
            // make a line from the last position for the ship to the new position
            val lastPos = positionMap[ufo.ship]?.last()
            val polylineColor = ContextCompat.getColor(this, R.color.purple_700)
            googleMap.addPolyline {
                color(polylineColor)
                add(lastPos!!.position, LatLng(ufo.lat, ufo.lon))
            }

            // remove old marker if it's showing
            lastPos?.remove()

            //add new marker
            val marker = addShipMarker(ufo)

            // add to positionMap list
            val newList = positionMap[ufo.ship]?.toMutableList()
            newList!!.add(marker!!)
            positionMap[ufo.ship] = newList

        } else {
            // first sighting of this ship, add initial marker
            val marker = addShipMarker(ufo)
            positionMap[ufo.ship] = mutableListOf(marker!!)
        }

    }

    private fun addShipMarker(ufo: UFOPosition) = googleMap.addMarker {
        position(LatLng(ufo.lat, ufo.lon))
        anchor(0.5f, 0.5f)
        icon(ufoIcon)
        title(ufo.ship.toString())
        snippet("Lat/Lng: ${ufo.lat}, ${ufo.lon}")
    }

    private fun startService() {
        val intent = Intent().apply {
            // Need to specify a different application's ID here
            setClassName("miller.adam.hw5service", "miller.adam.hw5service.AlienServiceImpl")
        }
        bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE)
    }

    override fun onStop() {
        binder?.remove(reporter)
        unbindService(serviceConnection)
        super.onStop()
    }

    private var binder: AlienService? = null

    private val serviceConnection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            binder = AlienService.Stub.asInterface(service).apply {
                add(reporter)
            }
        }

        override fun onServiceDisconnected(name: ComponentName?) {
            binder = null
        }

    }
}
