package stanchfield.scott.composemoviedatabase

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.PUT
import retrofit2.http.Path
import stanchfield.scott.composemoviedatabase.data.Movie

const val BASE_URL = "http://10.0.2.2:8080" // host computer for emulator

interface MovieApiService {
    @GET("movie")
    suspend fun getAllMovies(): List<Movie>

    @GET("movie/{id}")
    suspend fun getMovie(@Path("id") id: String): Movie // movie or dummy movie to get around retrofit suspend bug

    @PUT("movie/{id}")
    suspend fun updateMovie(@Path("id") id: String, @Body movie: Movie): Int // number updated

    @POST("movie")
    suspend fun createMovie(@Body movie: Movie): Movie

    @DELETE("movie/{id}")
    suspend fun deleteMovie(@Path("id") id: String): Int // number deleted

    companion object {
        fun create() =
            Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL)
                .build()
                .create(MovieApiService::class.java)
    }
}