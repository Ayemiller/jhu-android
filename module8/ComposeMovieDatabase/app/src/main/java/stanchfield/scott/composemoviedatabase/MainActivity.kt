package stanchfield.scott.composemoviedatabase

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.Column
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material.icons.filled.Edit
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.res.stringResource
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch
import stanchfield.scott.common.BasicList
import stanchfield.scott.common.CommonTextField
import stanchfield.scott.common.Display
import stanchfield.scott.common.Label
import stanchfield.scott.common.ScrollingColumn
import stanchfield.scott.common.Selection
import stanchfield.scott.common.SelectionTopBar
import stanchfield.scott.common.valueOr
import stanchfield.scott.composemoviedatabase.data.Movie
import stanchfield.scott.composemoviedatabase.ui.theme.ComposeMovieDatabaseTheme

class MainActivity : ComponentActivity() {
    private val viewModel by viewModels<MovieViewModel>()

    @ExperimentalCoroutinesApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // at this level, only view model interaction
        setContent {
            val scope = rememberCoroutineScope()

            Ui(
                getMovie = viewModel::getMovie,
                allMoviesFlow = viewModel.allMovies,
                selectionFlow = viewModel.selection,
                messageFlow = viewModel.message,
                onSelectionChanged = viewModel::updateSelection,
                onDeleteSelectedMovies = {
                    scope.launch(Dispatchers.IO) {
                        viewModel.deleteSelectedMovies()
                        // TODO really should NOT be cancelable...
                    }
                },
                onMovieChanged = {
                    scope.launch {
                        viewModel.onMovieChanged(it)
                    }
                },
                createMovie = viewModel::createMovie
            )
        }
    }
}

private const val MovieDisplay = "movie_display/{id}"
private const val MovieEdit = "movie_edit?id={id}"
private const val MovieList = "movie_list"
private fun toMovieDisplay(id: String) = "movie_display/$id"
private fun toMovieEdit(id: String? = null) = id?.let { "movie_edit?id=$it" } ?: "movie_edit"

@Composable
fun Ui(
    allMoviesFlow: Flow<List<Movie>>,
    selectionFlow: Flow<Selection>,
    messageFlow: Flow<String?>,
    onSelectionChanged: (Selection) -> Unit,
    onDeleteSelectedMovies: () -> Unit,
    onMovieChanged: (Movie) -> Unit,
    createMovie: suspend () -> Movie,
    getMovie: suspend (String?) -> Movie?
) {
    // at this level, only navigation

    val allMovies by allMoviesFlow.collectAsState(initial = emptyList())
    val selection by selectionFlow.collectAsState(initial = Selection())
    val message by messageFlow.collectAsState(initial = null)

    val navController = rememberNavController() // added for nav component

    ComposeMovieDatabaseTheme {
        Surface(color = MaterialTheme.colors.background) {
            NavHost(  // added for nav component
                navController = navController,
                startDestination = MovieList
            ) {
                composable(MovieList) {
                    MovieListScreen(
                        message = message,
                        movies = allMovies,
                        selection = selection,
                        onSelectionChanged = {
                            onSelectionChanged(it)
                            if (!it.multiSelectMode && it.selections.isNotEmpty()) {
                                val id = it.selections.first()
                                navController.navigate(toMovieDisplay(id))
                            }
                        },
                        onCreateMovie = { navController.navigate(toMovieEdit()) },
                        onDeleteSelectedMovies = onDeleteSelectedMovies
                    )
                }
                composable(MovieDisplay) {
                    val id = it.arguments?.getString("id")
                    MovieDisplayScreen(
                        movieId = id,
                        getMovie = getMovie,
                        onEditMovie = { navController.navigate(toMovieEdit(id)) }
                    )
                }
                composable(MovieEdit) {
                    val id = it.arguments?.getString("id")
                    MovieEditScreen(
                        movieId = id,
                        getMovie = getMovie,
                        createMovie = createMovie,
                        onMovieChanged = onMovieChanged
                    )
                }
            }
        }
    }
}

@Composable
fun MovieListScreen(
    message: String?,
    movies: List<Movie>,
    selection: Selection,
    onSelectionChanged: (Selection) -> Unit,
    onCreateMovie: () -> Unit,
    onDeleteSelectedMovies: () -> Unit
) {
    Scaffold(
        topBar = {
            SelectionTopBar(
                title = R.string.movies,
                selection = selection,
                onSelectionChanged = onSelectionChanged,
                multiSelectionActions = { DeleteMovieAction(onDeleteSelectedMovies = onDeleteSelectedMovies) },
                singleSelectionActions = { CreateMovieAction(onCreateMovie = onCreateMovie) }
            )
        },
        content = {
            Column {
                message?.let { Text("Message: $it") }
                BasicList(
                    items = movies,
                    getCardText = { title },
                    iconId = R.drawable.ic_baseline_movie_24,
                    iconDescription = R.string.movie_icon,
                    selection = selection,
                    onSelectionChanged = onSelectionChanged
                )
            }
        }
    )
}

@Composable
fun MovieDisplayScreen(
    movieId: String?,
    getMovie: suspend (String?) -> Movie?,
    onEditMovie: () -> Unit
) {
    var movie by remember { mutableStateOf<Movie?>(null) }
    LaunchedEffect(movieId) {
        movie = getMovie(movieId)
    }

    Scaffold(
        topBar = {
            TopAppBar(
                title = { Text(movie?.title ?: stringResource(id = R.string.nothing_selected)) },
                actions = {
                    movie?.let {
                        EditMovieAction(onEditMovie = onEditMovie)
                    }
                }
            )
        },
        content = {
            ScrollingColumn {
                Label(R.string.title)
                Display(text = movie?.title)
                Label(R.string.description)
                Display(text = movie?.description)
            }
        }
    )
}

@Composable
fun MovieEditScreen(
    movieId: String?,
    createMovie: suspend () -> Movie,
    getMovie: suspend (String?) -> Movie?,
    onMovieChanged: (Movie) -> Unit
) {
    var movie by remember { mutableStateOf<Movie?>(null) }
    var title by remember(movie) { mutableStateOf(movie?.title) }
    var description by remember(movie) { mutableStateOf(movie?.description) }

    LaunchedEffect(movieId) {
        movie = movieId?.let { getMovie(it) } ?: createMovie()
    }

    Scaffold(
        topBar = {
            TopAppBar(title = { Text(title.valueOr(resourceId = R.string.no_title)) })
        },
        content = {
            ScrollingColumn {
                CommonTextField(value = title, label = R.string.title) {
                    movie?.let { movie ->
                        title = it
                        onMovieChanged(Movie(id = movie.id, title = title ?: "", description = description ?: ""))
                    }
                }
                CommonTextField(value = description, label = R.string.description) {
                    movie?.let { movie ->
                        description = it
                        onMovieChanged(Movie(id = movie.id, title = title ?: "", description = description ?: ""))
                    }
                }
            }
        }
    )
}

@Composable
fun EditMovieAction(
    onEditMovie: () -> Unit
) {
    IconButton(onClick = onEditMovie) {
        Icon(Icons.Filled.Edit, contentDescription = stringResource(id = R.string.edit_movie))
    }
}

@Composable
fun CreateMovieAction(
    onCreateMovie: () -> Unit
) {
    IconButton(onClick = onCreateMovie) {
        Icon(Icons.Filled.Add, contentDescription = stringResource(id = R.string.create_movie))
    }
}

@Composable
fun DeleteMovieAction(
    onDeleteSelectedMovies: () -> Unit
) {
    IconButton(onClick = onDeleteSelectedMovies) {
        Icon(Icons.Filled.Delete, contentDescription = stringResource(id = R.string.delete_selected_movies))
    }
}
