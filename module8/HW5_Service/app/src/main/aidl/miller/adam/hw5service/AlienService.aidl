package miller.adam.hw5service;
import miller.adam.hw5service.AlienServiceReporter;

// AIDL that defines our API with the remote service
// Here we allow a "reset" request as well as adding/removing a callback
/* Portions borrowed from EN605.686 sample project. Apache 2.0 license */
interface AlienService {
	void reset();
	void add(AlienServiceReporter reporter);
	void remove(AlienServiceReporter reporter);
}