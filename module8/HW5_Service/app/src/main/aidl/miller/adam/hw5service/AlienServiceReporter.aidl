package miller.adam.hw5service;
import miller.adam.hw5service.UFOPosition;
/* Portions borrowed from EN605.686 sample project. Apache 2.0 license */
// An example callback interface that sends a list of ufo positions and the ufo ship numbers back to the requester
interface AlienServiceReporter {
	void report(in List<UFOPosition> ufos);
}