package miller.adam.hw5service;

/* Portions borrowed from EN605.686 sample project. Apache 2.0 license */
// AIDL that declares (to the AIDL compiler) that we have a class named
// miller.adam.hw5.UFOPosition that is Parcelable

parcelable UFOPosition;