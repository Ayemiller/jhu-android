package miller.adam.hw5service

import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.util.Log
import kotlinx.coroutines.*
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Url

const val BASE_URL = "http://www.javadude.com/aliens/" // host computer for emulator
const val JSON = ".json"
var filename: String? = null

/* Portions borrowed from EN605.686 sample project. Apache 2.0 license */
interface AlienJsonService {
    @GET
    suspend fun getAllPositions(@Url url: String): Response<List<UFOPosition>>

    companion object {
        fun createService(): AlienJsonService =
            Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL)
                .build()
                .create(AlienJsonService::class.java)
    }
}

class AlienServiceImpl : Service() {
    private val scope = CoroutineScope(Dispatchers.IO)
    private var job: Job? = null
    private val reporters = mutableListOf<AlienServiceReporter>()
    private var n = 1
    private lateinit var alienApiService: AlienJsonService

    private val binder = object : AlienService.Stub() {
        override fun reset() {
            n = 1
        }

        override fun add(reporter: AlienServiceReporter) {
            reporters.add(reporter)
            if (job == null) {
                job = scope.launch {
                    alienApiService = AlienJsonService.createService()
                    var counter = 0
                    do {
                        counter++
                        filename = counter.toString() + JSON
                        val response =
                            alienApiService.getAllPositions(BASE_URL + filename)
                        when (response.code()) {
                            200 -> {
                                reporters.forEach { it.report(response.body()!!) }
                            }
                            else -> {
                                Log.e(
                                    "ERROR",
                                    "Unexpected error returned from alien REST server"
                                )
                            }
                        }
                        delay(1000)
                    } while (response.code() != 404)
                }
            }
        }

        override fun remove(reporter: AlienServiceReporter) {
            reporters.remove(reporter)
        }
    }

    override fun onBind(intent: Intent): IBinder {
        return binder
    }

    override fun onDestroy() {
        job?.cancel()
        job = null
        super.onDestroy()
    }
}