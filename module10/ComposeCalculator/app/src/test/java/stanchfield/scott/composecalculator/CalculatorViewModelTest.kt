package stanchfield.scott.composecalculator

import app.cash.turbine.test
import kotlinx.coroutines.runBlocking
import org.hamcrest.CoreMatchers.anyOf
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import kotlin.time.ExperimentalTime
/* Portions borrowed from EN605.686 sample projects. Apache 2.0 license */
class CalculatorViewModelTest {

    lateinit var viewModel: CalculatorViewModel

    @ExperimentalTime
    @Before
    fun setup() {
        viewModel = CalculatorViewModel()
    }

    @ExperimentalTime
    @Test
    fun test_view_model_initial_state() {
        // the initial value should always be blank
        runBlocking {
            viewModel.display.test {
                assertEquals("", awaitItem())
                cancelAndIgnoreRemainingEvents()
            }
        }
    }

    @ExperimentalTime
    @Test
    fun test_view_model_clear() {
        viewModel.addDigit(3)
        viewModel.addDigit(4)
        viewModel.addDigit(5)
        viewModel.addDigit(6)
        viewModel.addDigit(7)
        viewModel.clear()
        runBlocking {
            viewModel.display.test {
                assertEquals("0.0", awaitItem())
                cancelAndIgnoreRemainingEvents()
            }
        }
    }

    @ExperimentalTime
    @Test
    fun test_view_model_addition() {

        viewModel.addDigit(3)
        viewModel.plus()
        viewModel.addDigit(4)
        viewModel.equals()
        runBlocking {
            viewModel.display.test {
                assertEquals("7.0", awaitItem())
                cancelAndIgnoreRemainingEvents()
            }
        }
    }

    @ExperimentalTime
    @Test
    fun test_view_model_division() {
        viewModel.addDigit(8)
        viewModel.divide()
        viewModel.addDigit(4)
        viewModel.equals()
        runBlocking {
            viewModel.display.test {
                assertEquals("2.0", awaitItem())
                cancelAndIgnoreRemainingEvents()
            }
        }
    }

    @ExperimentalTime
    @Test
    fun test_view_model_multiplication() {
        viewModel.addDigit(8)
        viewModel.times()
        viewModel.addDigit(4)
        viewModel.equals()
        runBlocking {
            viewModel.display.test {
                assertEquals("32.0", awaitItem())
                cancelAndIgnoreRemainingEvents()
            }
        }
    }

    /**
     * Test currently fails due to [CalculatorLogic.minus] performing addition instead of subtraction.
     */
    @ExperimentalTime
    @Test
    fun test_view_model_subtraction() {
        viewModel.addDigit(3)
        viewModel.minus()
        viewModel.addDigit(4)
        viewModel.equals()
        runBlocking {
            viewModel.display.test {
                assertEquals("-1.0", awaitItem())
                cancelAndIgnoreRemainingEvents()
            }
        }
    }

    /**
     * Test currently fails because [CalculatorLogic.decimal] doesn't set startNewNumber with a
     * decimal entry before a leading digit is entered. Test expects either 0.4 or .4, but it is
     * instead 4
     */
    @ExperimentalTime
    @Test
    fun test_view_model_broken_decimal() {
        viewModel.decimal()
        viewModel.addDigit(4)
        runBlocking {
            viewModel.display.test {
                val displayVal = awaitItem()
                assertThat(displayVal, anyOf(equalTo("0.4"), equalTo(".4")))
                cancelAndIgnoreRemainingEvents()
            }
        }
    }

    @ExperimentalTime
    @Test
    fun test_view_model_decimal() {
        viewModel.addDigit(3)
        viewModel.decimal()
        viewModel.addDigit(4)
        runBlocking {
            viewModel.display.test {
                assertEquals("3.4", awaitItem())
                cancelAndIgnoreRemainingEvents()
            }
        }
    }

    @ExperimentalTime
    @Test
    fun test_view_model_negate() {
        viewModel.addDigit(38)
        viewModel.negate()
        runBlocking {
            viewModel.display.test {
                assertEquals("-38", awaitItem())
                cancelAndIgnoreRemainingEvents()
            }
        }
    }

    @ExperimentalTime
    @Test
    fun test_view_model_remove_one_digit() {
        viewModel.addDigit(3)
        viewModel.removeDigit()
        runBlocking {
            viewModel.display.test {
                assertEquals("0.0", awaitItem())
                cancelAndIgnoreRemainingEvents()
            }
        }
    }

    /**
     * Test currently fails due to [CalculatorLogic.removeDigit] removing two digits instead of one
     */
    @ExperimentalTime
    @Test
    fun test_view_model_remove_two_digits() {
        viewModel.addDigit(4)
        viewModel.addDigit(5)
        viewModel.removeDigit()
        runBlocking {
            viewModel.display.test {
                assertEquals("4", awaitItem())
                cancelAndIgnoreRemainingEvents()
            }
        }
        viewModel.removeDigit()
        runBlocking {
            viewModel.display.test {
                assertEquals("0.0", awaitItem())
                cancelAndIgnoreRemainingEvents()
            }
        }
    }

    /**
     * Test currently fails due to [CalculatorLogic.removeDigit] removing two digits instead of one
     */
    @ExperimentalTime
    @Test
    fun test_view_model_remove_three_digits() {
        viewModel.addDigit(7)
        viewModel.addDigit(8)
        viewModel.addDigit(9)
        viewModel.removeDigit()
        runBlocking {
            viewModel.display.test {
                assertEquals("78", awaitItem())
                cancelAndIgnoreRemainingEvents()
            }
        }
        viewModel.removeDigit()
        runBlocking {
            viewModel.display.test {
                assertEquals("7", awaitItem())
                cancelAndIgnoreRemainingEvents()
            }
        }
        viewModel.removeDigit()
        runBlocking {
            viewModel.display.test {
                assertEquals("0.0", awaitItem())
                cancelAndIgnoreRemainingEvents()
            }
        }
    }

    @ExperimentalTime
    @Test
    fun test_view_model_clear_entry() {
        viewModel.addDigit(3)
        viewModel.clearEntry()
        runBlocking {
            viewModel.display.test {
                assertEquals("0.0", awaitItem())
                cancelAndIgnoreRemainingEvents()
            }
        }
    }

    @ExperimentalTime
    @Test
    fun test_view_model_clear_entry_complex() {
        viewModel.addDigit(3)
        viewModel.plus()
        viewModel.addDigit(4)
        viewModel.clearEntry()
        viewModel.addDigit(5)
        viewModel.equals()
        runBlocking {
            viewModel.display.test {
                assertEquals("8.0", awaitItem())
                cancelAndIgnoreRemainingEvents()
            }
        }
    }

    /**
     * This test fails due to [CalculatorLogic.minus] performing addition as well as [CalculatorViewModel.removeDigit] chopping off two values instead of one.
     */
    @ExperimentalTime
    @Test
    fun test_view_model_complex_entry() {
        viewModel.addDigit(3)
        viewModel.times()
        viewModel.addDigit(4)
        viewModel.divide()  // running total = 12
        viewModel.addDigit(6)
        viewModel.plus() // running total = 2
        viewModel.addDigit(17)
        viewModel.minus() // running total = 19
        viewModel.addDigit(7)
        viewModel.plus() // running total = 12
        viewModel.addDigit(4)
        viewModel.decimal()
        viewModel.addDigit(5)
        viewModel.addDigit(6)
        viewModel.removeDigit()

        viewModel.equals() // 12 + 4.5 = 16.5

        runBlocking {
            viewModel.display.test {
                assertEquals("16.0", awaitItem())
                cancelAndIgnoreRemainingEvents()
            }
        }
    }

}