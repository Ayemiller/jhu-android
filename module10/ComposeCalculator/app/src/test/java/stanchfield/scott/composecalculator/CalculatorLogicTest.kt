package stanchfield.scott.composecalculator

import org.hamcrest.CoreMatchers.anyOf
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

/* Portions borrowed from EN605.686 sample projects. Apache 2.0 license */
class CalculatorLogicTest {

    private lateinit var calcLogic: CalculatorLogic
    private var displayVal = String()

    @Before
    fun setup() {
        displayVal = String()
        calcLogic = CalculatorLogic(onDisplayChanged = { displayVal = it })
    }

    @Test
    fun `test negate logic`() {
        calcLogic.addDigit(2)
        calcLogic.negate()
        assertEquals("-2", displayVal)

        calcLogic.addDigit(4)
        assertEquals("-24", displayVal)

        calcLogic.negate()
        assertEquals("24", displayVal)
    }

    @Test
    fun `test plus logic`() {
        calcLogic.addDigit(3)
        calcLogic.plus()
        calcLogic.addDigit(6)
        calcLogic.equals()
        assertEquals("9.0", displayVal)

        calcLogic.plus()
        calcLogic.addDigit(34)
        calcLogic.equals()
        assertEquals("43.0", displayVal)
    }

    /**
     * Test currently fails due to [CalculatorLogic.minus] performing addition instead of subtraction.
     */
    @Test
    fun test_minus_logic() {
        calcLogic.addDigit(3)
        calcLogic.minus()
        calcLogic.addDigit(6)
        calcLogic.equals()
        assertEquals("-3.0", displayVal)
    }

    /**
     * Test currently fails due to [CalculatorLogic.minus] performing addition instead of subtraction.
     */
    @Test
    fun test_complex_minus_logic() {
        calcLogic.addDigit(4)
        calcLogic.negate()
        calcLogic.minus()
        calcLogic.addDigit(2)
        calcLogic.addDigit(5)
        calcLogic.negate()
        calcLogic.equals()
        assertEquals("21.0", displayVal)
    }

    @Test
    fun test_times_logic() {
        calcLogic.addDigit(2)
        calcLogic.times()
        calcLogic.addDigit(4)
        calcLogic.equals()
        assertEquals("8.0", displayVal)

        calcLogic.times()
        calcLogic.addDigit(-25)
        calcLogic.equals()
        assertEquals("-200.0", displayVal)
    }

    @Test
    fun test_divide_logic() {
        calcLogic.addDigit(9)
        calcLogic.divide()
        calcLogic.addDigit(3)
        calcLogic.equals()
        assertEquals("3.0", displayVal)

        calcLogic.divide()
        calcLogic.addDigit(-1)
        calcLogic.equals()
        assertEquals("-3.0", displayVal)
    }

    /**
     * Test currently fails because [CalculatorLogic.removeDigit] removes two digits instead of one
     */
    @Test
    fun test_remove_digit_logic() {
        calcLogic.addDigit(9)
        calcLogic.addDigit(4)
        calcLogic.addDigit(6)
        calcLogic.removeDigit()
        assertEquals("94", displayVal)

        calcLogic.removeDigit()
        assertEquals("9", displayVal)

        calcLogic.removeDigit()
        assertEquals("0.0", displayVal)
    }

    /**
     * Test currently fails because [CalculatorLogic.decimal] doesn't set startNewNumber with a
     * decimal entry before a leading digit is entered. Test expects either 0.6 or .6 but it is
     * instead 6
     */
    @Test
    fun test_broken_decimal_functionality() {
        calcLogic.decimal()
        calcLogic.addDigit(6)
        assertThat(displayVal, anyOf(equalTo("0.6"), equalTo(".6")))
    }

    @Test
    fun test_decimal_logic() {
        calcLogic.addDigit(1)
        calcLogic.addDigit(9)
        calcLogic.decimal()
        assertEquals("19.", displayVal)

        // make sure just one decimal is added
        calcLogic.decimal()
        assertEquals("19.", displayVal)

        calcLogic.addDigit(3)
        assertEquals("19.3", displayVal)

    }

    @Test
    fun test_clear_entry_logic() {
        calcLogic.addDigit(7)
        calcLogic.clearEntry()
        assertEquals("0.0", displayVal)

        calcLogic.addDigit(1)
        calcLogic.addDigit(2)
        calcLogic.plus()
        calcLogic.addDigit(5)
        calcLogic.clearEntry()
        assertEquals("0.0", displayVal)
        calcLogic.addDigit(6)
        calcLogic.equals()
        assertEquals("18.0", displayVal)

    }

    @Test
    fun test_clear_logic() {
        calcLogic.addDigit(8)
        calcLogic.addDigit(2)
        calcLogic.plus()
        calcLogic.addDigit(3)
        calcLogic.clear()
        assertEquals("0.0", displayVal)
        calcLogic.addDigit(5)
        calcLogic.equals()
        assertEquals("5.0", displayVal)
        calcLogic.clear()
        assertEquals("0.0", displayVal)
    }

}