@file:Suppress("IllegalIdentifier")

package stanchfield.scott.composecalculator

import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.test.assertTextEquals
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.test.performClick
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.hamcrest.CoreMatchers.anyOf
import org.hamcrest.CoreMatchers.equalTo
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import stanchfield.scott.composecalculator.ui.theme.ComposeCalculatorTheme
/* Portions borrowed from EN605.686 sample projects. Apache 2.0 license */
@RunWith(AndroidJUnit4::class)
class CalculatorInstrumentedTest {
    @get:Rule
    val composeRule = createComposeRule()

    @Before
    fun setup() {
        composeRule.setContent {
            val viewModel = CalculatorViewModel()
            val display by viewModel.display.collectAsState(initial = "")
            //var text by remember { mutableStateOf("0") }
            ComposeCalculatorTheme {
                Calculator(
                    display = display,
                    viewModel = viewModel
                )
            }
        }
    }

    /**
     * Test currently fails due to [CalculatorLogic.minus] performing addition instead of subtraction.
     */
    @Test
    fun test_compose_ui_add_subtract_multiply() {
        with(composeRule) {

            "7+8-3*4=".forEach { onNodeWithTag(it.toString()).performClick() }
            onNodeWithTag("display").assertTextEquals("48.0")
        }
    }


    @Test
    fun test_compose_ui_divide_add_multiply() {
        with(composeRule) {

            "36/4+5*6=".forEach { onNodeWithTag(it.toString()).performClick() }
            onNodeWithTag("display").assertTextEquals("84.0")
            onNodeWithTag("c").performClick()
            onNodeWithTag("display").assertTextEquals("0.0")

            // -80 / 80 + 3 * 4 = 8.0
            listOf("8", "0", "+/-","/","7","ce").forEach { onNodeWithTag(it).performClick() }
            onNodeWithTag("display").assertTextEquals("0.0")
            ("80+3*4=").forEach { onNodeWithTag(it.toString()).performClick() }
            onNodeWithTag("display").assertTextEquals("8.0")


            // -45 * 1.5 = -67.5
            listOf("4","5","+/-", "*", "1",".","5","=").forEach { onNodeWithTag(it).performClick() }
            onNodeWithTag("display").assertTextEquals("-67.5")
        }
    }

    /**
     * Test currently fails due the [CalculatorButton] for 2 being assigned the value 1 in [MainActivity]
     * Therefore, 9*4/2+1 is not 19, but instead, 37.
     */
    @Test
    fun test_compose_ui_multiply_divide_add() {
        with(composeRule) {
            "9*4/2+1=".forEach { onNodeWithTag(it.toString()).performClick() }
            onNodeWithTag("display").assertTextEquals("19.0")
        }
    }

    /**
     * Test currently fails because [CalculatorLogic.decimal] doesn't set startNewNumber with a
     * decimal entry before a leading digit is entered. Test allows for valid values to be either
     * 0.8 or .8
     */
    @Test
    fun test_compose_ui_broken_decimal() {
        with(composeRule) {
            ".8".forEach { onNodeWithTag(it.toString()).performClick() }
            onNodeWithTag("display").assertTextEquals(anyOf(equalTo("0.8"),equalTo(".8")).toString())
        }
    }

}