package stanchfield.scott.composetest

import androidx.room.Room
import app.cash.turbine.test
import io.mockk.every
import io.mockk.justRun
import io.mockk.mockk
import io.mockk.verify
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.runBlocking
import org.junit.Test

import org.junit.Assert.*
import org.junit.Before
import org.junit.BeforeClass
import org.junit.runner.RunWith
import kotlin.time.ExperimentalTime

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    companion object {
        @JvmStatic
        @BeforeClass
        fun overallSetup() {
            // runs once
        }
    }

    @Before
    fun setup() {
        // setup common stuff
    }
    @Test
    fun `addition is correct`() {
        assertEquals(5, 2 + 2)
        val timeTheThingHappens = 1000
    }

    @ExperimentalTime
    @Test
    fun `test view model`() {
//        val repository = MockRepository()
        val repository = mockk<MovieRepository>()
        every { repository.getMovies() } returns flow {
            emit(listOf(Movie("m1", "Die Hard"), Movie("m2", "Get Smart")))
        }
        justRun { repository.saveMovie(any()) }

        val viewModel = MovieViewModel(repository)

        runBlocking {
            viewModel.movies.test {
                verify { repository.getMovies() }
                assertEquals(listOf(Movie("m1", "Die Hard"), Movie("m2", "Get Smart")), awaitItem())
                cancelAndIgnoreRemainingEvents()
            }
        }
//        assertTrue(repository.getMoviesCalled)
        val toSave = Movie("m3", "Romancing the Stone")
        viewModel.saveMovie(toSave)
        verify { repository.saveMovie(toSave) }
//        assertTrue(repository.saveMovieCalled)
    }
}