package stanchfield.scott.composegraphics

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.withContext

class GraphViewModel: ViewModel() {
    private val shapes0 = MutableStateFlow<List<Shape>>(emptyList())
    val shapes: StateFlow<List<Shape>> get() = shapes0

    private val highlightedShapeType0 = MutableStateFlow<ShapeType?>(null)
    val highlightedShapeType: StateFlow<ShapeType?> get() = highlightedShapeType0

    private val lines0 = MutableStateFlow<List<Line>>(emptyList())
    val lines: StateFlow<List<Line>> get() = lines0

    private val selectedTool0 = MutableStateFlow<Tool>(Square)
    val selectedTool: StateFlow<Tool> get() = selectedTool0

    private var draggingShape: Shape? = null
    private var draggingOffsetX = -1f
    private var draggingOffsetY = -1f
    private var lineBeingCreated: Line? = null

    fun add(shape: Shape) { shapes0.value = shapes0.value + shape }
    fun add(line: Line) { lines0.value = lines0.value + line }
    fun select(tool: Tool) { selectedTool0.value = tool }

    fun startDragging(shape: Shape, offsetX: Float, offsetY: Float) {
        draggingShape = shape
        draggingOffsetX = shape.centerX - offsetX
        draggingOffsetY = shape.centerY - offsetY
    }

    suspend fun highlightShapes() = withContext(Dispatchers.Default) {
        draggingShape?.let { shape ->
            stopDragging()
            repeat(3) {
                try {
                    highlightedShapeType0.emit(shape.type)
                    delay(200)
                } finally {
                    highlightedShapeType0.emit(null)
                    if (it != 2) {
                        delay(200)
                    }
                }
            }
        }
    }
    fun stopDragging() {
        draggingShape = null
    }
    fun drag(x: Float, y: Float) {
        draggingShape?.let { shape ->
            val newShape = shape.copy(centerX = draggingOffsetX + x, centerY = draggingOffsetY + y)
            draggingShape = newShape
            shapes0.value = shapes0.value - shape + newShape
        }
    }

    fun createLine(startShape: Shape) {
        Line(startShape.id).apply {
            lines0.value = lines0.value + this
            lineBeingCreated = this
        }
    }
    fun cancelLine() {
        lineBeingCreated?.let {
            lines0.value = lines0.value - it
        }
        lineBeingCreated = null
    }
    fun finishLine(endShape: Shape) {
        lineBeingCreated?.let {
            val newLine = it.copy(end2 = endShape.id)
            lines0.value = lines0.value - it + newLine
            lineBeingCreated = null
        }
    }
}