package stanchfield.scott.composegraphics

import java.util.UUID

sealed class Tool
object DrawLine: Tool()
object Select: Tool()
abstract class ShapeType: Tool()
object Circle: ShapeType()
object Square: ShapeType()
object Triangle: ShapeType()

data class Shape(
    val type: ShapeType, val centerX: Float, val centerY: Float,
    val id: String = UUID.randomUUID().toString()
)
data class Line(val end1: String, val end2: String? = null)
