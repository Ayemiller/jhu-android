package stanchfield.scott.composegraphics

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.gestures.awaitFirstDown
import androidx.compose.foundation.gestures.awaitTouchSlopOrCancellation
import androidx.compose.foundation.gestures.detectTapGestures
import androidx.compose.foundation.gestures.drag
import androidx.compose.foundation.gestures.forEachGesture
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.size
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Path
import androidx.compose.ui.graphics.PathEffect
import androidx.compose.ui.graphics.drawscope.DrawScope
import androidx.compose.ui.graphics.drawscope.DrawStyle
import androidx.compose.ui.graphics.drawscope.Fill
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.input.pointer.PointerInputChange
import androidx.compose.ui.input.pointer.PointerInputScope
import androidx.compose.ui.input.pointer.consumeAllChanges
import androidx.compose.ui.input.pointer.consumePositionChange
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.input.pointer.positionChange
import androidx.compose.ui.input.pointer.positionChangeConsumed
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import stanchfield.scott.composegraphics.ui.theme.ComposeGraphicsTheme

class MainActivity : ComponentActivity() {
    private val viewModel by viewModels<GraphViewModel>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            val scope = rememberCoroutineScope()

            ComposeGraphicsTheme {
                with(LocalDensity.current) {
                    val shapes by viewModel.shapes.collectAsState(emptyList())
                    val lines by viewModel.lines.collectAsState(emptyList())
                    val selectedTool by viewModel.selectedTool.collectAsState(Square)
                    val highlightedShapeType by viewModel.highlightedShapeType.collectAsState(null)

                    val strokeWidth = 3.dp.toPx()
                    val buttonSize = 48.dp.toPx()
                    val shapeSize = 36.dp.toPx()
                    val dashSize = 6.dp.toPx()
                    val gapSize = 3.dp.toPx()
                    val cornerRadius = 3.dp.toPx()

                    val outline = Stroke(strokeWidth)
                    val dashedOutline =
                        Stroke(
                            width = strokeWidth,
                            pathEffect = PathEffect.chainPathEffect(
                                PathEffect.dashPathEffect(floatArrayOf(dashSize, gapSize)),
                                PathEffect.cornerPathEffect(cornerRadius)
                            )
                        )

                    val squareSize = Size(shapeSize, shapeSize)
                    val halfShapeSize = shapeSize / 2
                    val halfButtonSize = buttonSize / 2
                    val shapeOffsetAmount = halfButtonSize - halfShapeSize
                    val shapeTopLeft = Offset(shapeOffsetAmount, shapeOffsetAmount)
                    val buttonCenter = Offset(halfButtonSize, halfButtonSize)

                    var finger by remember { mutableStateOf<Offset?>(null) }

                    var blinker: Job? = null

                    // A surface container using the 'background' color from the theme
                    Surface(color = MaterialTheme.colors.background) {
                        Scaffold(
                            topBar = {
                                GraphToolbar(
                                    shapeTopLeft = shapeTopLeft,
                                    buttonCenter = buttonCenter,
                                    squareSize = squareSize,
                                    halfShapeSize = halfShapeSize,
                                    strokeWidth = strokeWidth,
                                    outline = outline,
                                    dashedOutline = dashedOutline,
                                    onSelect = viewModel::select
                                )
                            },
                            content = {
                                GraphArea(
                                    selectedTool = selectedTool,
                                    lines = lines,
                                    shapes = shapes,
                                    finger = finger,
                                    highlightedShapeType = highlightedShapeType,
                                    squareSize = squareSize,
                                    halfShapeSize = halfShapeSize,
                                    outline = outline,
                                    onAddShape = viewModel::add,
                                    onStartDragging = { shape, offset ->
                                        viewModel.startDragging(shape, offset.x, offset.y)
                                    },
                                    onUpWithoutDragging = {
                                        blinker?.cancel()
                                        blinker = scope.launch(Dispatchers.Default) {
                                            viewModel.highlightShapes()
                                            blinker = null
                                        }
                                    },
                                    onStopDragging = viewModel::stopDragging,
                                    onDrag = {
                                        finger = it
                                        viewModel.drag(it.x, it.y)
                                    },
                                    onCancelLine = viewModel::cancelLine,
                                    onCreateLine = { shape, offset ->
                                        finger = offset
                                        viewModel.createLine(shape)
                                    },
                                    onFinishLine = viewModel::finishLine
                                )
                            }
                        )
                    }
                }
            }
        }
    }
}

@Composable
fun GraphToolbar(
    shapeTopLeft: Offset,
    buttonCenter: Offset,
    squareSize: Size,
    halfShapeSize: Float,
    strokeWidth: Float,
    outline: DrawStyle,
    dashedOutline: DrawStyle,
    onSelect: (Tool) -> Unit
) {

    TopAppBar(
        title = { Text(stringResource(id = R.string.title)) },
        actions = {
            IconButton(onClick = { onSelect(Square) }) {
                Canvas(modifier = Modifier.size(48.dp)) {
                    drawSquareShape(
                        topLeft = shapeTopLeft,
                        size = squareSize,
                        outlineStyle = outline,
                        outlineColor = Color.Black
                    )
                }
            }
            IconButton(onClick = { onSelect(Circle) }) {
                Canvas(modifier = Modifier.size(48.dp)) {
                    drawCircleShape(
                        center = buttonCenter,
                        radius = halfShapeSize,
                        outlineStyle = outline,
                        outlineColor = Color.Black
                    )
                }
            }
            IconButton(onClick = { onSelect(Triangle) }) {
                Canvas(modifier = Modifier.size(48.dp)) {
                    drawTriangleShape(
                        center = buttonCenter,
                        halfSize = halfShapeSize,
                        outlineStyle = outline,
                        outlineColor = Color.Black
                    )
                }
            }
            IconButton(onClick = { onSelect(DrawLine) }) {
                Canvas(modifier = Modifier.size(48.dp)) {
                    drawLineShape(
                        center = buttonCenter,
                        halfSize = halfShapeSize,
                        strokeWidth = strokeWidth
                    )
                }
            }
            IconButton(onClick = { onSelect(Select) }) {
                Canvas(modifier = Modifier.size(48.dp)) {
                    drawSquareShape(
                        topLeft = shapeTopLeft,
                        size = squareSize,
                        outlineStyle = dashedOutline,
                        outlineColor = Color.Black,
                        fill = false
                    )
                }
            }
        }
    )
}

fun Shape.contains(offset: Offset, halfShapeSize: Float) =
    offset.x >= centerX - halfShapeSize &&
    offset.x <= centerX + halfShapeSize &&
    offset.y >= centerY - halfShapeSize &&
    offset.y <= centerY + halfShapeSize

fun List<Shape>.findAt(offset: Offset, halfShapeSize: Float) =
    reversed().find { it.contains(offset, halfShapeSize) }

@Composable
fun GraphArea(
    selectedTool: Tool,
    lines: List<Line>,
    shapes: List<Shape>,
    finger: Offset?,
    squareSize: Size,
    halfShapeSize: Float,
    outline: DrawStyle,
    highlightedShapeType: ShapeType?,
    onAddShape: (Shape) -> Unit,
    onStartDragging: (Shape, Offset) -> Unit,
    onUpWithoutDragging: () -> Unit,
    onStopDragging: () -> Unit,
    onDrag: (Offset) -> Unit,
    onCreateLine: (Shape, Offset) -> Unit,
    onCancelLine: () -> Unit,
    onFinishLine: (Shape) -> Unit
) {
    var shapesToUse by remember { mutableStateOf(shapes) }
    shapesToUse = shapes

    Canvas(
        modifier = Modifier
            .fillMaxSize()
            .pointerInput(selectedTool) {
                when (selectedTool) {
                    Select -> {
                        detectGraphGestures(
                            onDown = { finger ->
                                shapesToUse
                                    .findAt(finger, halfShapeSize)
                                    ?.let {
                                        onStartDragging(it, finger)
                                    }
                            },
                            onUpWithoutDragging = onUpWithoutDragging,
                            onDragEnd = { onStopDragging() },
                            onDragCancel = onStopDragging,
                            onDrag = { change, _ ->
                                change.consumeAllChanges()
                                onDrag(change.position)
                            }
                        )
                    }
                    DrawLine -> {
                        detectGraphGestures(
                            onDown = { finger ->
                                shapesToUse
                                    .findAt(finger, halfShapeSize)
                                    ?.let { shape ->
                                        onCreateLine(shape, finger)
                                    }
                            },
                            onUpWithoutDragging = onCancelLine,
                            onDragCancel = onCancelLine,
                            onDragEnd = { finger ->
                                finger.let { offset ->
                                    shapesToUse
                                        .findAt(offset, halfShapeSize)
                                        ?.let { shape ->
                                            onFinishLine(shape)
                                        } ?: onCancelLine()
                                }
                            },
                            onDrag = { change, _ ->
                                change.consumeAllChanges()
                                onDrag(change.position)
                            }
                        )
                    }
                    else -> {
                        detectTapGestures(
                            onTap = {
                                onAddShape(Shape(selectedTool as ShapeType, it.x, it.y))
                            }
                        )
                    }
                }
            }
    ) {
        lines.forEach { line ->
            val end1 = shapes.find { it.id == line.end1 } ?: throw IllegalStateException("Shape with id ${line.end1} not found")
            val end2 = line.end2?.let { end2 ->
                shapes.find { it.id == end2 } ?: throw IllegalStateException("Shape with id ${line.end1} not found")
            }

            drawLine(
                start = Offset(end1.centerX, end1.centerY),
                end = end2?.let { Offset(it.centerX, it.centerY) } ?: finger ?: throw IllegalStateException("Finger should not be able to be null here"),
                color = Color.DarkGray
            )
        }
        shapes.forEach {
            val outlineColor = if (it.type == highlightedShapeType) Color.Magenta else Color.Black
            when (it.type) {
                Circle -> drawCircleShape(center = Offset(it.centerX, it.centerY), radius = halfShapeSize, outlineStyle = outline, outlineColor = outlineColor)
                Square -> drawSquareShape(topLeft = Offset(it.centerX - halfShapeSize, it.centerY - halfShapeSize), size = squareSize, outlineStyle = outline, outlineColor = outlineColor)
                Triangle -> drawTriangleShape(center = Offset(it.centerX, it.centerY), halfSize = halfShapeSize, outlineStyle = outline, outlineColor = outlineColor)
            }
        }
    }
}

fun DrawScope.drawSquareShape(
    topLeft: Offset,
    size: Size,
    outlineStyle: DrawStyle,
    outlineColor: Color,
    fill: Boolean = true
) {
    if (fill) {
        drawRect(
            topLeft = topLeft,
            size = size,
            style = Fill,
            color = Color.Blue
        )
    }
    drawRect(
        topLeft = topLeft,
        size = size,
        style = outlineStyle,
        color = outlineColor
    )
}

fun DrawScope.drawCircleShape(
    center: Offset,
    radius: Float,
    outlineStyle: DrawStyle,
    outlineColor: Color
) {
    drawCircle(
        center = center,
        radius = radius,
        style = Fill,
        color = Color.Green
    )
    drawCircle(
        center = center,
        radius = radius,
        style = outlineStyle,
        color = outlineColor
    )
}

fun DrawScope.drawTriangleShape(
    center: Offset,
    halfSize: Float,
    outlineStyle: DrawStyle,
    outlineColor: Color
) {
    val path = Path().apply {
        moveTo(center.x, center.y - halfSize)
        lineTo(center.x + halfSize, center.y + halfSize)
        lineTo(center.x - halfSize, center.y + halfSize)
        close()
    }

    drawPath(
        path = path,
        style = Fill,
        color = Color.Red
    )
    drawPath(
        path = path,
        style = outlineStyle,
        color = outlineColor
    )
}

fun DrawScope.drawLineShape(
    center: Offset,
    halfSize: Float,
    strokeWidth: Float
) {
    drawLine(
        start = Offset(center.x - halfSize, center.y),
        end = Offset(center.x + halfSize, center.y),
        color = Color.DarkGray,
        strokeWidth = strokeWidth
    )
}

// This was copied from Compose's DragGestureDetector line 195, Compose version 1.0.0 rc01
// Apache 2.0 license
suspend fun PointerInputScope.detectGraphGestures(
    onDown: (Offset) -> Unit = { },
    onUpWithoutDragging: () -> Unit = { },
    onDragStart: (Offset) -> Unit = { },
    onDragEnd: (Offset) -> Unit = { },
    onDragCancel: () -> Unit = { },
    onDrag: (change: PointerInputChange, dragAmount: Offset) -> Unit
) {
    forEachGesture {
        awaitPointerEventScope {
            val down = awaitFirstDown(requireUnconsumed = false)
            onDown(down.position)
            var drag: PointerInputChange?
            var overSlop = Offset.Zero
            do {
                drag = awaitTouchSlopOrCancellation(down.id) { change, over ->
                    change.consumePositionChange()
                    overSlop = over
                }
            } while (drag != null && !drag.positionChangeConsumed())
            if (drag != null) {
                onDragStart.invoke(drag.position)
                onDrag(drag, overSlop)
                var lastOffset = drag.position
                if (
                    !drag(drag.id) {
                        lastOffset = it.position
                        onDrag(it, it.positionChange())
                        it.consumePositionChange()
                    }
                ) {
                    onDragCancel()
                } else {
                    onDragEnd(lastOffset)
                }
            } else {
                onUpWithoutDragging()
            }
        }
    }
}
