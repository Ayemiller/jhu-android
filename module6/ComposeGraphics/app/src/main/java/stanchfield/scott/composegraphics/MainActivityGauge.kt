package stanchfield.scott.composegraphics

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.gestures.detectTapGestures
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.drawscope.Fill
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.unit.dp
import stanchfield.scott.composegraphics.ui.theme.ComposeGraphicsTheme

class MainActivityGauge : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ComposeGraphicsTheme {
                var value by rememberSaveable { mutableStateOf(0.3f) }
                val outlineWidth = with(LocalDensity.current) {
                    3.dp.toPx()
                }

                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colors.background) {
                    Gauge(
                        fillColor = Color.Blue,
                        outlineColor = Color.Black,
                        outlineWidth = outlineWidth,
                        value = value,
                        onChangeValue = {
                            value += 0.2f
                            if (value > 1.0f) {
                                value = 0.1f
                            }
                        },
                        modifier = Modifier.fillMaxSize())
                }
            }
        }
    }
}

@Composable
fun Gauge(
    fillColor: Color,
    outlineColor: Color,
    outlineWidth: Float,
    value: Float,
    onChangeValue: () -> Unit,
    modifier: Modifier
) {
    Canvas(
        // pointerInput omits the ripple effect
        modifier = modifier.pointerInput(Unit) {
            detectTapGestures(
                onTap = {
                    onChangeValue()
                }
            )
        }
        //
        // or if you want the ripple effect...
        //        clickable {
        //            onChangeValue()
        //        }
    ) {
        drawRect(
            topLeft = Offset(0f, size.height - size.height*value),
            size = Size(size.width, size.height*value),
            color = fillColor,
            style = Fill
        )
        drawRect(
            topLeft = Offset(0f, size.height - size.height*value),
            size = Size(size.width, size.height*value),
            color = outlineColor,
            style = Stroke(outlineWidth)
        )
    }
}