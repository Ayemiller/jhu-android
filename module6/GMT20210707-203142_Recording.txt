01:03:46	Jake Rhodes:	can you explain the difference in that notation? I don't quite understand that Shape.(Offset)
01:06:12	Jake Rhodes:	can I ask how you'd handle having a second parameter there?
01:06:17	Jake Rhodes:	Like one more than "it"
01:07:00	Jake Rhodes:	Oh, easy peasy!
01:13:17	Jake Rhodes:	What does the _ in "change, _" do for us?
01:13:59	Jake Rhodes:	Sorry, the Underscore
01:30:57	Ed Gleeck:	it's a feature :)
01:34:05	Ed Gleeck:	would adding some logging help?
02:21:49	Jake Rhodes:	That actually made pretty good sense! It's just like having a pointer that's pointing to changing data!
02:31:27	Alek:	isn't the construction/destruction super expensive? suppose you had 1,000 moving shapes on the screen at the same time. would you then consider mutable?
02:34:09	Alek:	yes ty
