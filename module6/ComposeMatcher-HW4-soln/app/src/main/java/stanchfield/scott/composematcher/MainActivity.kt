package stanchfield.scott.composematcher

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.clickable
import androidx.compose.foundation.gestures.awaitFirstDown
import androidx.compose.foundation.gestures.awaitTouchSlopOrCancellation
import androidx.compose.foundation.gestures.detectDragGestures
import androidx.compose.foundation.gestures.drag
import androidx.compose.foundation.gestures.forEachGesture
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.State
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Path
import androidx.compose.ui.graphics.drawscope.DrawScope
import androidx.compose.ui.graphics.drawscope.DrawStyle
import androidx.compose.ui.graphics.drawscope.Fill
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.input.pointer.PointerInputChange
import androidx.compose.ui.input.pointer.PointerInputScope
import androidx.compose.ui.input.pointer.consumeAllChanges
import androidx.compose.ui.input.pointer.consumePositionChange
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.input.pointer.positionChange
import androidx.compose.ui.input.pointer.positionChangeConsumed
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.flowWithLifecycle
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch
import stanchfield.scott.composematcher.ui.theme.ComposeMatcherTheme
import kotlin.math.min

class MainActivity : ComponentActivity() {
    private val viewModel by viewModels<MatcherViewModel>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // keep track of the current blinker job so we can cancel it if the user clicks something else
        var blinker: Job? = null

        setContent {
            ComposeMatcherTheme {
                val scope = rememberCoroutineScope()

                Surface(color = MaterialTheme.colors.background) {
                    Scaffold(
                        // just have a title at the top
                        topBar = {
                            TopAppBar(
                                title = { Text(stringResource(id = R.string.matcher)) }
                            )
                        },

                        // include the game grid
                        content = {
                            Grid(
                                shapesFlow = viewModel.shapes,
                                matchesFlow = viewModel.matches,
                                highlightedShapeFlow = viewModel.highlightedShape,
                                scoreFlow = viewModel.score,
                                onDown = viewModel::selectShape,
                                onDragStart = viewModel::startDrag,
                                onStopDragging = {
                                    scope.launch {
                                        viewModel.stopDrag(it)
                                    }
                                },
                                onBlink= {
                                    blinker?.cancel() // cancel a blinker job if in progress
                                    blinker = scope.launch {
                                        viewModel.highlightSelectedShape()
                                    }
                                },
                                modifier = Modifier.fillMaxSize(),
                            )

                            // start the game by shuffling the board and matching
                            scope.launch {
                                viewModel.shuffle()
                                viewModel.performMatching()
                            }
                        }
                    )
                }
            }
        }
    }
}

/**
 * Helper function that implements the new recommended "safe" way to collect flows
 * flowWithLifecycle sets up a lifecycle observer that will start/stop collection only when the
 * lifecycle is AT LEAST in the indicated state (if we use STARTED for example, it will be active when in the
 * STARTED or RESUMED state, but not the CREATED state)
 */
@Composable
fun <T : R, R> Flow<T>.collectAsStateWithLifecycle(
    initial: R,
    state: Lifecycle.State
): State<R> {
    val lifecycleOwner = LocalLifecycleOwner.current
    val flowLifecycleAware = remember(this, lifecycleOwner) {
        this.flowWithLifecycle(lifecycleOwner.lifecycle, state)
    }
    return flowLifecycleAware.collectAsState(initial)
}

/**
 * A helper for state lifecycle that calls it with the STARTED state. Not really necessary, but can make the code
 *   feel a wee bit cleaner...
 */
@Composable
fun <T : R, R> Flow<T>.collectAsStateWhenStarted(
    initial: R
) = collectAsStateWithLifecycle(initial = initial, state = Lifecycle.State.STARTED)

/**
 * The main grid display - This is a Canvas that draws everything on the screen
 */
@Composable
fun Grid(
    shapesFlow: Flow<List<Shape>>,
    matchesFlow: Flow<Set<Int>>,
    highlightedShapeFlow: Flow<Shape?>,
    scoreFlow: Flow<Int>,
    onDown: (FoundShape) -> Unit,
    onStopDragging: (FoundShape?) -> Unit,
    onDragStart: () -> Unit,
    onBlink: () -> Unit,
    modifier: Modifier
) {
    // collect the flows that were passed in (only when STARTED/RESUMED)
    val score by scoreFlow.collectAsStateWhenStarted(0)
    val shapes by shapesFlow.collectAsStateWhenStarted(initial = emptyList())
    val matches by matchesFlow.collectAsStateWhenStarted(initial = emptySet())
    val highlightedShape by highlightedShapeFlow.collectAsStateWhenStarted(initial = null)

    // local state
    var paused by remember { mutableStateOf(false) } // are we paused?
    var finger by remember { mutableStateOf<Offset?>(null) } // current location of user's finger
    var dragShape by remember { mutableStateOf<Shape?>(null) } // the shape being dragged (if any)
    var draggedAtLeastALittle by remember { mutableStateOf(false) } // has the user dragged past touch-slop?
    var cellSize by remember { mutableStateOf(0f) }
        // the size of a cell (computed inside Canvas but needed by the Canvas' pointer input

    // if paused, just display "paused" on the screen and allow the user to click it to unpause
    if (paused) {
        Text(
            text = stringResource(id = R.string.paused),
            style = MaterialTheme.typography.h1,
            textAlign = TextAlign.Center,
            modifier = Modifier
                .fillMaxSize()
                .clickable { paused = false }
        )
        return
    }

    // determine how thick 2dp is in pixels
    val strokeWidth = with(LocalDensity.current) {
        2.dp.toPx()
    }

    // helper to clear drag information consistently (I like nested functions...)
    fun clearDrag() {
        finger = null
        dragShape = null
        draggedAtLeastALittle = false
    }

    Column(modifier = modifier) {
        Text(
            text = "${stringResource(id = R.string.score)} $score",
            style = MaterialTheme.typography.h3,
            textAlign = TextAlign.Center,
            modifier = Modifier
                .padding(8.dp)
                .fillMaxWidth()
        )
        Canvas(
            modifier = Modifier
                .fillMaxSize()

                // deal with user interaction
                .pointerInput(Unit) {
                    detectGestures(
                        // determine where the user clicked, finding a shape, then tell the caller
                        onDown = { offset ->
                            finger = offset
                            shapes
                                .findAt(offset, cellSize)
                                ?.let {
                                    onDown(it)
                                    dragShape = it.shape
                                }
                        },

                        // user clicked but didn't drag - it's a blink or pause, depending on if they were dragging
                        //   then clear our local drag data
                        onUpWithoutDrag = {
                            if (dragShape != null) {
                                onBlink()
                            } else {
                                paused = true
                            }
                            clearDrag()
                        },

                        // user dragged at least past touch-slop; tell the caller
                        onDragStart = {
                            onDragStart()
                            draggedAtLeastALittle = true
                        },

                        // user lifted their finger - see if there is a shape there and tell the caller
                        //   then clear our local drag data
                        onDragEnd = {
                            val stopShape = finger?.let { shapes.findAt(it, cellSize) }
                            onStopDragging(stopShape)
                            clearDrag()
                        },

                        // if user canceled drag, tell the caller and clear our local drag data
                        onDragCancel = {
                            onStopDragging(null)
                            clearDrag()
                        },

                        // as the user is dragging, change the tracked location
                        onDrag = { change, _ ->
                            change.consumeAllChanges()
                            finger = change.position
                        }
                    )
                }
        ) {
            // set up the cell size to a 10th of the smallest of width/height
            cellSize = min(size.width, size.height) / 10

            drawGrid(
                cellSize = cellSize,
                strokeWidth = strokeWidth
            )
            drawShapes(
                shapes = shapes,
                dragShape = if (draggedAtLeastALittle) dragShape else null,
                finger = finger,
                matches = matches,
                strokeWidth = strokeWidth,
                cellSize = cellSize,
                highlightedShape = highlightedShape
            )
        }
    }
}

/**
 * Draw shapes on a canvas
 */
fun DrawScope.drawShapes(
    shapes: List<Shape>,
    dragShape: Shape?,
    finger: Offset?,
    matches: Set<Int>,
    cellSize: Float,
    strokeWidth: Float,
    highlightedShape: Shape?
) {
    val shapeSize = cellSize * 0.8f
    val halfShapeSize = shapeSize / 2
    val quarterShapeSize = shapeSize / 4
    val outline = Stroke(width = strokeWidth)

    // draw the grid of shapes, highlighting if requested
    if (shapes.size == 64) {
        (1..8).forEach { row ->
            (1..8).forEach { column ->
                val shape = shapes[row, column]
                val shapeTopLeft = Offset(
                    x = column * cellSize + (cellSize - shapeSize) / 2,
                    y = row * cellSize + (cellSize - shapeSize) / 2
                )
                val outlineColor =
                    if (shape == highlightedShape || shapeIndex(row, column) in matches) Color.Magenta else Color.Black
                drawShape(
                    shape = shape,
                    shapeTopLeft = shapeTopLeft,
                    shapeSize = shapeSize,
                    halfShapeSize = halfShapeSize,
                    quarterShapeSize = quarterShapeSize,
                    outlineStyle = outline,
                    outlineColor = outlineColor
                )
            }
        }
    }

    // if we're dragging a shape, draw it
    //   we paint this AFTER the grid so the dragged shape will always appear on top
    dragShape?.let { shape ->
        finger?.let { finger ->
            drawShape(
                shape = shape,
                shapeTopLeft = finger - Offset(halfShapeSize, halfShapeSize),
                shapeSize = shapeSize,
                halfShapeSize = halfShapeSize,
                quarterShapeSize = quarterShapeSize,
                outlineStyle = outline,
                outlineColor = Color.Black
            )
        }
    }
}

/**
 * Draw an individual shape on the screen at the indicated location
 */
fun DrawScope.drawShape(
    shape: Shape,
    shapeTopLeft: Offset,
    shapeSize: Float,
    halfShapeSize: Float,
    quarterShapeSize: Float,
    outlineStyle: DrawStyle,
    outlineColor: Color
) {
    when (shape) {
        is Square -> drawSquare(
            topLeft = shapeTopLeft,
            shapeSize = shapeSize,
            outlineStyle = outlineStyle,
            outlineColor = outlineColor
        )
        is Circle -> drawCircle(
            center = shapeTopLeft + Offset(halfShapeSize, halfShapeSize),
            radius = halfShapeSize,
            outlineStyle = outlineStyle,
            outlineColor = outlineColor
        )
        is Cross -> drawCross(
            topLeft = shapeTopLeft,
            quarterShapeSize = quarterShapeSize,
            outlineStyle = outlineStyle,
            outlineColor = outlineColor
        )
        is Diamond -> drawDiamond(
            topLeft = shapeTopLeft,
            halfShapeSize = halfShapeSize,
            outlineStyle = outlineStyle,
            outlineColor = outlineColor
        )
        Empty -> { } // draw nothing
    }
}

/**
 * Draw the grid
 */
fun DrawScope.drawGrid(
    cellSize: Float,
    strokeWidth: Float
) {
    val end = cellSize * 9

    repeat(9) { n ->
        drawLine(
            start = Offset(cellSize, (n + 1) * cellSize),
            end = Offset(end, (n + 1) * cellSize),
            color = Color.DarkGray,
            strokeWidth = strokeWidth
        )
        drawLine(
            start = Offset((n + 1) * cellSize, cellSize),
            end = Offset((n + 1) * cellSize, end),
            color = Color.DarkGray,
            strokeWidth = strokeWidth
        )
    }
}

/**
 * Determine if the indicated location represents a gem
 */
fun List<Shape>.findAt(offset: Offset, cellSize: Float): FoundShape? {
    val row = offset.y.toInt() / cellSize.toInt()
    val column = offset.x.toInt() / cellSize.toInt()
    return if (row < 1 || row > 8 || column < 1 || column > 8)
        null
    else
        FoundShape(this[row, column], row, column)
}

fun DrawScope.drawSquare(
    topLeft: Offset,
    shapeSize: Float,
    outlineStyle: DrawStyle,
    outlineColor: Color
) {
    drawRect(
        topLeft = topLeft,
        size = Size(shapeSize, shapeSize),
        style = Fill,
        color = Color.Blue
    )
    drawRect(
        topLeft = topLeft,
        size = Size(shapeSize, shapeSize),
        style = outlineStyle,
        color = outlineColor
    )
}

fun DrawScope.drawCircle(
    center: Offset,
    radius: Float,
    outlineStyle: DrawStyle,
    outlineColor: Color
) {
    drawCircle(
        center = center,
        radius = radius,
        style = Fill,
        color = Color.Yellow
    )
    drawCircle(
        center = center,
        radius = radius,
        style = outlineStyle,
        color = outlineColor
    )
}

fun DrawScope.drawCross(
    topLeft: Offset,
    quarterShapeSize: Float,
    outlineStyle: DrawStyle,
    outlineColor: Color
) {
    val path = Path().apply {
        moveTo(topLeft.x + quarterShapeSize, topLeft.y)
        relativeLineTo(quarterShapeSize * 2, 0f)
        relativeLineTo(0f, quarterShapeSize)
        relativeLineTo(quarterShapeSize, 0f)
        relativeLineTo(0f, quarterShapeSize * 2)
        relativeLineTo(-quarterShapeSize, 0f)
        relativeLineTo(0f, quarterShapeSize)
        relativeLineTo(-quarterShapeSize * 2, 0f)
        relativeLineTo(0f, -quarterShapeSize)
        relativeLineTo(-quarterShapeSize, 0f)
        relativeLineTo(0f, -quarterShapeSize * 2)
        relativeLineTo(quarterShapeSize, 0f)
        close()
    }
    drawPath(
        path = path,
        style = Fill,
        color = Color.Green
    )
    drawPath(
        path = path,
        style = outlineStyle,
        color = outlineColor
    )
}

fun DrawScope.drawDiamond(
    topLeft: Offset,
    halfShapeSize: Float,
    outlineStyle: DrawStyle,
    outlineColor: Color
) {
    val path = Path().apply {
        moveTo(topLeft.x + halfShapeSize, topLeft.y)
        relativeLineTo(halfShapeSize, halfShapeSize)
        relativeLineTo(-halfShapeSize, halfShapeSize)
        relativeLineTo(-halfShapeSize, -halfShapeSize)
        close()
    }
    drawPath(
        path = path,
        style = Fill,
        color = Color.Red
    )
    drawPath(
        path = path,
        style = outlineStyle,
        color = outlineColor
    )
}

/**
 * An enhanced version of [PointerInputScope.detectDragGestures] that allows us to know exactly where the user
 *   clicked to start a tap/drag, and report if they lifted their finger without dragging.
 * The original code is licensed Apache 2:
 *   Copyright 2020 The Android Open Source Project
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
suspend fun PointerInputScope.detectGestures(
    onDown: (Offset) -> Unit = { }, // initial down press
    onUpWithoutDrag: () -> Unit = { }, // released without drag
    onDragStart: (Offset) -> Unit = { },
    onDragEnd: () -> Unit = { },
    onDragCancel: () -> Unit = { },
    onDrag: (change: PointerInputChange, dragAmount: Offset) -> Unit
) {
    forEachGesture {
        awaitPointerEventScope {
            val down = awaitFirstDown(requireUnconsumed = false)
            onDown(down.position)
            var drag: PointerInputChange?
            var overSlop = Offset.Zero
            do {
                drag = awaitTouchSlopOrCancellation(down.id) { change, over ->
                    change.consumePositionChange()
                    overSlop = over
                }
            } while (drag != null && !drag.positionChangeConsumed())
            if (drag != null) {
                onDragStart.invoke(drag.position)
                onDrag(drag, overSlop)
                if (
                    !drag(drag.id) {
                        onDrag(it, it.positionChange())
                        it.consumePositionChange()
                    }
                ) {
                    onDragCancel()
                } else {
                    onDragEnd()
                }
            } else {
                onUpWithoutDrag()
            }
        }
    }
}