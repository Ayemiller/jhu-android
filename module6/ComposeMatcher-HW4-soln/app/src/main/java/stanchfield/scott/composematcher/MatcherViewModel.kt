package stanchfield.scott.composematcher

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.withContext
import kotlin.math.abs
import kotlin.random.Random

sealed class Shape
object Square: Shape()
object Circle: Shape()
object Cross: Shape()
object Diamond: Shape()
object Empty: Shape()

/**
 * Tracks a shape that was located in the UI. The UI determines the row/column of the shape
 * and passes a [FoundShape] to the [MatcherViewModel] for processing. The [MatcherViewModel]
 * will hold onto the selected [FoundShape] so it knows where to return the selected shape in
 * case of no match, or which shapes to swap.
 */
data class FoundShape(
    val shape: Shape,
    val row: Int,
    val column: Int
)

/** delay between blinks - made it flash quickly */
private const val blinkDelayTime = 100L
/** delay between steps in the matching algorithm */
private const val stepDelayTime = 400L

/**
 * Calculate the List index of a gem based on its row and column
 */
fun shapeIndex(row: Int, column: Int) = (row-1)*8 + column - 1

/**
 * "Get" operator that allows us to use shapes[row, column] for a more natural
 * two-dimensional-array like access
 */
operator fun List<Shape>.get(row: Int, column: Int) = this[shapeIndex(row, column)]

/**
 * The View Model for our gem-matching game. We don't need an Android context inside here,
 * so we only extend ViewModel rather than extending AndroidViewModel
 */
class MatcherViewModel: ViewModel() {
    // the game score state
    private val score0 = MutableStateFlow(0)
    val score: Flow<Int> = score0

    // the gems on the board
    private val shapes0 = MutableStateFlow<List<Shape>>(emptyList())
    val shapes: Flow<List<Shape>> = shapes0

    // which gem list indexes matched so the UI can highlight matches
    private val matches0 = MutableStateFlow<Set<Int>>(emptySet())
    val matches: Flow<Set<Int>> = matches0

    // which shape should be blinked by the UI
    private val highlightedShape0 = MutableStateFlow<Shape?>(null)
    val highlightedShape: Flow<Shape?> = highlightedShape0

    // which shape was clicked/dragged
    private var selectedShape: FoundShape? = null

    /**
     * Randomize all pieces on the board and deduct 10 points from the score.
     * Run the matching algorithm to get to a state where there are no more matches
     */
    suspend fun shuffle() {
        shapes0.emit(List(64) { randomShape() })
        score0.emit(score0.value - 10)
        performMatching()
    }

    /**
     * Run a matching algorithm that
     *   - Sets board state to highlight all matches
     *   - Sets board state to empty all matches
     *   - Sets board state to shift down gems to fill in empty spots (but leave empty spots at top)
     *   - Sets board state to fill empty spots with random gems
     * The algorithm is repeated until there are no more matches
     */
    suspend fun performMatching(initialMatches: Set<Int> = emptySet()) = withContext(Dispatchers.Default) {
        var shapes = shapes0.value
        var matches = if (initialMatches.isNotEmpty()) initialMatches else shapes.matches
        while (matches.isNotEmpty()) {
            repeat(3) {
                try {
                    matches0.emit(matches)
                    delay(blinkDelayTime)
                } finally {
                    matches0.emit(emptySet())
                    delay(blinkDelayTime)
                }
            }
            shapes = shapes.removeMatches(matches)
            shapes0.emit(shapes)
            score0.emit(score0.value + matches.size)
            delay(stepDelayTime)
            shapes = shapes.shiftDown()
            shapes0.emit(shapes)
            delay(stepDelayTime)
            shapes = shapes.replaceEmptiesWithRandoms()
            shapes0.emit(shapes)
            matches = shapes.matches
        }
    }

    /**
     * Allow the user interface to tell us which piece the user clicked/dragged
     */
    fun selectShape(foundShape: FoundShape) {
        selectedShape = foundShape
    }

    /**
     * Helper function to determine if two pieces are directly next to each other,
     *   horizontally or vertically
     */
    private fun FoundShape.isNextTo(otherShape: FoundShape) =
        abs(row - otherShape.row) + abs(column - otherShape.column) == 1

    /**
     * Called by the UI to tell us when the user has stopped dragging a piece, and
     *   on which piece (if any) they lifted their finger
     */
    suspend fun stopDrag(stopShape: FoundShape?) = withContext(Dispatchers.Default) {
        // if the start AND end happened outside the grid, run the shuffle
        if (selectedShape == null && stopShape == null) {
            shuffle()

        } else {
            selectedShape?.let { selected -> // if the started outside the grid, we'll ignore the drag
                // if they started on a shape, blank it (we have it as "selected" now)
                selectedShape = null

                // if they stopped on a shape...
                stopShape?.let { stop ->
                    if (stop.isNextTo(selected)) {
                        // swap the shapes by modifying the board
                        val swapped =
                            shapes0.value
                                .replace(stop.row, stop.column, selected.shape)
                                .replace(selected.row, selected.column, stop.shape)

                        // determine if we have any matches
                        val matches = swapped.matches

                        // if so, update the board with the swapped state and run the matching algorithm
                        if (matches.isNotEmpty()) {
                            shapes0.emit(swapped)
                            performMatching(matches)

                        } else {
                            null // no matches - trigger putting the shape back
                        }
                    } else {
                        null // bad move - trigger putting the shape back
                    }

                // cleanup code to put the shape that was being moved back from whence it came
                } ?: run {
                    // just put the selected shape back
                    shapes0.emit(shapes0.value.replace(selected.row, selected.column, selected.shape))
                }
            }
        }
    }

    /**
     * Called by the UI to tell us that the user has actually started dragging the piece (they've dragged it
     *   past the touch-slop distance)
     * We remove the selected shape from the board state while it's being dragged
     */
    fun startDrag() {
        // remove the selected shape (if exists) from the grid while it's being dragged
        selectedShape?.let {
            shapes0.value = shapes0.value.replace(it.row, it.column, Empty)
        }
    }

    /**
     * Toggle highlight of the selected shape three times
     */
    suspend fun highlightSelectedShape() = withContext(Dispatchers.Default)  {
        selectedShape?.let { foundShape ->
            repeat(3) {
                try {
                    highlightedShape0.emit(foundShape.shape)
                    delay(blinkDelayTime)
                } finally {
                    // note the finally - we ALWAYS want to ensure the "off" is done in case the coroutine
                    //   is cancelled
                    highlightedShape0.emit(null)
                    if (it != 2) {
                        delay(blinkDelayTime)
                    }
                }
            }
            selectedShape = null
        } ?: throw IllegalStateException("Selected shape should be non-null when highlightSelectedShape is called")
    }

    /**
     * Helper function that converts the shape state into a new shape state with the indicated piece replaced
     */
    private fun List<Shape>.replace(row: Int, column: Int, shape: Shape): List<Shape> {
        val index = shapeIndex(row, column)
        return mapIndexed { n, existingShape -> if (n == index) shape else existingShape}
    }

    /**
     * "Set" operator that allows us to use shapes0[row, column] = shape for more natural
     *    two-dimensional-array-like access
     */
    private operator fun MutableList<Shape>.set(row: Int, column: Int, shape: Shape) {
        this[shapeIndex(row, column)] = shape
    }

    /**
     * Swaps the gems at two positions in a MUTABLE copy of the board.
     * Note that this is an unfortunate idiom in kotlin for swapping. It's the equivalent of
     *    val temp = this[row2, column2]
     *    this[row2, column2] = this[row1, column1]
     *    this[row1, column1] = temp
     *
     * It works, but it's crazy confusing unless you've really gotten used to kotlin. I only show it here
     *   because you're likely to see it somewhere... Otherwise, I'd recommend the above code
     *
     * Here's what's happening
     *    1. this[row2, column2].also {...} makes a copy of this[row2, column2], passing it into the lambda as "this"
     *         -- we're using that "this" as the temporary copy of the second shape
     *    2. That { this[row2, column2] = this[row1, column1] } lambda runs, doing exactly what it says
     *    3. The "also" returns its "this", which was that temp copy of the second shape
     *    4. this[row1, column1] is set to that temp, which was the old value of the second shape
     *
     * Yes, it works, but I don't recommend writing code like this. However, take a few moments to read the above
     *   carefully and if you have questions, let me know. You will see this at some point and I would rather you
     *   actually understand what it's doing than just say "oh, that's a kotlin idiom that swaps and I don't get it"...
     */
    private fun MutableList<Shape>.swapMutable(row1: Int, column1: Int, row2:Int, column2: Int) {
        this[row1, column1] = this[row2, column2].also { this[row2, column2] = this[row1, column1] }
    }

    /**
     * Get a list of indexes that represent gems that are in matches. There are other more optimal ways
     *   to do this, but this is good for an assignment...
     * Note that we temporarily use a mutable set to add matches to and convert it to an immutable list at the end
     */
    private val List<Shape>.matches: Set<Int>
        get() {
            val matches = mutableSetOf<Int>()
            (1..8).forEach { i ->
                (1..6).forEach { j ->
                    // check horizontal
                    val shape1Horizontal = this[i, j]
                    val shape2Horizontal = this[i, j + 1]
                    val shape3Horizontal = this[i, j + 2]
                    if (shape1Horizontal == shape2Horizontal && shape1Horizontal == shape3Horizontal) {
                        matches.add(shapeIndex(i, j))
                        matches.add(shapeIndex(i, j + 1))
                        matches.add(shapeIndex(i, j + 2))
                    }
                    // check vertical
                    val shape1Vertical = this[j, i]
                    val shape2Vertical = this[j+1, i]
                    val shape3Vertical = this[j+2, i]
                    if (shape1Vertical == shape2Vertical && shape1Vertical == shape3Vertical) {
                        matches.add(shapeIndex(j, i))
                        matches.add(shapeIndex(j+1, i))
                        matches.add(shapeIndex(j+2, i))
                    }
                }
            }
            return matches.toSet() // create an immutable set out of it so the caller can't cast it and abuse it
        }

    /**
     * Shift down pieces to fill in empty spots.
     * Note that we temporarily use a mutable copy of the gem list to make our job easier, then
     *   convert it to an immutable list at the end
     */
    private fun List<Shape>.shiftDown(): List<Shape> {
        val mutable = toMutableList()
        for(column in 1..8) {
            for(row in 8 downTo 2) { // if top is empty doesn't matter; nothing to move into it
                val shape = mutable[row, column]
                if (shape == Empty) {
                    // find first non-empty above it and swap
                    for(above in row-1 downTo 1) {
                        if (mutable[above, column] != Empty) {
                            mutable.swapMutable(row, column, above, column)
                            break
                        }
                    }
                }
            }
        }
        return mutable.toList() // create an immutable list out of it so the caller can't cast it and abuse it
    }

    /**
     * Create a copy of the gem list that has Empty spots where matches were
     */
    private fun List<Shape>.removeMatches(matches: Set<Int>) =
        mapIndexed { n, existingShape ->
            if (n in matches) {
                Empty
            } else {
                existingShape
            }
        }

    /**
     * Create a copy of the gem list that has random shapes where Empty spaces used to be
     */
    private fun List<Shape>.replaceEmptiesWithRandoms() =
        map {
            if (it == Empty) {
                randomShape()
            } else {
                it
            }
        }

    /**
     * Create a random shape
     */
    private fun randomShape() = when(Random.nextInt(4)) {
        0 -> Square
        1 -> Circle
        2 -> Cross
        else -> Diamond
    }
}