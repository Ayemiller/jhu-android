package miller.adam.hw4

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.clickable
import androidx.compose.foundation.gestures.awaitFirstDown
import androidx.compose.foundation.gestures.awaitTouchSlopOrCancellation
import androidx.compose.foundation.gestures.drag
import androidx.compose.foundation.gestures.forEachGesture
import androidx.compose.foundation.layout.*
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.drawscope.DrawScope
import androidx.compose.ui.graphics.drawscope.DrawStyle
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.input.pointer.*
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import miller.adam.hw4.ui.theme.HW4Theme

// Portions of code were borrowed from the ComposeGraphics sample and inspired by the HW4
// description. Those portions are commented throughout this class.
// Apache 2.0 license
class MainActivity : ComponentActivity() {
    private val viewModel by viewModels<GridViewModel>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            val scope = rememberCoroutineScope()
            HW4Theme {
                with(LocalDensity.current) {
                    // list construction suggested by A4 description
                    // Apache 2.0 license
                    val newList = List(8 * 8) { Types.getShape() }
                    val shapes by viewModel.shapes.collectAsState(newList)
                    val highlightedShapeList by viewModel.highlightedShapes.collectAsState(null)
                    val selectedShape by viewModel.selectedShape.collectAsState(null)
                    val draggingShape by viewModel.draggingShape.collectAsState(null)
                    val paused by viewModel.paused.collectAsState(false)
                    val clickDownForPause by viewModel.clickDownForPause.collectAsState(false)
                    val padding = 3.dp.toPx()
                    val gridWidth = 1.dp.toPx()
                    val cellsPerSide = 8
                    val outline = Stroke(padding)
                    var blinker: Job? = null
                    viewModel.initShapes(shapes)
                    // create a temporary mutable shape list
                    val theShapes: MutableList<Shape> = shapes as MutableList<Shape>
                    blinker?.cancel()
                    blinker = scope.launch(Dispatchers.Default) {
                        delay(500)
                        viewModel.handleMatches(cellsPerSide, theShapes)
                        blinker = null
                    }
                    // Some of the general GUI construction below was borrowed from the "ComposeGraphics" example
                    // Apache 2.0 license
                    Surface(
                        color = MaterialTheme.colors.background,
                    ) {
                        Scaffold {
//                            ScoreBoard(score = viewModel.getScore())
                            if (paused) {
                                PauseScreen(paused)
                            } else {
                                Column(
                                    horizontalAlignment = Alignment.CenterHorizontally,
                                    modifier = Modifier.fillMaxWidth()
                                ) {
                                    ScoreBoard()
                                    Row(verticalAlignment = Alignment.CenterVertically) {
                                        GameGrid(
                                            selectedShape = selectedShape,
                                            shapes = shapes,
                                            dragging = draggingShape,
                                            highlightedShapeList = highlightedShapeList,
                                            cellsPerSide = cellsPerSide,
                                            gridWidth = gridWidth,
                                            outline = outline,
                                            onStartDragging = { shape, offset, sideSize, sizePerCell ->
                                                viewModel.startDragging(
                                                    shape,
                                                    offset.x,
                                                    offset.y,
                                                    sideSize,
                                                    sizePerCell
                                                )
                                            },
                                            onUpWithoutDragging = {
                                                if (clickDownForPause) {
                                                    viewModel.resetClickDownForPause()
                                                    viewModel.togglePause()
                                                } else {
                                                    blinker?.cancel()
                                                    val highlights: Set<Shape> =
                                                        viewModel.getAllShapesWithType(
                                                            selectedShape?.type ?: Types.EMPTY
                                                        )
                                                    blinker = scope.launch(Dispatchers.Default) {
                                                        viewModel.highlightShapes(highlights)
                                                        blinker = null
                                                    }
                                                }
                                            },
                                            onStopDragging = viewModel::stopDragging,
                                            onDrag = {
                                                viewModel.drag(it.x, it.y)
                                            },
                                            onCancelDrag = viewModel::cancelDrag,
                                            onFinishDrag = { endShape, sideSize, sizePerCell ->
                                                blinker?.cancel()
                                                blinker = scope.launch(Dispatchers.Default) {
                                                    viewModel.finishDrag(
                                                        endShape = endShape,
                                                        sideSize = sideSize,
                                                        sizePerCell = sizePerCell
                                                    )
                                                    blinker = null
                                                }
                                            },
                                            onSelect = viewModel::select,
                                            getClickDownForPause = clickDownForPause,
                                            setClickDownForPause = viewModel::setClickDownForPause,
                                            resetClickDownForPause = viewModel::resetClickDownForPause,
                                            shuffleGrid = { cellsPerSide ->
                                                blinker?.cancel()
                                                blinker = scope.launch(Dispatchers.Default) {
                                                    viewModel.shuffleGrid(cellsPerSide)
                                                    blinker = null
                                                }
                                            },
                                            foundPotentialMatches = { sideSize, it, sizePerCell ->
                                                viewModel.foundPotentialMatches(
                                                    sideSize = sideSize,
                                                    offset = it,
                                                    sizePerCell = sizePerCell,
                                                )
                                            }
                                        )
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    @Composable
    private fun PauseScreen(paused: Boolean) {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier
                .fillMaxWidth()
                .clickable(onClick = {
                    if (paused) {
                        viewModel.togglePause()
                    }
                })
        ) {
            Row(
                verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier.fillMaxHeight()
            ) {
                Spacer(modifier = Modifier.padding(20.dp))
                Text(
                    text = stringResource(id = R.string.paused),
                    style = MaterialTheme.typography.h3,
                    modifier = Modifier.padding(20.dp)
                )
                Spacer(modifier = Modifier.padding(20.dp))
            }
        }
    }

    @Composable
    private fun ScoreBoard() {
        Row(verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier
                .clickable(onClick = {
                    viewModel.togglePause()
                }
                )) {
            Spacer(modifier = Modifier.padding(20.dp))
            Text(
                text = stringResource(id = R.string.score) + viewModel.getScore()
                    .toString(),
                style = MaterialTheme.typography.h3,
                modifier = Modifier.padding(20.dp)
            )
            Spacer(modifier = Modifier.padding(20.dp))
        }
    }


}

// from assignment A4 description
// Apache 2.0 license
fun shapeIndex(coords: GridCoords): Int = (coords.row - 1) * 8 + coords.col - 1

data class GridCoords(val row: Int, val col: Int, val cellsPerSide: Int = -1) {
    fun isValid(): Boolean {
        return row in 1..cellsPerSide && col in 1..cellsPerSide
    }
}

fun getIndex(offset: Offset, sideSize: Int, sizePerCell: Float): Int {
    val rowAndColumn = getRowAndColumn(offset, sideSize, sizePerCell)
    return shapeIndex(rowAndColumn)
}

private fun getRowAndColumn(offset: Offset, sideSize: Int, sizePerCell: Float): GridCoords {
    val x = offset.x
    val y = offset.y
    val col = (x / sizePerCell).toInt()
    val row = (y / sizePerCell).toInt()
    return GridCoords(row = row, col = col, cellsPerSide = sideSize)
}

// from assignment A4 description
// Apache 2.0 license
operator fun List<Shape>.get(coords: GridCoords) = this[shapeIndex(coords)]

// from assignment A4 description
// Apache 2.0 license
operator fun MutableList<Shape>.set(coords: GridCoords, shape: Shape) {
    this[shapeIndex(coords)] = shape
}

@Composable
fun GameGrid(
    selectedShape: Shape?,
    shapes: List<Shape>,
    dragging: Shape?,
    cellsPerSide: Int,
    gridWidth: Float,
    outline: DrawStyle,
    highlightedShapeList: Set<Shape>?,
    onStartDragging: (Shape, Offset, Int, Float) -> Unit,
    onUpWithoutDragging: () -> Unit,
    onStopDragging: () -> Unit,
    onDrag: (Offset) -> Unit,
    onCancelDrag: (Shape, Int, Float) -> Unit,
    onFinishDrag: (Shape, Int, Float) -> Unit,
    onSelect: (Shape) -> Unit,
    setClickDownForPause: () -> Unit,
    resetClickDownForPause: () -> Unit,
    getClickDownForPause: Boolean,
    shuffleGrid: (Int) -> Unit,
    foundPotentialMatches: (Int, Offset, Float) -> Boolean,
) {
    var shapesToUse by remember { mutableStateOf(shapes) }
    var draggingShape by remember { mutableStateOf(dragging) }
    var startingShape by remember { mutableStateOf(selectedShape) }
    var highlightedShapes by remember { mutableStateOf(highlightedShapeList) }
    var clickDownForPause by remember { mutableStateOf(getClickDownForPause) }

    shapesToUse = shapes
    draggingShape = dragging
    startingShape = selectedShape
    highlightedShapes = highlightedShapeList
    clickDownForPause = getClickDownForPause
    var size: Float
    var sizePerCell = 0F
    var halfCell: Float

    Canvas(
        modifier = Modifier
            .fillMaxSize()
            .pointerInput(Unit) {
                detectGraphGestures(

                    onDown = { finger ->
                        val fingerCoordinates = getRowAndColumn(finger, cellsPerSide, sizePerCell)
                        if (fingerCoordinates.isValid()) {
                            val theShape = shapesToUse[fingerCoordinates]
                            onSelect(theShape)
                            onStartDragging(theShape, finger, cellsPerSide, sizePerCell)
                        } else {
                            setClickDownForPause()
                        }
                    },
                    onUpWithoutDragging = onUpWithoutDragging,
                    onDragEnd = { finger ->
                        finger.let {
                            if (getRowAndColumn(it, cellsPerSide, sizePerCell).isValid() &&
                                isValidDistance(
                                    endCoord = getRowAndColumn(it, cellsPerSide, sizePerCell),
                                    startingCoord = getRowAndColumn(
                                        Offset(
                                            startingShape!!.centerX,
                                            startingShape!!.centerY
                                        ), cellsPerSide, sizePerCell
                                    ),
                                    cellsPerSide = cellsPerSide

                                ) && foundPotentialMatches(cellsPerSide, it, sizePerCell)
                            ) {

                                val index = getIndex(it, cellsPerSide, sizePerCell)
                                val dragEndShape = shapesToUse[index]
                                onFinishDrag(dragEndShape, cellsPerSide, sizePerCell)
                            } else if (clickDownForPause && !getRowAndColumn(it,
                                    cellsPerSide,
                                    sizePerCell).isValid()
                            ) {
                                resetClickDownForPause()
                                shuffleGrid(cellsPerSide)
                            } else {
                                // if we're dragging anything, put it back
                                draggingShape?.let {
                                    onCancelDrag(it, cellsPerSide, sizePerCell)
                                }
                            }
                        }
                    },
                    onDragCancel = onStopDragging,
                    onDrag = { change, _ ->
                        change.consumeAllChanges()
                        onDrag(change.position)
                    },

                    )
            }

    ) {

        size = minOf(this.size.height, this.size.width)
        sizePerCell = size / (cellsPerSide + 2)
        halfCell = sizePerCell / 2
        drawGrid(
            drawScope = this,
            sideSize = cellsPerSide,
            sizePerCell = sizePerCell,
            gridWidth = gridWidth
        )
        val padding = 5.dp.toPx()

        for ((index, aShape) in shapesToUse.withIndex()) {
            val outlineColor =
                if (highlightedShapes?.contains(aShape) == true) Color.Magenta else Color.Black
            val offset = Offset(
                (sizePerCell * ((index) % cellsPerSide + 1) + halfCell),
                (sizePerCell * (index / cellsPerSide + 1) + halfCell)
            )
            aShape.centerX = offset.x
            aShape.centerY = offset.y
            Types.drawThisShape(
                drawScope = this,
                aShape = aShape,
                halfSize = halfCell - padding,
                outline = outline,
                outlineColor = outlineColor,
            )
        }
        if (draggingShape != null) {
            val outlineColor =
                if (highlightedShapes?.contains(draggingShape!!) == true) Color.Magenta else Color.Black
            Types.drawThisShape(
                drawScope = this,
                aShape = draggingShape!!,
                halfSize = halfCell - padding,
                outline = outline,
                outlineColor = outlineColor,
            )
        }
    }
}

fun isValidDistance(endCoord: GridCoords, startingCoord: GridCoords, cellsPerSide: Int): Boolean {
    return (startingCoord.isValid() && endCoord.isValid() && (
            GridCoords(startingCoord.row + 1, startingCoord.col, cellsPerSide) == endCoord ||
                    GridCoords(
                        startingCoord.row - 1,
                        startingCoord.col,
                        cellsPerSide
                    ) == endCoord ||
                    GridCoords(
                        startingCoord.row,
                        startingCoord.col + 1,
                        cellsPerSide
                    ) == endCoord ||
                    GridCoords(startingCoord.row, startingCoord.col - 1, cellsPerSide) == endCoord))
}

private fun drawGrid(
    drawScope: DrawScope,
    sideSize: Int,
    sizePerCell: Float,
    gridWidth: Float,
) {
    for (row in 1..sideSize + 1) {
        // draw row lines
        drawScope.drawLine(
            start = Offset(sizePerCell, row * sizePerCell),
            end = Offset(sizePerCell + sizePerCell * sideSize, row * sizePerCell),
            color = Color.Black,
            strokeWidth = gridWidth,
        )

        // draw col lines
        drawScope.drawLine(
            start = Offset(row * sizePerCell, sizePerCell),
            end = Offset(row * sizePerCell, sizePerCell + sizePerCell * sideSize),
            color = Color.Black,
            strokeWidth = gridWidth,
        )
    }
}

// This was copied from Compose's DragGestureDetector line 195, Compose version 1.0.0 rc01
// Apache 2.0 license
suspend fun PointerInputScope.detectGraphGestures(
    onDown: (Offset) -> Unit = { },
    onUpWithoutDragging: () -> Unit = { },
    onDragStart: (Offset) -> Unit = { },
    onDragEnd: (Offset) -> Unit = { },
    onDragCancel: () -> Unit = { },
    onDrag: (change: PointerInputChange, dragAmount: Offset) -> Unit,
) {
    forEachGesture {
        awaitPointerEventScope {
            val down = awaitFirstDown(requireUnconsumed = false)
            onDown(down.position)
            var drag: PointerInputChange?
            var overSlop = Offset.Zero
            do {
                drag = awaitTouchSlopOrCancellation(down.id) { change, over ->
                    change.consumePositionChange()
                    overSlop = over
                }
            } while (drag != null && !drag.positionChangeConsumed())
            if (drag != null) {
                onDragStart.invoke(drag.position)
                onDrag(drag, overSlop)
                var lastOffset = drag.position
                if (
                    !drag(drag.id) {
                        lastOffset = it.position
                        onDrag(it, it.positionChange())
                        it.consumePositionChange()
                    }
                ) {
                    onDragCancel()
                } else {
                    onDragEnd(lastOffset)
                }
            } else {
                onUpWithoutDragging()
            }
        }
    }
}