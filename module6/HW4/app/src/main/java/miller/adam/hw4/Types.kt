package miller.adam.hw4

import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Path
import androidx.compose.ui.graphics.drawscope.DrawScope
import androidx.compose.ui.graphics.drawscope.DrawStyle
import androidx.compose.ui.graphics.drawscope.Fill
import java.util.*

enum class Types {
    EMPTY {
        override fun drawShape(
            drawScope: DrawScope,
            center: Offset,
            halfSize: Float,
            outline: DrawStyle,
            outlineColor: Color,
        ) = Unit
    },
    CIRCLE {
        override fun drawShape(
            drawScope: DrawScope,
            center: Offset,
            halfSize: Float,
            outline: DrawStyle,
            outlineColor: Color,
        ) {
            drawScope.apply {
                drawCircle(
                    center = center,
                    radius = halfSize,
                    style = Fill,
                    color = Color.Yellow
                )
                drawCircle(
                    center = center,
                    radius = halfSize,
                    style = outline,
                    color = outlineColor
                )
            }
        }
    },
    SQUARE {
        override fun drawShape(
            drawScope: DrawScope,
            center: Offset,
            halfSize: Float,
            outline: DrawStyle,
            outlineColor: Color,
        ) {
            drawScope.apply {
                val topLeft = Offset(center.x - halfSize, center.y - halfSize)
                val size = Size(halfSize * 2, halfSize * 2)
                drawRect(
                    topLeft = topLeft,
                    size = size,
                    style = Fill,
                    color = Color.Blue,
                )
                drawRect(
                    topLeft = topLeft,
                    size = size,
                    style = outline,
                    color = outlineColor,
                )
            }
        }
    },
    DIAMOND {
        override fun drawShape(
            drawScope: DrawScope,
            center: Offset,
            halfSize: Float,
            outline: DrawStyle,
            outlineColor: Color,
        ) {
            drawScope.apply {
                val path = Path().apply {
                    // start @ top middle
                    moveTo(center.x, center.y - halfSize)
                    // to right middle
                    lineTo(center.x + halfSize, center.y)
                    // to bottom middle
                    lineTo(center.x, center.y + halfSize)
                    // to left middle
                    lineTo(center.x - halfSize, center.y)
                    // to top middle
                    lineTo(center.x, center.y - halfSize)
                    close()
                }
                drawPath(path = path, style = Fill, color = Color.Red)
                drawPath(path = path, style = outline, color = outlineColor)
            }
        }
    },
    PLUS {
        override fun drawShape(
            drawScope: DrawScope,
            center: Offset,
            halfSize: Float,
            outline: DrawStyle,
            outlineColor: Color,
        ) {
            drawScope.apply {
                val quarterSize = halfSize / 2
                val start = Offset(center.x - quarterSize, center.y - halfSize)
                val path = Path().apply {
                    // start @ top left-ish
                    moveTo(start.x, start.y)
                    // move to the right a halfSize
                    lineTo(start.x + halfSize, start.y)
                    //down a quarter
                    lineTo(start.x + halfSize, start.y + quarterSize)
                    //right a quarter
                    lineTo(start.x + halfSize + quarterSize, start.y + quarterSize)
                    //down a halfSize
                    lineTo(start.x + halfSize + quarterSize, start.y + quarterSize + halfSize)
                    //left a quarter
                    lineTo(start.x + halfSize, start.y + quarterSize + halfSize)
                    //down a quarter
                    lineTo(start.x + halfSize, start.y + halfSize * 2)
                    //left a halfSize
                    lineTo(start.x, start.y + halfSize * 2)
                    //up a quarter
                    lineTo(start.x, start.y + halfSize + quarterSize)
                    //left a quarter
                    lineTo(start.x - quarterSize, start.y + halfSize + quarterSize)
                    //up a halfSize
                    lineTo(start.x - quarterSize, start.y + quarterSize)
                    //right a quarter
                    lineTo(start.x, start.y + quarterSize)
                    //up a quarter
                    lineTo(start.x, start.y)
                    close()
                }
                drawPath(path = path, style = Fill, color = Color.Green)
                drawPath(path = path, style = outline, color = outlineColor)
            }
        }
    };

    abstract fun drawShape(
        drawScope: DrawScope,
        center: Offset,
        halfSize: Float,
        outline: DrawStyle,
        outlineColor: Color,
    )

    companion object {
        // random int from 1 to 4 inclusive
        fun getRandomShape() = (1..4).random()
        fun getShape(): Shape {
            return Shape(type = values()[getRandomShape()])
        }

        fun getRandomType() = values()[getRandomShape()]

        fun drawThisShape(
            drawScope: DrawScope,
            aShape: Shape,
            halfSize: Float,
            outline: DrawStyle,
            outlineColor: Color,
        ) {
            aShape.type.drawShape(
                drawScope,
                Offset(aShape.centerX, aShape.centerY),
                halfSize,
                outline,
                outlineColor,
            )
        }
    }
}

data class Shape(
    val type: Types, var centerX: Float = 0f, var centerY: Float = 0f,
    val id: String = UUID.randomUUID().toString(),
)
