package miller.adam.hw4

import androidx.compose.ui.geometry.Offset
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

// Portions of code were borrowed and inspired from the ComposeGraphics sample and the HW4
// description.
// Apache 2.0 license
class GridViewModel : ViewModel() {
    private val paused0 = MutableStateFlow(false)
    val paused: StateFlow<Boolean> get() = paused0

    private val clickDownForPause0 = MutableStateFlow(false)
    val clickDownForPause: StateFlow<Boolean> get() = clickDownForPause0

    private val shapes0 = MutableStateFlow<List<Shape>>(emptyList())
    val shapes: StateFlow<List<Shape>> get() = shapes0

    private val highlightedShapes0 = MutableStateFlow<Set<Shape>?>(emptySet())
    val highlightedShapes: StateFlow<Set<Shape>?> get() = highlightedShapes0

    private val selectedShape0 = MutableStateFlow<Shape?>(null)
    val selectedShape: StateFlow<Shape?> get() = selectedShape0

    private val draggingShape0 = MutableStateFlow<Shape?>(null)
    val draggingShape: StateFlow<Shape?> get() = draggingShape0

    private val score0 = MutableStateFlow(0)
    private val score: StateFlow<Int> get() = score0

    private var draggingStartX: Float? = null
    private var draggingStartY: Float? = null
    private var draggingOffsetX = -1f
    private var draggingOffsetY = -1f
    private var draggingStartIndex: Int? = null

    fun togglePause() {
        paused0.tryEmit(!paused0.value)
    }

    fun initShapes(shapes: List<Shape>) {
        shapes0.value = shapes
    }

    fun select(shape: Shape) {
        selectedShape0.value = shape
    }

    fun startDragging(
        shape: Shape,
        offsetX: Float,
        offsetY: Float,
        sideSize: Int,
        sizePerCell: Float,
    ) {
        val newDraggingShape = shape.copy()
        newDraggingShape.centerX = shape.centerX
        newDraggingShape.centerY = shape.centerY
        draggingShape0.value = newDraggingShape
        draggingOffsetX = shape.centerX - offsetX
        draggingOffsetY = shape.centerY - offsetY
        draggingStartX = shape.centerX
        draggingStartY = shape.centerY
        draggingStartIndex = getIndex(
            offset = Offset(draggingStartX!!, draggingStartY!!),
            sideSize = sideSize,
            sizePerCell = sizePerCell
        )
    }

    private suspend fun pause() = withContext(Dispatchers.Default) {
        repeat(3) {
            try {
                highlightedShapes0.emit(setOf())
                delay(100)
            } finally {
                highlightedShapes0.emit(null)
                if (it != 2) {
                    delay(100)
                }
            }
        }
    }

    suspend fun highlightShapes(highlights: Set<Shape>?) = withContext(Dispatchers.Default) {
        draggingShape.value?.type?.let { _ ->
            repeat(3) {
                try {
                    highlightedShapes0.emit(highlights)
                    delay(100)
                } finally {
                    highlightedShapes0.emit(null)
                    if (it != 2) {
                        delay(100)
                    }
                    stopDragging()
                }
            }
        }
    }

    fun getAllShapesWithType(type: Types): Set<Shape> {
        val shapes: MutableSet<Shape> = mutableSetOf()
        for (gridShape in shapes0.value) {
            if (gridShape.type == type) {
                shapes.add(gridShape.copy())
            }
        }
        return shapes
    }

    fun stopDragging() {
        draggingShape0.value = null
        draggingStartX = null
        draggingStartY = null
        draggingStartIndex = null
    }

    fun drag(x: Float, y: Float) {
        draggingShape.value?.let { shape ->

            //since we're dragging, make sure the original shape in the grid appears empty

            if (draggingStartIndex != null && 0 <= draggingStartIndex!! && draggingStartIndex!! < shapes0.value.size) {
                val theShapes: MutableList<Shape> = shapes0.value as MutableList<Shape>
                theShapes[draggingStartIndex!!] = shape.copy(type = Types.EMPTY)
                shapes0.value = theShapes
                draggingStartIndex = null
            }
            draggingShape0.tryEmit(
                shape.copy(
                    centerX = draggingOffsetX + x,
                    centerY = draggingOffsetY + y,
                )
            )
        }
    }

    fun cancelDrag(shape: Shape, sideSize: Int, sizePerCell: Float) {
        // create a temporary mutable shape list
        val theShapes: MutableList<Shape> = shapes0.value as MutableList<Shape>
        val index = getIndex(
            offset = Offset(draggingStartX!!, draggingStartY!!),
            sideSize = sideSize,
            sizePerCell = sizePerCell,
        )
        theShapes[index] = theShapes[index].copy(type = shape.type)
        shapes0.value = theShapes
        stopDragging()
    }

    suspend fun finishDrag(
        endShape: Shape,
        sideSize: Int,
        sizePerCell: Float,
    ) {
        draggingShape0.value?.let {

            // create a temporary mutable shape list
            val theShapes: MutableList<Shape> = shapes0.value as MutableList<Shape>

            // move the shape we're dropping onto back to where we started dragging
            val oldShapeIndex = getIndex(
                offset = Offset(draggingStartX!!, draggingStartY!!),
                sideSize = sideSize,
                sizePerCell = sizePerCell,
            )
            theShapes[oldShapeIndex] =
                endShape.copy(centerX = draggingStartX!!, centerY = draggingStartY!!)

            //drop the shape we're dragging
            val endShapeIndex = getIndex(
                offset = Offset(endShape.centerX, endShape.centerY),
                sideSize = sideSize,
                sizePerCell = sizePerCell,
            )
            theShapes[endShapeIndex] =
                it.copy(centerX = endShape.centerX, centerY = endShape.centerY)

            handleMatches(sideSize, theShapes)
        }
    }

    suspend fun handleMatches(
        sideSize: Int,
        theShapes: MutableList<Shape>,
    ) {
        // now check for matches
        var matches = getMatches(sideSize)
        while (matches.isNotEmpty()) {
            score0.emit(score.value + matches.size)
            draggingShape0.value = Shape(type = Types.EMPTY)
            highlightShapes(matches)
            removeShapes(matches, theShapes)
            pause()
            pushDown(sideSize)
            pause()
            insertNewShapes()
            pause()
            matches = getMatches(sideSize)
        }
    }

    // check to see if any potential matches will be created by the dragged shape, and return
    // true if there are matches, false if no matches
    fun foundPotentialMatches(sideSize: Int, offset: Offset, sizePerCell: Float): Boolean {
        // create a temporary mutable shape list
        val theShapes: MutableList<Shape> = shapes0.value as MutableList<Shape>
        // put in the dragging shape to test to see if any matches were made
        val draggingShapeIndex = getIndex(
            offset = offset,
            sideSize = sideSize,
            sizePerCell = sizePerCell)
        val draggingStartIndex = getIndex(
            offset = Offset(draggingStartX!!, draggingStartY!!),
            sideSize = sideSize,
            sizePerCell = sizePerCell,
        )
        val oldDragEndType = theShapes[draggingShapeIndex].type
        val oldDragStartType = theShapes[draggingStartIndex].type
        theShapes[draggingShapeIndex] =
            theShapes[draggingShapeIndex].copy(type = draggingShape.value!!.type)
        theShapes[draggingStartIndex] =
            theShapes[draggingStartIndex].copy(type = oldDragEndType)
        // test whether there are matches if we actually would perform the swap
        val returnVal = getMatches(sideSize, theShapes).isNotEmpty()

        // return the board to the way it was
        theShapes[draggingShapeIndex] =
            theShapes[draggingShapeIndex].copy(type = oldDragEndType)
        theShapes[draggingStartIndex] =
            theShapes[draggingStartIndex].copy(type = oldDragStartType)
        return returnVal
    }

    private fun getMatches(sideSize: Int, shapes: List<Shape> = shapes0.value): Set<Shape> {
        // find 3+ in a row (up or down)
        val matches: MutableSet<Shape> = mutableSetOf()
        for (row in 1..sideSize) {
            var lastShape: Types? = null
            val shapeList: MutableList<Shape> = mutableListOf()
            for (col in 1..sideSize) {
                val coord = GridCoords(row, col)
                val aShape: Shape = shapes[coord]
                if (aShape.type == lastShape) {
                    shapeList.add(aShape)
                } else {
                    lastShape = aShape.type
                    if (shapeList.size >= 3) {
                        matches.addAll(shapeList)
                    }
                    shapeList.clear()
                    shapeList.add(aShape)
                }
            }
            if (shapeList.size >= 3) {
                matches.addAll(shapeList)
            }
        }

        for (col in 1..sideSize) {
            var lastShape: Types? = null
            val shapeList: MutableList<Shape> = mutableListOf()
            for (row in 1..sideSize) {
                val coord = GridCoords(row, col)
                val aShape: Shape = shapes[coord]
                if (aShape.type == lastShape) {
                    shapeList.add(aShape)
                } else {
                    lastShape = aShape.type
                    if (shapeList.size >= 3) {
                        matches.addAll(shapeList)
                    }
                    shapeList.clear()
                    shapeList.add(aShape)
                }
            }
            if (shapeList.size >= 3) {
                matches.addAll(shapeList)
            }
        }
        return matches
    }

    fun getScore(): Int {
        return score.value
    }

    private suspend fun removeShapes(
        matches: Set<Shape>,
        theShapes: MutableList<Shape>,
    ) = coroutineScope {
        // "remove" the matched shapes
        for ((index, shape) in theShapes.withIndex()) if (shape in matches) {
            theShapes[index] = theShapes[index].copy(type = Types.EMPTY)
        }
        shapes0.emit(theShapes)
    }

    private suspend fun pushDown(cellsPerSide: Int) = coroutineScope {
        // shift shapes down if possible
        val theShapes: MutableList<Shape> = shapes0.value as MutableList<Shape>
        for (row in (cellsPerSide - 1) downTo 1) {
            for (col in 1..cellsPerSide) {

                //check if square below is empty, if so, move me down
                var rowBelow = row + 1
                var shapeBelowCoord = GridCoords(rowBelow, col)
                var shapeBelow = theShapes[shapeBelowCoord].copy()

                while (shapeBelow.type == Types.EMPTY && rowBelow <= cellsPerSide) {
                    val currentRow = rowBelow - 1
                    val currentShapeCoord = GridCoords(currentRow, col)
                    val currentShape = theShapes[currentShapeCoord].copy()
                    theShapes.set(
                        coords = shapeBelowCoord,
                        shape = shapeBelow.copy(type = currentShape.type)
                    )
                    theShapes.set(
                        coords = currentShapeCoord,
                        shape = currentShape.copy(type = Types.EMPTY)
                    )
                    rowBelow++
                    if (rowBelow <= cellsPerSide) {
                        shapeBelowCoord = GridCoords(rowBelow, col)
                        shapeBelow = theShapes[shapeBelowCoord].copy()
                    }

                }

            }
        }
        shapes0.emit(theShapes)
    }

    private suspend fun insertNewShapes() = coroutineScope {
        val theShapes: MutableList<Shape> = shapes0.value as MutableList<Shape>
        try {
            // "insert" new shapes
            for ((index, shape) in theShapes.withIndex()) {
                if (shape.type == Types.EMPTY) {
                    theShapes[index] = shape.copy(type = Types.getRandomType())
                }
            }
        } finally {
            shapes0.emit(theShapes)
        }
    }

    // reset the flag that keeps track of user clicking outside the grid
    fun resetClickDownForPause() {
        clickDownForPause0.tryEmit(false)
    }

    // set the flag that keeps track of the user clicking outside the grid
    fun setClickDownForPause() {
        clickDownForPause0.tryEmit(true)
    }

    // Fill the grid w/ random shapes and subtract 10 points from the score.
    // Also check for matches once the random shapes are brought out.
    suspend fun shuffleGrid(cellsPerSide: Int) {
        // create a temporary mutable shape list
        val theShapes: MutableList<Shape> = shapes0.value as MutableList<Shape>

        for ((index, shape) in theShapes.withIndex()) {
            theShapes[index] = shape.copy(type = Types.getRandomType())
        }
        val shufflePenalty = -10
        score0.tryEmit(score.value + shufflePenalty)
        shapes0.tryEmit(theShapes)
        coroutineScope {
            launch(Dispatchers.Default) {
                delay(500)
                handleMatches(sideSize = cellsPerSide, theShapes = theShapes)
            }
        }

    }

}