package stanchfield.scott.composesample

import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Divider
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import stanchfield.scott.composesample.ui.theme.ComposeSampleTheme

sealed class MovieState
object SelectionList: MovieState()
class Display(val movie: Movie): MovieState()

class MainActivity : ComponentActivity() {
    private val movieStates = MutableStateFlow<List<MovieState>>(value = listOf(SelectionList))

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val movieList = listOf(
            Movie("m1", "The Transporter", "Jason Statham Rocks"),
            Movie("m2", "The Transporter II", "Jason Statham Rocks Again"),
            Movie("m3", "The Transporter III", "Jason Statham is Getting Tired of These"),
        )
//        val movieFlow = flow<Movie> {
//            movieList.forEach {
//                emit(it)
//                delay(2000)
//            }
//        }


        setContent {
            ComposeSampleTheme {
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colors.background) {
                    UI(
                        movieStates = movieStates,
                        movies = movieList
                    ) { movie ->
                        movieStates.value += Display(movie)
                    }
                }
            }
        }
    }

    override fun onBackPressed() {
        val newStack = movieStates.value.dropLast(1)
        if (newStack.isEmpty()) {
            finish()
        } else {
            movieStates.value = newStack
        }
    }
}

@Composable
fun UI(
    movieStates: Flow<List<MovieState>>,
    movies: List<Movie>,
    onMovieSelected: (Movie) -> Unit
) {
    val stateStack by movieStates.collectAsState(initial = listOf(SelectionList))

    @Suppress("UnnecessaryVariable")
    when (val currentState = stateStack.last()) {
        SelectionList -> BadMovieList(movies = movies) {
            onMovieSelected(it)
        }
        is Display -> MovieDisplay(movie = currentState.movie)
    }
}



@Composable
fun BadMovieList(
    movies: List<Movie>,
    onMovieSelected: (Movie) -> Unit
) {
    Column {
        movies.forEach { movie ->
            Text(
                text = movie.title,
                modifier =
                Modifier
                    .padding(8.dp)
                    .clickable {
                        onMovieSelected(movie)
                    }
            )
        }
    }
}

@Composable
fun MovieDisplay(movieFlow: Flow<Movie>) {
    val movie: Movie? by movieFlow.collectAsState(initial = null)

    movie?.let {
        Text(text = "Current Movie")
        MovieDisplay(movie = it)
    }
}

@Composable
fun MovieDisplay(movieList: List<Movie>) {
    Column {
        movieList.forEach {
            MovieDisplay(movie = it)
            Divider()
        }
    }
}

@Composable
fun MovieDisplay(movie: Movie) {
    Column {
        Text(
            text = "Title",
            modifier =
                Modifier.padding(all = 8.dp)
        )
        Text(
            text = movie.title,l
            modifier = Modifier.padding(top = 8.dp, bottom = 8.dp, end = 8.dp, start = 16.dp)
        )
        Text(
            text = "Description",
            modifier = Modifier.padding(all = 8.dp)
        )
        Text(
            text = movie.description,
            modifier = Modifier.padding(top = 8.dp, bottom = 8.dp, end = 8.dp, start = 16.dp)
        )
    }
}

@Composable
fun MovieEdit(movie: Movie) {
    Column {
        TextField(
            value = movie.title,
            onValueChange = {
                Log.d("Text", it)
            },
            label = { Text("Title")},
            modifier = Modifier
                .padding(all = 8.dp)
                .fillMaxWidth()
        )
        TextField(
            value = movie.description,
            onValueChange = {
                Log.d("Text", it)
            },
            label = { Text("Description")},
            modifier = Modifier
                .padding(all = 8.dp)
                .fillMaxWidth()
        )
    }
}

@Composable
fun Greeting(name: String) {
    Text(text = "Hello $name!")
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    ComposeSampleTheme {
        Greeting("Scott")
    }
}