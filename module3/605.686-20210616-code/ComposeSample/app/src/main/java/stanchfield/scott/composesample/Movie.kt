package stanchfield.scott.composesample

import java.util.UUID

class Movie(
    var id: String = UUID.randomUUID().toString(),
    var title : String,
    var description : String
)