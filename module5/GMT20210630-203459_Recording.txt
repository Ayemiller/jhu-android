00:04:15	Sam Pagano:	Something
00:04:22	Scott Stanchfield:	Random response
01:05:38	Jacob Rhodes:	is it normal that we have to use TopLeft in compose vs. start and end in the XML
01:22:34	Ed Gleeck:	can you remove that clickable effect?
01:28:23	Alek:	is your device a real android device or an emulator
01:31:31	Scott Stanchfield:	This one is a OnePlus 9 Pro
01:32:10	Scott Stanchfield:	My emulator is acting up; system UI keeps crashing for some reason
01:33:47	Scott Stanchfield:	This is the emulator - notice how the status bar and nav buttons are gone... Sigh...
01:34:07	Scott Stanchfield:	But it still runs the app
01:34:08	Alek:	ohh I see :/
01:34:44	Scott Stanchfield:	and there's the message that keeps popping up
01:37:20	Ed Gleeck:	yes
01:37:23	Ed Gleeck:	can you write something?
02:01:05	Ed Gleeck:	I don't see the purpose of having the extra pointer variable since it can still mutate the private variables' contents. Maybe I'm missing something
02:01:47	Ed Gleeck:	right
02:02:17	Ed Gleeck:	so I can't do shapes.property = 1
02:02:34	Ed Gleeck:	ok got it
02:03:12	Ed Gleeck:	i missed the "val" on shape class
02:10:16	Ed Gleeck:	i saw it in the recorded video from last week
02:43:51	Jake Rhodes:	Just to refresh: the "let" command basically says that if it's not null, it can go ahead an do that expression?
02:44:55	Jake Rhodes:	Perfect! Thanks for that!
