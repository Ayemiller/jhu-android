package stanchfield.scott.noncomposecustomview

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Paint.Style
import android.util.AttributeSet
import android.view.View

// Was used in layout XML as follows
//    <stanchfield.scott.noncomposecustomview.OldCustomView
//       android:id="@+id/custom1"
//       ... layout attributes omitted ...
//       app:fill_color="@color/red"
//       app:outline_color="@color/white"
//       app:outline_width="3dp"
//       app:value="0.5" />
//
// and its attributes can be modified in code:
//    binding.custom1.value = 0.3f
//    binding.fillColor = Color.RED
//    ...
class OldGaugeView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {
    var outlineColor: Int = Color.WHITE
        set(value) {
            field = value
            updateOutlinePaint()
        }

    var outlineWidth: Float = 0f
        set(value) {
            field = value
            updateOutlinePaint()
        }
    var fillColor: Int = Color.GREEN
        set(value) {
            field = value
            updateFillPaint()
        }
    var value: Float = 0f
        set(value) {
            require(value in 0f..1f) { "Value must be between 0.0 and 1.0, inclusive" }
            field = value
            invalidate()
        }

    private lateinit var outlinePaint: Paint
    private lateinit var fillPaint: Paint

    init {
        context.theme.obtainStyledAttributes(
            attrs,
            R.styleable.OldGaugeView,
            0, 0).apply {

            try {
                outlineColor = getColor(R.styleable.OldGaugeView_outline_color, Color.WHITE)
                fillColor = getColor(R.styleable.OldGaugeView_fill_color, Color.BLUE)
                value = getFloat(R.styleable.OldGaugeView_value, 0f)
                outlineWidth = getFloat(R.styleable.OldGaugeView_outline_width, 0f)
            } finally {
                recycle()
            }
        }
    }

    private fun updateOutlinePaint() {
        outlinePaint = Paint().apply {
            style = Style.STROKE
            color = outlineColor
            strokeWidth = outlineWidth
        }
        invalidate()
    }
    private fun updateFillPaint() {
        fillPaint = Paint().apply {
            style = Style.FILL
            color = fillColor
        }
        invalidate()
    }

    override fun onDraw(canvas: Canvas) {
        canvas.drawRect(0f,height - height*value, width.toFloat(), height.toFloat(), fillPaint)
        canvas.drawRect(1f,1f, width.toFloat()-1, height.toFloat()-1, outlinePaint)
    }
}