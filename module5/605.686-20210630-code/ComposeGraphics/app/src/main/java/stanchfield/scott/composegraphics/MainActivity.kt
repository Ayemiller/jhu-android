package stanchfield.scott.composegraphics

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.gestures.detectTapGestures
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.size
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Path
import androidx.compose.ui.graphics.PathEffect
import androidx.compose.ui.graphics.drawscope.DrawScope
import androidx.compose.ui.graphics.drawscope.DrawStyle
import androidx.compose.ui.graphics.drawscope.Fill
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import stanchfield.scott.composegraphics.ui.theme.ComposeGraphicsTheme

class MainActivity : ComponentActivity() {
    private val viewModel by viewModels<GraphViewModel>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ComposeGraphicsTheme {
                with(LocalDensity.current) {
                    val shapes by viewModel.shapes.collectAsState(emptyList())
                    val lines by viewModel.lines.collectAsState(emptyList())
                    val selectedTool by viewModel.selectedTool.collectAsState(Square)

                    val strokeWidth = 3.dp.toPx()
                    val buttonSize = 48.dp.toPx()
                    val shapeSize = 36.dp.toPx()
                    val dashSize = 6.dp.toPx()
                    val gapSize = 3.dp.toPx()
                    val cornerRadius = 3.dp.toPx()

                    val outline = Stroke(strokeWidth)
                    val dashedOutline =
                        Stroke(
                            width = strokeWidth,
                            pathEffect = PathEffect.chainPathEffect(
                                PathEffect.dashPathEffect(floatArrayOf(dashSize, gapSize)),
                                PathEffect.cornerPathEffect(cornerRadius)
                            )
                        )

                    val squareSize = Size(shapeSize, shapeSize)
                    val halfShapeSize = shapeSize / 2
                    val halfButtonSize = buttonSize / 2
                    val shapeOffsetAmount = halfButtonSize - halfShapeSize
                    val shapeTopLeft = Offset(shapeOffsetAmount, shapeOffsetAmount)
                    val buttonCenter = Offset(halfButtonSize, halfButtonSize)

                    var finger by remember { mutableStateOf<Offset?>(null) }

                    // A surface container using the 'background' color from the theme
                    Surface(color = MaterialTheme.colors.background) {
                        Scaffold(
                            topBar = {
                                TopAppBar(
                                    title = { Text(stringResource(id = R.string.title)) },
                                    actions = {
//                                        viewModel.ToolbarButton(tool = Square) {
//                                            drawSquareShape(
//                                                topLeft = shapeTopLeft,
//                                                size = squareSize,
//                                                outlineStyle = outline
//                                            )
//                                        }
                                        IconButton(onClick = { viewModel.select(Square) }) {
                                            Canvas(modifier = Modifier.size(48.dp)) {
                                                drawSquareShape(
                                                    topLeft = shapeTopLeft,
                                                    size = squareSize,
                                                    outlineStyle = outline
                                                )
                                            }
                                        }
                                        IconButton(onClick = { viewModel.select(Circle) }) {
                                            Canvas(modifier = Modifier.size(48.dp)) {
                                                drawCircleShape(
                                                    center = buttonCenter,
                                                    radius = halfShapeSize,
                                                    outlineStyle = outline
                                                )
                                            }
                                        }
                                        IconButton(onClick = { viewModel.select(Triangle) }) {
                                            Canvas(modifier = Modifier.size(48.dp)) {
                                                drawTriangleShape(
                                                    center = buttonCenter,
                                                    halfSize = halfShapeSize,
                                                    outlineStyle = outline
                                                )
                                            }
                                        }
                                        IconButton(onClick = { viewModel.select(DrawLine) }) {
                                            Canvas(modifier = Modifier.size(48.dp)) {
                                                drawLineShape(
                                                    center = buttonCenter,
                                                    halfSize = halfShapeSize,
                                                    strokeWidth = strokeWidth
                                                )
                                            }
                                        }
                                        IconButton(onClick = { viewModel.select(Select) }) {
                                            Canvas(modifier = Modifier.size(48.dp)) {
                                                drawSquareShape(
                                                    topLeft = shapeTopLeft,
                                                    size = squareSize,
                                                    outlineStyle = dashedOutline,
                                                    fill = false
                                                )
                                            }
                                        }
                                    }
                                )
                            },
                            content = {
                                Canvas(
                                    modifier = Modifier.fillMaxSize().pointerInput(selectedTool) {
                                        when(selectedTool) {
                                            Select -> {}
                                            DrawLine -> {}
                                            else -> {
                                                detectTapGestures(
                                                    onTap = {
                                                        viewModel.add(Shape(selectedTool as ShapeType, it.x, it.y))
                                                    }
                                                )
                                            }
                                        }
                                    }
                                ) {
                                    lines.forEach {
                                        drawLine(
                                            start = Offset(it.end1.centerX, it.end1.centerY),
                                            end =
                                                it.end2?.let { end2 -> Offset(end2.centerX, end2.centerY) } ?: finger ?: throw IllegalStateException("Finger should not be able to be null here"),
                                                // equivalent to
//                                                if (it.end2 != null) {
//                                                    Offset(it.end2.centerX, it.end2.centerY)
//                                                } else if (finger != null) {
//                                                    finger
//                                                } else {
//                                                    throw IllegalStateException("Finger should not be able to be null here")
//                                                }
                                            color = Color.DarkGray
                                        )
                                    }
                                    shapes.forEach {
                                        when (it.type) {
                                            Circle -> drawCircleShape(center = Offset(it.centerX, it.centerY), radius = halfShapeSize, outlineStyle = outline)
                                            Square -> drawSquareShape(topLeft = Offset(it.centerX - halfShapeSize, it.centerY - halfShapeSize), size = squareSize, outlineStyle = outline)
                                            Triangle -> drawTriangleShape(center = Offset(it.centerX, it.centerY), halfSize = halfShapeSize, outlineStyle = outline)
                                        }
                                    }
                                }
                            }
                        )
                    }
                }
            }
        }
    }
}

fun DrawScope.drawSquareShape(
    topLeft: Offset,
    size: Size,
    outlineStyle: DrawStyle,
    fill: Boolean = true
) {
    if (fill) {
        drawRect(
            topLeft = topLeft,
            size = size,
            style = Fill,
            color = Color.Blue
        )
    }
    drawRect(
        topLeft = topLeft,
        size = size,
        style = outlineStyle,
        color = Color.Black
    )
}

fun DrawScope.drawCircleShape(
    center: Offset,
    radius: Float,
    outlineStyle: DrawStyle
) {
    drawCircle(
        center = center,
        radius = radius,
        style = Fill,
        color = Color.Green
    )
    drawCircle(
        center = center,
        radius = radius,
        style = outlineStyle,
        color = Color.Black
    )
}

fun DrawScope.drawTriangleShape(
    center: Offset,
    halfSize: Float,
    outlineStyle: DrawStyle
) {
    val path = Path().apply {
        moveTo(center.x, center.y - halfSize)
        lineTo(center.x + halfSize, center.y + halfSize)
        lineTo(center.x - halfSize, center.y + halfSize)
        close()
    }

    drawPath(
        path = path,
        style = Fill,
        color = Color.Red
    )
    drawPath(
        path = path,
        style = outlineStyle,
        color = Color.Black
    )
}

fun DrawScope.drawLineShape(
    center: Offset,
    halfSize: Float,
    strokeWidth: Float
) {
    drawLine(
        start = Offset(center.x - halfSize, center.y),
        end = Offset(center.x + halfSize, center.y),
        color = Color.DarkGray,
        strokeWidth = strokeWidth
    )
}

@Composable
fun GraphViewModel.ToolbarButton(
    tool: Tool,
    drawIcon: DrawScope.() -> Unit
) {
    IconButton(onClick = { select(tool) }) {
        Canvas(modifier = Modifier.size(48.dp)) {
            drawIcon()
        }
    }
}
