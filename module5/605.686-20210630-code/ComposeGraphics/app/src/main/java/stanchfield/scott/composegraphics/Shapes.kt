package stanchfield.scott.composegraphics

sealed class Tool
object DrawLine: Tool()
object Select: Tool()
abstract class ShapeType: Tool()
object Circle: ShapeType()
object Square: ShapeType()
object Triangle: ShapeType()

data class Shape(val type: ShapeType, val centerX: Float, val centerY: Float)
data class Line(val end1: Shape, val end2: Shape? = null)
