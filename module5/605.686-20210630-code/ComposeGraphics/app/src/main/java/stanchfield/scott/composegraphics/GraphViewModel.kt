package stanchfield.scott.composegraphics

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

class GraphViewModel: ViewModel() {
    private val shapes0 = MutableStateFlow<List<Shape>>(emptyList())
    val shapes: StateFlow<List<Shape>> get() = shapes0

    private val lines0 = MutableStateFlow<List<Line>>(emptyList())
    val lines: StateFlow<List<Line>> get() = lines0

    private val selectedTool0 = MutableStateFlow<Tool>(Square)
    val selectedTool: StateFlow<Tool> get() = selectedTool0

    fun add(shape: Shape) { shapes0.value = shapes0.value + shape }
    fun add(line: Line) { lines0.value = lines0.value + line }
    fun select(tool: Tool) { selectedTool0.value = tool }
}