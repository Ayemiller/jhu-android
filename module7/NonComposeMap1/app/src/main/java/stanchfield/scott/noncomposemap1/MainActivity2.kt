package stanchfield.scott.noncomposemap1

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import androidx.lifecycle.lifecycleScope
import com.google.android.libraries.maps.CameraUpdateFactory
import com.google.android.libraries.maps.GoogleMap
import com.google.android.libraries.maps.MapView
import com.google.android.libraries.maps.model.LatLng
import com.google.maps.android.ktx.addMarker
import com.google.maps.android.ktx.awaitMap
import kotlinx.coroutines.delay

class MainActivity2 : AppCompatActivity() {
    private lateinit var mapView: MapView
    private lateinit var googleMap: GoogleMap
    private val cameraPosition = LatLng(39.163400392993395, -76.89197363588868)
    private lateinit var mapObserver: LifecycleEventObserver
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mapView = MapView(this)
        mapObserver = LifecycleEventObserver { _, event ->
            when (event) {
                Lifecycle.Event.ON_CREATE -> mapView.onCreate(Bundle())
                Lifecycle.Event.ON_START -> mapView.onStart()
                Lifecycle.Event.ON_RESUME -> mapView.onResume()
                Lifecycle.Event.ON_PAUSE -> mapView.onPause()
                Lifecycle.Event.ON_STOP -> mapView.onStop()
                Lifecycle.Event.ON_DESTROY -> {
                    mapView.onDestroy()
                    lifecycle.removeObserver(mapObserver)
                }
                else -> throw IllegalStateException()
            }
        }
        lifecycle.addObserver(mapObserver)
        mapView.onCreate(savedInstanceState)
        lifecycleScope.launchWhenCreated {
            googleMap = mapView.awaitMap()
            delay(1000)
            googleMap.addMarker {
                position(cameraPosition)
            }
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(cameraPosition, 15f), 1000, null) // MP6
        }
        setContentView(mapView)
    }
}