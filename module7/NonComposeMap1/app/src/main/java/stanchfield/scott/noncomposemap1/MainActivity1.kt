package stanchfield.scott.noncomposemap1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import androidx.lifecycle.lifecycleScope
import com.google.android.libraries.maps.CameraUpdate
import com.google.android.libraries.maps.CameraUpdateFactory
import com.google.android.libraries.maps.GoogleMap
import com.google.android.libraries.maps.MapView
import com.google.android.libraries.maps.model.LatLng
import com.google.maps.android.ktx.addMarker
import com.google.maps.android.ktx.awaitMap
import kotlinx.coroutines.delay

class MainActivity1 : AppCompatActivity() {
    private lateinit var mapView: MapView
    private lateinit var googleMap: GoogleMap
    private val cameraPosition = LatLng(39.163400392993395, -76.89197363588868)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mapView = MapView(this)
        mapView.onCreate(savedInstanceState)
        lifecycleScope.launchWhenCreated {
            googleMap = mapView.awaitMap()
            delay(1000)
            googleMap.addMarker {
                position(cameraPosition)
            }
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(cameraPosition, 15f), 1000, null) // MP6
        }
        setContentView(mapView)
    }

    override fun onStart() {
        super.onStart()
        mapView.onStart()
    }
    override fun onResume() {
        super.onResume()
        mapView.onResume()
    }
    override fun onPause() {
        mapView.onPause()
        super.onPause()
    }
    override fun onStop() {
        mapView.onStop()
        super.onStop()
    }
    override fun onDestroy() {
        mapView.onDestroy()
        super.onDestroy()
    }
}