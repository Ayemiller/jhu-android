package stanchfield.scott.composemap

import android.app.Application
import android.location.Location
import androidx.core.content.edit
import androidx.lifecycle.AndroidViewModel
import androidx.preference.PreferenceManager
import com.google.android.libraries.maps.model.LatLng
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow

private const val LAT_PREF = "lat"
private const val LON_PREF = "lon"

// car icon: <div>Icons made by <a href="https://www.flaticon.com/authors/vectors-market" title="Vectors Market">Vectors Market</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a></div>

class CarViewModel(application: Application) : AndroidViewModel(application) {
    private val currentLocation0 = MutableStateFlow<Location?>(null)
    val currentLocation: Flow<Location?> = currentLocation0

    private val carLatLng0 = MutableStateFlow(
        PreferenceManager.getDefaultSharedPreferences(getApplication()).let {
            it.getString(LAT_PREF, null)?.let { latString ->
                it.getString(LON_PREF, null)?.let { lonString ->
                    LatLng(latString.toDouble(), lonString.toDouble())
                }
            }
        }
    )
    val carLatLng: Flow<LatLng?> = carLatLng0

    fun clearCarLocation() {
        PreferenceManager.getDefaultSharedPreferences(getApplication()).edit {
            remove(LAT_PREF)
            remove(LON_PREF)
        }
        carLatLng0.value = null
    }
    fun setCarLocation(): LatLng? {
        val newLatLng = currentLocation0.value?.let {
            PreferenceManager.getDefaultSharedPreferences(getApplication()).edit {
                putString(LAT_PREF, it.latitude.toString())
                putString(LON_PREF, it.longitude.toString())
            }
            LatLng(it.latitude, it.longitude)
        } ?: run {
            clearCarLocation()
            null
        }

        carLatLng0.value = newLatLng
        return newLatLng
    }

    fun updateCurrentLocation(location: Location) {
        currentLocation0.value = location
    }
}