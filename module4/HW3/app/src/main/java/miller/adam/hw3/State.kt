package miller.adam.hw3

import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

sealed class ContactState
object ContactList : ContactState()
object ContactDisplay : ContactState()
object ContactEdit : ContactState()
object AddressEdit : ContactState()
object About : ContactState()
class StateMachine {
    private val initialState = ContactList
    private val currentState0 = MutableStateFlow<ContactState?>(initialState)
    private var stack = listOf<ContactState>(initialState)

    val currentState: StateFlow<ContactState?> = currentState0

    fun push(state: ContactState) {
        stack = stack + state
        currentState0.value = state
    }

    fun peek() = stack.lastOrNull()
    fun pop() {
        stack = stack.dropLast(1)
        currentState0.value = peek()
    }
}
