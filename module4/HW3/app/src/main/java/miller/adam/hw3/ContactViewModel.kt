package miller.adam.hw3

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.withContext
import miller.adam.hw3.data.Address
import miller.adam.hw3.data.Contact
import miller.adam.hw3.data.ContactAndAddresses
import miller.adam.hw3.data.ContactDatabase

// code inspired by lecture from ::MovieViewModel from Module 4
class ContactViewModel(application: Application) : AndroidViewModel(application) {
    private val db = Room.databaseBuilder(application, ContactDatabase::class.java, "MOVIES")
        .addCallback(object : RoomDatabase.Callback() {
            override fun onCreate(db: SupportSQLiteDatabase) {
                super.onCreate(db)
                db.execSQL("INSERT INTO Contact (id, firstName, lastName, homePhone, workPhone, mobilePhone, email) VALUES('c1', 'Jane', 'Doe', '123-456-7890', 'x3933', '555-555-1234', 'jane.doe@mail.com')")
                db.execSQL("INSERT INTO Contact (id, firstName, lastName, homePhone, workPhone, mobilePhone, email) VALUES('c2', 'John', 'Doe', '789-812-3456', 'x9399', '444-444-1234', 'john.doe@mail.com')")
                db.execSQL("INSERT INTO Contact (id, firstName, lastName, homePhone, workPhone, mobilePhone, email) VALUES('c3', 'Jake', 'Doe', '890-123-4567', 'x9909', '554-454-1234', 'jake.doe@mail.com')")
                db.execSQL("INSERT INTO Contact (id, firstName, lastName, homePhone, workPhone, mobilePhone, email) VALUES('c4', 'Alpha', 'Doe', '890-123-4567', 'x9909', '554-454-1234', 'jake.doe@mail.com')")
                db.execSQL("INSERT INTO Contact (id, firstName, lastName, homePhone, workPhone, mobilePhone, email) VALUES('c5', 'Charlie', 'Doe', '890-123-4567', 'x9909', '554-454-1234', 'jake.doe@mail.com')")
                db.execSQL("INSERT INTO Contact (id, firstName, lastName, homePhone, workPhone, mobilePhone, email) VALUES('c6', 'Bravo', 'Doe', '890-123-4567', 'x9909', '554-454-1234', 'jake.doe@mail.com')")
                db.execSQL("INSERT INTO Contact (id, firstName, lastName, homePhone, workPhone, mobilePhone, email) VALUES('c7', 'Jake', 'Dod', '890-123-4567', 'x9909', '554-454-1234', 'jake.doe@mail.com')")
                db.execSQL("INSERT INTO Contact (id, firstName, lastName, homePhone, workPhone, mobilePhone, email) VALUES('c8', 'Echo', 'Doe', '890-123-4567', 'x9909', '554-454-1234', 'jake.doe@mail.com')")
                db.execSQL("INSERT INTO Contact (id, firstName, lastName, homePhone, workPhone, mobilePhone, email) VALUES('c9', 'Jake', 'Doc', '890-123-4567', 'x9909', '554-454-1234', 'jake.doe@mail.com')")
                db.execSQL("INSERT INTO Contact (id, firstName, lastName, homePhone, workPhone, mobilePhone, email) VALUES('c10', 'Jake', 'Dne', '890-123-4567', 'x9909', '554-454-1234', 'jake.doe@mail.com')")

                db.execSQL("INSERT INTO Address (id, contactId, type, street, city, state, zip) VALUES('a1', 'c1', 'home', '11 Walnut Street', 'Baltimore', 'Maryland', '21204')")
                db.execSQL("INSERT INTO Address (id, contactId, type, street, city, state, zip) VALUES('a2', 'c1', 'work', '22 Walnut Street', 'Baltimore', 'Maryland', '21205')")
                db.execSQL("INSERT INTO Address (id, contactId, type, street, city, state, zip) VALUES('a3', 'c2', 'home', '33 Fir Street', 'Baltimore', 'Maryland', '21206')")
                db.execSQL("INSERT INTO Address (id, contactId, type, street, city, state, zip) VALUES('a4', 'c2', 'work', '44 Chestnut Street', 'Baltimore', 'Maryland', '21207')")
                db.execSQL("INSERT INTO Address (id, contactId, type, street, city, state, zip) VALUES('a5', 'c3', 'home', '55 Elm Street', 'Baltimore', 'Maryland', '21208')")
                db.execSQL("INSERT INTO Address (id, contactId, type, street, city, state, zip) VALUES('a6', 'c3', 'work', '22 H Street', 'Baltimore', 'Maryland', '21209')")
            }
        })
        .build()

    private val stateMachine = StateMachine()
    val currentState: StateFlow<ContactState?> = stateMachine.currentState

    // return Flow<List<Movie>> from the view model (which gets them from the database
    // exposed as functions to be consistent with the functions defined below that
    val allContacts = db.dao.allContactsAsync()

    // currently selected contactId and addressId in the UI
    val selectedContactIdFlow = MutableStateFlow<String?>(null)
    val selectedAddressIdFlow = MutableStateFlow<String?>(null)

    // when the selected contact id changes, fetch an actual movie object
    @ExperimentalCoroutinesApi
    val selectedContact: Flow<ContactAndAddresses?>
        get() =
            selectedContactIdFlow.flatMapLatest {
                if (it == null) {
                    flow { emit(null) }
                } else {
                    db.dao.contactAndAddresses(it)
                }
            }

    // when the selected address id changes, fetch an actual movie object
    @ExperimentalCoroutinesApi
    val selectedAddress: Flow<Address?>
        get() =
            selectedAddressIdFlow.flatMapLatest {
                if (it == null) {
                    flow { emit(null) }
                } else {
                    db.dao.getAddressAsync(it)
                }
            }


    suspend fun deleteContact(id: String) = withContext(Dispatchers.IO) {
        db.dao.deleteContact(id)
    }

    /**
     * Insert the provided [Contact] into the database
     */
    suspend fun insert(contact: Contact) = withContext(Dispatchers.IO) {
        db.dao.insert(contact)
    }

    /**
     * Insert the provided [Address] into the database
     */
    suspend fun insert(address: Address) = withContext(Dispatchers.IO) {
        db.dao.insert(address)
    }

    suspend fun deleteAddress(addressId: String) = withContext(Dispatchers.IO) {
        db.dao.deleteAddress(addressId)
    }

    suspend fun deleteSelectedContact(contactId: String) = withContext(Dispatchers.IO) {
        db.dao.deleteContact(contactId)
    }

    fun pushState(state: ContactState) {
        stateMachine.push(state)
    }

    fun popState() = stateMachine.pop()
    fun peekState() = stateMachine.peek()
    suspend fun createContact() = withContext(Dispatchers.IO) {
        Contact().apply {
            selectedContactIdFlow.value = null
            insert(this)
            selectedContactIdFlow.value = this.id

        }
    }

    suspend fun createAddress(contactId: String) = withContext(Dispatchers.IO) {
        Address().apply {
            selectedAddressIdFlow.value = null
            this.contactId = contactId
            insert(this)
            selectedAddressIdFlow.value = this.id
        }
    }

    suspend fun onContactChanged(contact: Contact) = withContext(Dispatchers.IO) {
        db.dao.update(contact)
        selectedContactIdFlow.value = contact.id
    }

    suspend fun onAddressChanged(address: Address) = withContext(Dispatchers.IO) {
        db.dao.update(address)
        selectedAddressIdFlow.value = address.id
    }
}