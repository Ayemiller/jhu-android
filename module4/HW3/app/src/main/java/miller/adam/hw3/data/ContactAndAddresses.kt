package miller.adam.hw3.data

import androidx.room.Embedded
import androidx.room.Relation

data class ContactAndAddresses(
    @Embedded val contact: Contact,
    @Relation(
        parentColumn = "id",
        entityColumn = "contactId"
    )
    val addresses: List<Address>
)
