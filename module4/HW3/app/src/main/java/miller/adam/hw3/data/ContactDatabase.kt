package miller.adam.hw3.data

import androidx.room.Database
import androidx.room.RoomDatabase

// code inspired by DatabaseSample::Database from Module 2
@Database(version = 1, entities = [Contact::class, Address::class], exportSchema = false)
abstract class ContactDatabase : RoomDatabase() {
    abstract val dao: ContactDao
}