package miller.adam.hw3

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CornerSize
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material.icons.filled.Edit
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.capitalize
import androidx.compose.ui.text.intl.Locale
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch
import miller.adam.hw3.data.Address
import miller.adam.hw3.data.Contact
import miller.adam.hw3.data.ContactAndAddresses
import miller.adam.hw3.ui.theme.HW3Theme

// code inspired by lecture from ::MainActivity from Module 4
class MainActivity : ComponentActivity() {
    private val viewModel by viewModels<ContactViewModel>()

    @ExperimentalCoroutinesApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            HW3Theme {
                val scope = rememberCoroutineScope()

                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colors.background) {
                    Ui(
                        allContactsFlow = viewModel.allContacts,
                        currentStateFlow = viewModel.currentState,
                        selectedContactFlow = viewModel.selectedContact,
                        selectedAddressFlow = viewModel.selectedAddress,
                        onContactSelectionChanged = {
                            viewModel.selectedContactIdFlow.value = it
                        },
                        onAddressSelectionChanged = {
                            viewModel.selectedAddressIdFlow.value = it
                        },
                        onDeleteSelectedContact = {
                            scope.launch {
                                viewModel.deleteSelectedContact(it)
                                // TODO really should NOT be cancelable...
                            }
                        },
                        deleteAddress = {
                            scope.launch {
                                viewModel.deleteAddress(it)
                            }
                        },
                        onContactChanged = {
                            scope.launch {
                                viewModel.onContactChanged(it)
                            }
                        },
                        onAddressChanged = {
                            scope.launch {
                                viewModel.onAddressChanged(it)
                            }
                        },
                        pushState = viewModel::pushState,
                        createContact = {
                            scope.launch {
                                viewModel.createContact()
                            }
                        },
                        createAddress = {
                            scope.launch {
                                viewModel.createAddress(it)
                            }
                        }
                    )
                }
            }
        }
    }

    override fun onBackPressed() {
        viewModel.popState()
        if (viewModel.peekState() == null) {
            finish()
        }
    }
}

@Composable
fun Ui(
    allContactsFlow: Flow<List<Contact>>,
    currentStateFlow: Flow<ContactState?>,
    selectedContactFlow: Flow<ContactAndAddresses?>,
    selectedAddressFlow: Flow<Address?>,
    onContactSelectionChanged: (String) -> Unit,
    onAddressSelectionChanged: (String) -> Unit,
    onDeleteSelectedContact: (String) -> Unit,
    onContactChanged: (Contact) -> Unit,
    onAddressChanged: (Address) -> Unit,
    pushState: (ContactState) -> Unit,
    createContact: () -> Unit,
    createAddress: (String) -> Unit,
    deleteAddress: (String) -> Unit
) {
    val allContacts by allContactsFlow.collectAsState(initial = emptyList())
    val currentState by currentStateFlow.collectAsState(initial = ContactList)
    val selectedContact by selectedContactFlow.collectAsState(initial = null)
    val selectedAddress by selectedAddressFlow.collectAsState(initial = null)

    when (currentState) {
        ContactList ->
            ContactListScreen(
                contacts = allContacts,
                onContactSelectionChanged = {
                    onContactSelectionChanged(it)
                    pushState(ContactDisplay)
                },
                onCreateAbout = {
                    pushState(About)
                },
                onCreateContact = {
                    createContact()
                    pushState(ContactEdit)
                }
            )
        ContactDisplay ->
            ContactDisplayScreen(
                contact = selectedContact,
                onEditContact = {
                    pushState(ContactEdit)
                },
                onCreateAbout = { pushState(About) }
            )
        ContactEdit ->
            ContactEditScreen(
                contact = selectedContact,
                onContactChanged = onContactChanged,
                onCreateAbout = { pushState(About) },
                onAddAddress = {
                    createAddress(it)
                    pushState(AddressEdit)
                },
                onAddressSelectionChanged = {
                    onAddressSelectionChanged(it)
                    pushState(AddressEdit)
                },
                onDeleteAddress = {
                    deleteAddress(it)
                }
            )


        AddressEdit ->
            AddressEditScreen(
                contact = selectedContact,
                address = selectedAddress,
                onAddressChanged = onAddressChanged,
                onCreateAbout = { pushState(About) }
            )
        About ->
            AboutScreen()
    }

}

@Composable
fun ContactListScreen(
    contacts: List<Contact>,
    onContactSelectionChanged: (String) -> Unit,
    onCreateContact: () -> Unit,
    onCreateAbout: () -> Unit,
) = Scaffold(
    topBar = {
        TopAppBar(
            title = { Text(stringResource(id = R.string.contacts)) },
            actions = {
                CreateAboutAction(onCreateAbout = onCreateAbout)
                CreateContactAction(onCreateContact = onCreateContact)
            }
        )
    },
    content = {

        LazyColumn {
            items(
                items = contacts,
                key = { it.id } // ensures equivalent items in new/old lists are matched up properly
            ) { contact ->
                Card(
                    elevation = 2.dp,
                    backgroundColor = MaterialTheme.colors.surface,
                    shape = RoundedCornerShape(corner = CornerSize(16.dp)),
                    modifier = Modifier
                        .padding(8.dp)
                        .fillMaxWidth()
                        .clickable {
                            onContactSelectionChanged(contact.id)
                        }
                ) {
                    Row(verticalAlignment = Alignment.CenterVertically) {
                        Image(
                            painter = painterResource(id = R.drawable.ic_baseline_person_24),
                            colorFilter = ColorFilter.tint(MaterialTheme.colors.onSurface),
                            contentDescription = stringResource(id = R.string.contact_icon),
                            modifier = Modifier
                                .padding(8.dp)
                                .size(48.dp)
                        )
                        //display Lastname, Firstname above and mobile phone number below
                        Column {
                            Text(
                                text = getFullName(contact.lastName, contact.firstName) ?: "",
                                color = MaterialTheme.colors.onSurface
                            )
                            Spacer(modifier = Modifier.padding(8.dp))
                            Text(
                                text = contact.mobilePhone,
                                color = MaterialTheme.colors.onSurface
                            )
                        }
                    }

                }
            }
        }
    }
)


@Composable
fun ContactDisplayScreen(
    contact: ContactAndAddresses?,
    onEditContact: () -> Unit,
    onCreateAbout: () -> Unit,
) = Scaffold(
    topBar = {
        TopAppBar(
            title = {
                Text(
                    getFullName(contact?.contact?.lastName, contact?.contact?.firstName)
                        ?: stringResource(id = R.string.nothing_selected)
                )
            },
            actions = {
                CreateAboutAction(onCreateAbout = onCreateAbout)
                contact?.let {
                    EditContactAction(onEditContact = onEditContact)
                }
            }
        )
    },
    content = {
        Column(
            modifier = Modifier
                .verticalScroll(rememberScrollState())
                .fillMaxWidth()
        ) {
            Text(
                text = stringResource(id = R.string.first_name),
                style = MaterialTheme.typography.h6,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(8.dp)
            )
            Text(
                text = contact?.contact?.firstName ?: "",
                style = MaterialTheme.typography.h5,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(start = 16.dp, end = 8.dp, top = 8.dp, bottom = 8.dp)
            )
            Text(
                text = stringResource(id = R.string.last_name),
                style = MaterialTheme.typography.h6,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(8.dp)
            )
            Text(
                text = contact?.contact?.lastName ?: "",
                style = MaterialTheme.typography.h5,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(start = 16.dp, end = 8.dp, top = 8.dp, bottom = 8.dp)
            )
            Text(
                text = stringResource(id = R.string.home_phone),
                style = MaterialTheme.typography.h6,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(8.dp)
            )
            Text(
                text = contact?.contact?.homePhone ?: "",
                style = MaterialTheme.typography.h5,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(start = 16.dp, end = 8.dp, top = 8.dp, bottom = 8.dp)
            )
            Text(
                text = stringResource(id = R.string.work_phone),
                style = MaterialTheme.typography.h6,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(8.dp)
            )
            Text(
                text = contact?.contact?.workPhone ?: "",
                style = MaterialTheme.typography.h5,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(start = 16.dp, end = 8.dp, top = 8.dp, bottom = 8.dp)
            )
            Text(
                text = stringResource(id = R.string.mobile_phone),
                style = MaterialTheme.typography.h6,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(8.dp)
            )
            Text(
                text = contact?.contact?.mobilePhone ?: "",
                style = MaterialTheme.typography.h5,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(start = 16.dp, end = 8.dp, top = 8.dp, bottom = 8.dp)
            )
            Text(
                text = stringResource(id = R.string.email_address),
                style = MaterialTheme.typography.h6,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(8.dp)
            )
            Text(
                text = contact?.contact?.email ?: "",
                style = MaterialTheme.typography.h5,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(start = 16.dp, end = 8.dp, top = 8.dp, bottom = 8.dp)
            )


            //display each address...
            Card(modifier = Modifier.fillMaxWidth()) {
                Row(verticalAlignment = Alignment.CenterVertically) {
                    Column {
                        Text(
                            text = stringResource(id = R.string.addresses),
                            style = MaterialTheme.typography.h6,
                            modifier = Modifier
                                .padding(8.dp)
                        )
                    }
                }
            }

            contact?.addresses?.forEach { address ->
                Card(modifier = Modifier.fillMaxWidth()) {
                    Row(
                        verticalAlignment = Alignment.CenterVertically,
                        modifier = Modifier.fillMaxWidth()
                    ) {
                        Column(horizontalAlignment = Alignment.Start) {
                            Text(
                                text = printAddress(address),
                                style = MaterialTheme.typography.h6,
                                modifier = Modifier
                                    .padding(8.dp)
                                    .align(Alignment.Start)
                            )
                        }
                    }
                }
            }
        }
    }
)

/**
 * Helper "toString" function for [Address]
 */
fun printAddress(address: Address): String {
    return "(${address.type}) ${address.street}\n${address.city}, ${address.state} ${address.zip}"
}

/**
 * Helper "toString" function for [Contact] which returns Lastname, Firstname
 */
fun getFullName(lastName: String?, firstName: String?): String? {
    var fullName: String? = lastName
    if (firstName != "") {
        if (lastName != "") {
            fullName += ", "
        }
        fullName += firstName
    }
    return fullName
}

@Composable
fun EditContactAction(
    onEditContact: () -> Unit
) {
    IconButton(onClick = onEditContact) {
        Icon(Icons.Filled.Edit, contentDescription = stringResource(id = R.string.edit_contact))
    }
}

@Composable
fun AddAddressAction(
    onAddAddress: () -> Unit
) {
    IconButton(onClick = onAddAddress) {
        Icon(
            Icons.Filled.Add,
            contentDescription = stringResource(id = R.string.add_address),
            tint = Color.Green
        )
    }
}

@Composable
fun DeleteAddressAction(
    onDeleteAddress: () -> Unit
) {
    IconButton(onClick = onDeleteAddress, modifier = Modifier.size(48.dp)) {
        Icon(
            Icons.Filled.Delete,
            contentDescription = stringResource(id = R.string.delete_address),
            tint = Color.Red
        )
    }
}

@Composable
fun CreateAboutAction(onCreateAbout: () -> Unit) {
    IconButton(onClick = onCreateAbout) {
        Icon(
            painterResource(id = R.drawable.ic_baseline_error_24),
            contentDescription = stringResource(id = R.string.create_about)
        )
    }
}

@Composable
fun CreateContactAction(onCreateContact: () -> Unit) {
    IconButton(onClick = onCreateContact) {
        Icon(Icons.Filled.Add, contentDescription = stringResource(id = R.string.create_contact))
    }
}

@Composable
fun DeleteContactAction(
    onDeleteSelectedContact: () -> Unit
) {
    IconButton(onClick = onDeleteSelectedContact) {
        Icon(
            Icons.Filled.Delete,
            contentDescription = stringResource(id = R.string.delete_selected_contacts)
        )
    }
}

@Composable
fun ContactEditScreen(
    contact: ContactAndAddresses?,
    onContactChanged: (Contact) -> Unit,
    onCreateAbout: () -> Unit,
    onAddAddress: (String) -> Unit,
    onAddressSelectionChanged: (String) -> Unit,
    onDeleteAddress: (String) -> Unit,
) = Scaffold(
    topBar = {
        TopAppBar(
            title = {
                Text(
                    if (getFullName(
                            contact?.contact?.lastName,
                            contact?.contact?.firstName
                        ).isNullOrEmpty()
                    ) {
                        stringResource(id = R.string.no_title)
                    } else {
                        (getFullName(contact?.contact?.lastName, contact?.contact?.firstName))
                            ?: stringResource(id = R.string.nothing_selected)
                    }
                )
            },
            actions = {
                CreateAboutAction(onCreateAbout = onCreateAbout)
            }
        )
    },
    content = {
        var firstName by remember { mutableStateOf(contact?.contact?.firstName) }
        var lastName by remember { mutableStateOf(contact?.contact?.lastName) }
        var homePhone by remember { mutableStateOf(contact?.contact?.homePhone) }
        var workPhone by remember { mutableStateOf(contact?.contact?.workPhone) }
        var mobilePhone by remember { mutableStateOf(contact?.contact?.mobilePhone) }
        var email by remember { mutableStateOf(contact?.contact?.email) }
        Column(
            modifier = Modifier.verticalScroll(rememberScrollState())
        ) {
            TextField(
                value = firstName ?: "",
                label = { Text(text = stringResource(id = R.string.first_name)) },
                onValueChange = { value ->
                    contact?.let { contact ->
                        firstName = value
                        onContactChanged(
                            Contact(
                                id = contact.contact.id,
                                lastName = lastName ?: "",
                                firstName = firstName ?: "",
                                homePhone = homePhone ?: "",
                                workPhone = workPhone ?: "",
                                mobilePhone = mobilePhone ?: "",
                                email = email ?: ""
                            )
                        )
                    }
                },
                modifier = Modifier
                    .padding(8.dp)
                    .fillMaxWidth()
            )
            TextField(
                value = lastName ?: "",
                label = { Text(text = stringResource(id = R.string.last_name)) },
                onValueChange = { value ->
                    contact?.let { contact ->
                        lastName = value
                        onContactChanged(
                            Contact(
                                id = contact.contact.id,
                                lastName = lastName ?: "",
                                firstName = firstName ?: "",
                                homePhone = homePhone ?: "",
                                workPhone = workPhone ?: "",
                                mobilePhone = mobilePhone ?: "",
                                email = email ?: ""
                            )
                        )
                    }
                },
                modifier = Modifier
                    .padding(8.dp)
                    .fillMaxWidth()
            )
            TextField(
                value = homePhone ?: "",
                label = { Text(text = stringResource(id = R.string.home_phone)) },
                onValueChange = { value ->
                    contact?.let { contact ->
                        homePhone = value
                        onContactChanged(
                            Contact(
                                id = contact.contact.id,
                                lastName = lastName ?: "",
                                firstName = firstName ?: "",
                                homePhone = homePhone ?: "",
                                workPhone = workPhone ?: "",
                                mobilePhone = mobilePhone ?: "",
                                email = email ?: ""
                            )
                        )
                    }
                },
                modifier = Modifier
                    .padding(8.dp)
                    .fillMaxWidth()
            )
            TextField(
                value = workPhone ?: "",
                label = { Text(text = stringResource(id = R.string.work_phone)) },
                onValueChange = { value ->
                    contact?.let { contact ->
                        workPhone = value
                        onContactChanged(
                            Contact(
                                id = contact.contact.id,
                                lastName = lastName ?: "",
                                firstName = firstName ?: "",
                                homePhone = homePhone ?: "",
                                workPhone = workPhone ?: "",
                                mobilePhone = mobilePhone ?: "",
                                email = email ?: ""
                            )
                        )
                    }
                },
                modifier = Modifier
                    .padding(8.dp)
                    .fillMaxWidth()
            )
            TextField(
                value = mobilePhone ?: "",
                label = { Text(text = stringResource(id = R.string.mobile_phone)) },
                onValueChange = { value ->
                    contact?.let { contact ->
                        mobilePhone = value
                        onContactChanged(
                            Contact(
                                id = contact.contact.id,
                                lastName = lastName ?: "",
                                firstName = firstName ?: "",
                                homePhone = homePhone ?: "",
                                workPhone = workPhone ?: "",
                                mobilePhone = mobilePhone ?: "",
                                email = email ?: ""
                            )
                        )
                    }
                },
                modifier = Modifier
                    .padding(8.dp)
                    .fillMaxWidth()
            )
            TextField(
                value = email ?: "",
                label = { Text(text = stringResource(id = R.string.email_address)) },
                onValueChange = { value ->
                    contact?.let { contact ->
                        email = value
                        onContactChanged(
                            Contact(
                                id = contact.contact.id,
                                lastName = lastName ?: "",
                                firstName = firstName ?: "",
                                homePhone = homePhone ?: "",
                                workPhone = workPhone ?: "",
                                mobilePhone = mobilePhone ?: "",
                                email = email ?: ""
                            )
                        )
                    }
                },
                modifier = Modifier
                    .padding(8.dp)
                    .fillMaxWidth()
            )
            //display each address...
            Card(modifier = Modifier.fillMaxWidth()) {
                Row(verticalAlignment = Alignment.CenterVertically) {
                    Column {
                        Text(
                            text = stringResource(id = R.string.addresses),
                            style = MaterialTheme.typography.h6,
                            modifier = Modifier
                                .padding(8.dp)
                        )
                    }
                    Column(horizontalAlignment = Alignment.End, modifier = Modifier.fillMaxSize()) {

                        contact?.let {
                            AddAddressAction(onAddAddress = {
                                onAddAddress(it.contact.id)
                            })
                        }
                    }
                }
            }

            contact?.addresses?.forEach { address ->
                Card(modifier = Modifier.fillMaxWidth()) {
                    Row(
                        verticalAlignment = Alignment.CenterVertically,
                        modifier = Modifier.fillMaxWidth()
                    ) {
                        Column(horizontalAlignment = Alignment.Start) {
                            Text(
                                text = printAddress(address),
                                style = MaterialTheme.typography.h6,
                                modifier = Modifier
                                    .padding(8.dp)
                                    .align(Alignment.Start)
                                    .clickable {
                                        onAddressSelectionChanged(address.id)
                                    }
                            )
                        }
                        Column(
                            horizontalAlignment = Alignment.End,
                            modifier = Modifier.fillMaxSize()
                        ) {
                            DeleteAddressAction(onDeleteAddress = {
                                onDeleteAddress(address.id)
                            })
                        }
                    }
                }
            }

        }
    }
)

@Composable
fun AddressEditScreen(
    contact: ContactAndAddresses?,
    address: Address?,
    onAddressChanged: (Address) -> Unit,
    onCreateAbout: () -> Unit
) = Scaffold(
    topBar = {
        TopAppBar(
            title = {
                Text(
                    if (address?.type.isNullOrEmpty()) {
                        stringResource(id = R.string.no_title)
                    } else {
                        address?.type?.capitalize(Locale.current) + " " + stringResource(id = R.string.address)
                    }
                )
            },
            actions = {
                CreateAboutAction(onCreateAbout = onCreateAbout)
            }
        )
    },
    content = {
        var type by remember { mutableStateOf(address?.type) }
        var street by remember { mutableStateOf(address?.street) }
        var city by remember { mutableStateOf(address?.city) }
        var state by remember { mutableStateOf(address?.state) }
        var zip by remember { mutableStateOf(address?.zip) }
        val contactId by remember { mutableStateOf(contact?.contact?.id) }
        Column(
            modifier = Modifier.verticalScroll(rememberScrollState())
        ) {
            TextField(
                value = type ?: "",
                label = { Text(text = stringResource(id = R.string.type)) },
                onValueChange = { value ->
                    address?.let { address ->
                        type = value
                        onAddressChanged(
                            Address(
                                id = address.id,
                                contactId = contactId ?: "",
                                type = type ?: "",
                                street = street ?: "",
                                city = city ?: "",
                                state = state ?: "",
                                zip = zip ?: ""
                            )
                        )
                    }
                },
                modifier = Modifier
                    .padding(8.dp)
                    .fillMaxWidth()
            )
            TextField(
                value = street ?: "",
                label = { Text(text = stringResource(id = R.string.street)) },
                onValueChange = { value ->
                    address?.let { address ->
                        street = value
                        onAddressChanged(
                            Address(
                                id = address.id,
                                contactId = contactId ?: "",
                                type = type ?: "",
                                street = street ?: "",
                                city = city ?: "",
                                state = state ?: "",
                                zip = zip ?: ""
                            )
                        )
                    }
                },
                modifier = Modifier
                    .padding(8.dp)
                    .fillMaxWidth()
            )
            TextField(
                value = city ?: "",
                label = { Text(text = stringResource(id = R.string.city)) },
                onValueChange = { value ->
                    address?.let { address ->
                        city = value
                        onAddressChanged(
                            Address(
                                id = address.id,
                                contactId = contactId ?: "",
                                type = type ?: "",
                                street = street ?: "",
                                city = city ?: "",
                                state = state ?: "",
                                zip = zip ?: ""
                            )
                        )
                    }
                },
                modifier = Modifier
                    .padding(8.dp)
                    .fillMaxWidth()
            )
            TextField(
                value = state ?: "",
                label = { Text(text = stringResource(id = R.string.state)) },
                onValueChange = { value ->
                    address?.let { address ->
                        state = value
                        onAddressChanged(
                            Address(
                                id = address.id,
                                contactId = contactId ?: "",
                                type = type ?: "",
                                street = street ?: "",
                                city = city ?: "",
                                state = state ?: "",
                                zip = zip ?: ""
                            )
                        )
                    }
                },
                modifier = Modifier
                    .padding(8.dp)
                    .fillMaxWidth()
            )
            TextField(
                value = zip ?: "",
                label = { Text(text = stringResource(id = R.string.zip)) },
                onValueChange = { value ->
                    address?.let { address ->
                        zip = value
                        onAddressChanged(
                            Address(
                                id = address.id,
                                contactId = contactId ?: "",
                                type = type ?: "",
                                street = street ?: "",
                                city = city ?: "",
                                state = state ?: "",
                                zip = zip ?: ""
                            )
                        )
                    }
                },
                modifier = Modifier
                    .padding(8.dp)
                    .fillMaxWidth()
            )
        }
    }
)

@Composable
fun AboutScreen() = Scaffold(
    topBar = {
        TopAppBar(
            title = { Text(stringResource(id = R.string.about)) }
        )
    },
    content = {
        Row(verticalAlignment = Alignment.CenterVertically, modifier = Modifier.fillMaxSize()) {
            Column(
                horizontalAlignment = Alignment.CenterHorizontally,
                modifier = Modifier
                    .verticalScroll(rememberScrollState())
                    .fillMaxSize()
            ) {
                Text(
                    text = stringResource(id = R.string.about_info),
                    textAlign = TextAlign.Center,
                    style = MaterialTheme.typography.h5,
                    modifier = Modifier
                        .padding(8.dp)
                        .fillMaxWidth()
                )
            }
        }
    }
)

