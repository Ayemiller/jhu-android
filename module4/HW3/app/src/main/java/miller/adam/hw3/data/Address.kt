package miller.adam.hw3.data

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import java.util.*

// code inspired by lecture from Module 4
@Entity(
    foreignKeys = [
        ForeignKey(
            entity = Contact::class,
            parentColumns = ["id"],
            childColumns = ["contactId"],
            onUpdate = ForeignKey.CASCADE,
            onDelete = ForeignKey.CASCADE
        )
    ]
)
data class Address(
    @PrimaryKey var id: String = UUID.randomUUID().toString(),
    var contactId: String = "",
    var type: String = "",
    var street: String = "",
    var city: String = "",
    var state: String = "",
    var zip: String = ""
) {

}
