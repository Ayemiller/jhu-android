package stanchfield.scott.composemoviedatabase.data

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.UUID

@Entity
data class Movie(
    @PrimaryKey var id: String = UUID.randomUUID().toString(),
    var title : String = "",
    var description : String = ""

// toString
// equals
// hashCode
// copy
// component1, component2...
)