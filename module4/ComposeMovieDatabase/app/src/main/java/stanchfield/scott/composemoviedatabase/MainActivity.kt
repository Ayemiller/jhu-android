package stanchfield.scott.composemoviedatabase

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.gestures.detectTapGestures
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CornerSize
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Card
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material.icons.filled.Edit
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import stanchfield.scott.composemoviedatabase.data.Movie
import stanchfield.scott.composemoviedatabase.ui.theme.ComposeMovieDatabaseTheme


class MainActivity : ComponentActivity() {
    private val viewModel by viewModels<MovieViewModel>()

    @ExperimentalCoroutinesApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ComposeMovieDatabaseTheme {
                val scope = rememberCoroutineScope()

                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colors.background) {
                    Ui(
                        allMoviesFlow = viewModel.allMovies,
                        currentStateFlow = viewModel.currentState,
                        selectedMovieFlow = viewModel.selectedMovie,
                        selectionFlow = viewModel.selection,
                        onSelectionChanged = {
                            viewModel.selection.value = it
                        },
                        onDeleteSelectedMovies = {
                            scope.launch {
                                viewModel.deleteSelectedMovies()
                                // TODO really should NOT be cancelable...
                            }
                        },
                        onMovieChanged = {
                            scope.launch {
                                viewModel.onMovieChanged(it)
                            }
                        },
                        pushState = viewModel::pushState,
                        createMovie = {
                            scope.launch {
                                viewModel.createMovie()
                            }
                        }
                    )
                }
            }
        }
    }

    override fun onBackPressed() {
        viewModel.popState()
        if (viewModel.peekState() == null) {
            finish()
        }
    }
}

@Composable
fun Ui(
    allMoviesFlow: Flow<List<Movie>>,
    currentStateFlow: Flow<MovieState?>,
    selectedMovieFlow: Flow<Movie?>,
    selectionFlow: Flow<Selection>,
    onSelectionChanged: (Selection) -> Unit,
    onDeleteSelectedMovies: () -> Unit,
    onMovieChanged: (Movie) -> Unit,
    pushState: (MovieState) -> Unit,
    createMovie: () -> Unit
) {
    val allMovies by allMoviesFlow.collectAsState(initial = emptyList())
    val currentState by currentStateFlow.collectAsState(initial = MovieList)
    val selectedMovie = MutableStateFlow<Movie?>(selectedMovieFlow.collectAsState(initial=null).value)
    //val selectedMovie by selectedMovieFlow.collectAsState(initial = null)
    val selection by selectionFlow.collectAsState(initial = Selection())

    when (currentState) {
        MovieList ->
            MovieListScreen(
                movies = allMovies,
                selection = selection,
                onSelectionChanged = {
                    onSelectionChanged(it)
                    if (!it.multiSelectMode && it.selections.isNotEmpty()) {
                        pushState(MovieDisplay)
                    }
                },
                onCreateMovie = {
                    createMovie()
                    selectedMovie.value=null
                    pushState(MovieEdit)
                },
                onDeleteSelectedMovies = onDeleteSelectedMovies
            )
        MovieDisplay ->
            MovieDisplayScreen(
                movie = selectedMovie.value,
                onEditMovie = {
                    pushState(MovieEdit)
                }
            )
        MovieEdit ->
            MovieEditScreen(
                movie = selectedMovie.value,
                onMovieChanged = onMovieChanged
            )
    }

}

@Composable
fun MovieListScreen(
    movies: List<Movie>,
    selection: Selection,
    onSelectionChanged: (Selection) -> Unit,
    onCreateMovie: () -> Unit,
    onDeleteSelectedMovies: () -> Unit
) = Scaffold(
    topBar = {
        if (selection.multiSelectMode) {
             TopAppBar(
                 navigationIcon = {
                     IconButton(onClick = {
                         onSelectionChanged(Selection())
                     }) {
                         Icon(Icons.Filled.ArrowBack, stringResource(id = R.string.cancel_multi_select))
                     }
                 },
                 title = { Text(selection.selections.size.toString()) },
                 actions = {
                     DeleteMovieAction(onDeleteSelectedMovies = onDeleteSelectedMovies)
                 }
             )
        } else {
             TopAppBar(
                 title = { Text(stringResource(id = R.string.movies)) },
                 actions = {
                     CreateMovieAction(onCreateMovie = onCreateMovie)
                 }
             )
        }
    },
    content = {

        // nasty hack to get the selection working properly inside the detectTapGestures call.
        // looks like detectTapGestures is capturing the selection passed in inside its lambdas
        //   and thinking it never needs to recompose to update the lambdas. I can't see a way to
        //   trigger it to recompose, so instead of having it directly use selection, I store it in
        //   a mutable state bucket, and use that bucket via remember to force the update, and
        //   every time the list is updated, put the new selection in the bucket
        var selectionX by remember { mutableStateOf(selection) }
        selectionX = selection

        // an alternate approach that I briefly tried but tossed:
        //    manage the selection here using rememberSaveable(...) which will save the
        //       selection across configuration changes
        //    make Selection implement Parcelable so it can be saved
        //    don't pass the selection down into the composable functions
        //    update the selection here using the toggle and multiToggle functions
        //    pass the new selection to onSelectionChanged so the view model knows about it
        //       note - the view model still has a selection flow so it can drive fetching the
        //              selected movie from the database. I'm not keen on that as it means the
        //              selection is tracked in two places
        // this worked, but felt even more awkward than the hack. I do however like the idea of
        //   having the selection stored only inside this function (via rememberSaveable) and
        //   only pass out its data to onSelectionChanged - didn't want to try that right now as
        //   the hack is working ok

        LazyColumn {
            items(
                items = movies,
                key = { it.id } // ensures equivalent items in new/old lists are matched up properly
            ) { movie ->
                Card(
                    elevation = 2.dp,
                    backgroundColor =
                        if (selection.multiSelectMode && movie.id in selection.selections) {
                            MaterialTheme.colors.primary
                        } else {
                            MaterialTheme.colors.surface
                        },
                    shape = RoundedCornerShape(corner = CornerSize(16.dp)),
                    modifier = Modifier
                        .padding(8.dp)
                        .fillMaxWidth()
                        .pointerInput(Unit) {
                            detectTapGestures(
                                onLongPress = {
                                    onSelectionChanged(selectionX.multiToggle(movie.id))
                                },
                                onTap = {
                                    onSelectionChanged(selectionX.toggle(movie.id))
                                }
                            )
                        }
                ) {
                    Row(verticalAlignment = Alignment.CenterVertically) {
                        Image(
                            painter = painterResource(id = R.drawable.ic_baseline_movie_24),
                            colorFilter =
                                if (selection.multiSelectMode && movie.id in selection.selections) {
                                    ColorFilter.tint(MaterialTheme.colors.onPrimary)
                                } else {
                                    ColorFilter.tint(MaterialTheme.colors.onSurface)
                                },
                            contentDescription = stringResource(id = R.string.movie_icon),
                            modifier = Modifier
                                .padding(8.dp)
                                .size(48.dp)
                                .clickable {
                                    onSelectionChanged(selection.multiToggle(movie.id))
                                }
                        )
                        Text(
                            text = movie.title,
                            color =
                                if (selection.multiSelectMode && movie.id in selection.selections) {
                                    MaterialTheme.colors.onPrimary
                                } else {
                                    MaterialTheme.colors.onSurface
                                }
                            )
                    }
                }
            }
        }
    }
)


@Composable
fun MovieDisplayScreen(
    movie: Movie?,
    onEditMovie: () -> Unit
) = Scaffold(
    topBar = {
        TopAppBar(
            title = { Text(movie?.title ?: stringResource(id = R.string.nothing_selected)) },
            actions = {
                movie?.let {
                    EditMovieAction(onEditMovie = onEditMovie)
                }
            }
        )
    },
    content = {
        Column(
            modifier = Modifier.verticalScroll(rememberScrollState())
        ) {
            Text(
                text = stringResource(id = R.string.title),
                style = MaterialTheme.typography.h6,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(8.dp)
            )
            Text(
                text = movie?.title ?: "",
                style = MaterialTheme.typography.h5,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(start = 16.dp, end = 8.dp, top = 8.dp, bottom = 8.dp)
            )
            Text(
                text = stringResource(id = R.string.description),
                style = MaterialTheme.typography.h6,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(8.dp)
            )
            Text(
                text = movie?.description ?: "",
                style = MaterialTheme.typography.h5,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(start = 16.dp, end = 8.dp, top = 8.dp, bottom = 8.dp)
            )
        }
    }
)

@Composable
fun EditMovieAction(
    onEditMovie: () -> Unit
) {
    IconButton(onClick = onEditMovie) {
        Icon(Icons.Filled.Edit, contentDescription = stringResource(id = R.string.edit_movie))
    }
}

@Composable
fun CreateMovieAction(
    onCreateMovie: () -> Unit
) {
    IconButton(onClick = onCreateMovie) {
        Icon(Icons.Filled.Add, contentDescription = stringResource(id = R.string.create_movie))
    }
}

@Composable
fun DeleteMovieAction(
    onDeleteSelectedMovies: () -> Unit
) {
    IconButton(onClick = onDeleteSelectedMovies) {
        Icon(Icons.Filled.Delete, contentDescription = stringResource(id = R.string.delete_selected_movies))
    }
}

@Composable
fun MovieEditScreen(
    movie: Movie?,
    onMovieChanged: (Movie) -> Unit
) = Scaffold(
    topBar = {
        TopAppBar(
            title = {
                Text(
                    if (movie?.title.isNullOrEmpty()) {
                        stringResource(id = R.string.no_title)
                    } else
                        movie?.title ?: stringResource(id = R.string.nothing_selected)
                )
            }
        )
    },
    content = {
        var title by remember { mutableStateOf(movie?.title) }
        title = movie?.title
        var description by remember { mutableStateOf(movie?.description) }
        description = movie?.description
        Column(
            modifier = Modifier.verticalScroll(rememberScrollState())
        ) {
            TextField(
                value = title ?: "",
                label = { Text(text = stringResource(id = R.string.title)) },
                onValueChange = { value ->
                    movie?.let { movie ->
                        title = value
                        onMovieChanged(Movie(id = movie.id, title = title ?: "", description = description ?: ""))
                    }
                },
                modifier = Modifier
                    .padding(8.dp)
                    .fillMaxWidth()
            )
            TextField(
                value = description ?: "",
                label = { Text(text = stringResource(id = R.string.description)) },
                onValueChange = { value ->
                    movie?.let { movie ->
                        description = value
                        onMovieChanged(Movie(id = movie.id, title = title ?: "", description = description ?: ""))
                    }
                },
                modifier = Modifier
                    .padding(8.dp)
                    .fillMaxWidth()
            )
        }
    }
)

