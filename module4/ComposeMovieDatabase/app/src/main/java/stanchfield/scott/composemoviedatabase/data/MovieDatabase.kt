package stanchfield.scott.composemoviedatabase.data

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(version = 1, entities = [Movie::class], exportSchema = false)
abstract class MovieDatabase : RoomDatabase() {
    abstract val dao : MovieDao
}