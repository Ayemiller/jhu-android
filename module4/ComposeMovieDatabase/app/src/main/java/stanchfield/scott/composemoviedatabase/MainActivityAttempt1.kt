//package stanchfield.scott.composemoviedatabase
//
//import android.os.Bundle
//import androidx.activity.ComponentActivity
//import androidx.activity.compose.setContent
//import androidx.activity.viewModels
//import androidx.compose.foundation.Image
//import androidx.compose.foundation.clickable
//import androidx.compose.foundation.layout.Column
//import androidx.compose.foundation.layout.Row
//import androidx.compose.foundation.layout.fillMaxWidth
//import androidx.compose.foundation.layout.padding
//import androidx.compose.foundation.layout.size
//import androidx.compose.foundation.lazy.LazyColumn
//import androidx.compose.foundation.lazy.items
//import androidx.compose.foundation.shape.CornerSize
//import androidx.compose.foundation.shape.RoundedCornerShape
//import androidx.compose.material.Card
//import androidx.compose.material.Icon
//import androidx.compose.material.IconButton
//import androidx.compose.material.MaterialTheme
//import androidx.compose.material.Scaffold
//import androidx.compose.material.Surface
//import androidx.compose.material.Text
//import androidx.compose.material.TextField
//import androidx.compose.material.TopAppBar
//import androidx.compose.material.icons.Icons
//import androidx.compose.material.icons.filled.Add
//import androidx.compose.material.icons.filled.Edit
//import androidx.compose.runtime.Composable
//import androidx.compose.runtime.collectAsState
//import androidx.compose.runtime.getValue
//import androidx.compose.runtime.setValue
//import androidx.compose.runtime.mutableStateOf
//import androidx.compose.runtime.remember
//import androidx.compose.runtime.rememberCoroutineScope
//import androidx.compose.ui.Alignment
//import androidx.compose.ui.Modifier
//import androidx.compose.ui.graphics.ColorFilter
//import androidx.compose.ui.res.painterResource
//import androidx.compose.ui.res.stringResource
//import androidx.compose.ui.unit.dp
//import kotlinx.coroutines.flow.Flow
//import kotlinx.coroutines.launch
//import stanchfield.scott.composemoviedatabase.data.Movie
//import stanchfield.scott.composemoviedatabase.ui.theme.ComposeMovieDatabaseTheme
//
//
//class MainActivity : ComponentActivity() {
//    private val viewModel by viewModels<MovieViewModel>()
//
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        setContent {
//            ComposeMovieDatabaseTheme {
//                val scope = rememberCoroutineScope()
//
//                // A surface container using the 'background' color from the theme
//                Surface(color = MaterialTheme.colors.background) {
//                    Ui(
//                        allMoviesFlow = viewModel.allMovies,
//                        currentStateFlow = viewModel.currentState,
//                        selectedMovieFlow = viewModel.selectedMovie,
//                        onSelectionChanged = {
//                            viewModel.selectedMovie.value = it
//                        },
//                        onMovieChanged = {
//                            scope.launch {
//                                viewModel.onMovieChanged(it)
//                            }
//                        },
//                        pushState = viewModel::pushState,
//                        createMovie = {
//                            scope.launch {
//                                viewModel.createMovie()
//                            }
//                        }
//                    )
//                }
//            }
//        }
//    }
//
//    override fun onBackPressed() {
//        viewModel.popState()
//        if (viewModel.peekState() == null) {
//            finish()
//        }
//    }
//}
//
//@Composable
//fun Ui(
//    allMoviesFlow: Flow<List<Movie>>,
//    currentStateFlow: Flow<MovieState?>,
//    selectedMovieFlow: Flow<Movie?>,
//    onSelectionChanged: (Movie) -> Unit,
//    onMovieChanged: (Movie) -> Unit,
//    pushState: (MovieState) -> Unit,
//    createMovie: () -> Unit
//) {
//    val allMovies by allMoviesFlow.collectAsState(initial = emptyList())
//    val currentState by currentStateFlow.collectAsState(initial = MovieList)
//    val selectedMovie by selectedMovieFlow.collectAsState(initial = null)
//
//    when (currentState) {
//        MovieList ->
//            MovieListScreen(
//                movies = allMovies,
//                onSelectionChanged = {
//                    onSelectionChanged(it)
//                    pushState(MovieDisplay)
//                },
//                onCreateMovie = {
//                    createMovie()
//                    pushState(MovieEdit)
//                }
//            )
//        MovieDisplay ->
//            MovieDisplayScreen(
//                movie = selectedMovie,
//                onEditMovie = {
//                    pushState(MovieEdit)
//                }
//            )
//        MovieEdit ->
//            MovieEditScreen(
//                movie = selectedMovie,
//                onMovieChanged = onMovieChanged
//            )
//    }
//
//}
//
//@Composable
//fun MovieListScreen(
//    movies: List<Movie>,
//    onSelectionChanged: (Movie) -> Unit,
//    onCreateMovie: () -> Unit
//) = Scaffold(
//    topBar = {
//             TopAppBar(
//                 title = { Text(stringResource(id = R.string.movies)) },
//                 actions = {
//                     CreateMovieAction(onCreateMovie = onCreateMovie)
//                 }
//             )
//    },
//    content = {
//        LazyColumn() {
//            items(
//                items = movies
//            ) { movie ->
//                Card(
//                    elevation = 2.dp,
//                    backgroundColor = MaterialTheme.colors.surface,
//                    shape = RoundedCornerShape(corner = CornerSize(16.dp)),
//                    modifier = Modifier
//                        .padding(8.dp)
//                        .fillMaxWidth()
//                        .clickable {
//                            onSelectionChanged(movie)
//                        }
//                ) {
//                    Row(verticalAlignment = Alignment.CenterVertically) {
//                        Image(
//                            painter = painterResource(id = R.drawable.ic_baseline_movie_24),
//                            colorFilter = ColorFilter.tint(MaterialTheme.colors.onSurface),
//                            contentDescription = stringResource(id = R.string.movie_icon),
//                            modifier = Modifier
//                                .padding(8.dp)
//                                .size(48.dp)
//                        )
//                        Text(
//                            text = movie.title,
//                            color = MaterialTheme.colors.onSurface
//                        )
//                    }
//                }
//            }
//        }
//    }
//)
//
//
//@Composable
//fun MovieDisplayScreen(
//    movie: Movie?,
//    onEditMovie: () -> Unit
//) = Scaffold(
//    topBar = {
//        TopAppBar(
//            title = { Text(movie?.title ?: stringResource(id = R.string.nothing_selected)) },
//            actions = {
//                movie?.let {
//                    EditMovieAction(onEditMovie = onEditMovie)
//                }
//            }
//        )
//    },
//    content = {
//        Column {
//            Text(
//                text = stringResource(id = R.string.title),
//                style = MaterialTheme.typography.h6,
//                modifier = Modifier
//                    .fillMaxWidth()
//                    .padding(8.dp)
//            )
//            Text(
//                text = movie?.title ?: "",
//                style = MaterialTheme.typography.h5,
//                modifier = Modifier
//                    .fillMaxWidth()
//                    .padding(start = 16.dp, end = 8.dp, top = 8.dp, bottom = 8.dp)
//            )
//            Text(
//                text = stringResource(id = R.string.description),
//                style = MaterialTheme.typography.h6,
//                modifier = Modifier
//                    .fillMaxWidth()
//                    .padding(8.dp)
//            )
//            Text(
//                text = movie?.description ?: "",
//                style = MaterialTheme.typography.h5,
//                modifier = Modifier
//                    .fillMaxWidth()
//                    .padding(start = 16.dp, end = 8.dp, top = 8.dp, bottom = 8.dp)
//            )
//        }
//    }
//)
//
//@Composable
//fun EditMovieAction(
//    onEditMovie: () -> Unit
//) {
//    IconButton(onClick = onEditMovie) {
//        Icon(Icons.Filled.Edit, contentDescription = stringResource(id = R.string.edit_movie))
//    }
//}
//
//@Composable
//fun CreateMovieAction(
//    onCreateMovie: () -> Unit
//) {
//    IconButton(onClick = onCreateMovie) {
//        Icon(Icons.Filled.Add, contentDescription = stringResource(id = R.string.create_movie))
//    }
//}
//
//@Composable
//fun MovieEditScreen(
//    movie: Movie?,
//    onMovieChanged: (Movie) -> Unit
//) = Scaffold(
//    topBar = {
//        TopAppBar(
//            title = {
//                Text(
//                    if (movie?.title.isNullOrEmpty()) {
//                        stringResource(id = R.string.no_title)
//                    } else
//                        movie?.title ?: stringResource(id = R.string.nothing_selected)
//                )
//            }
//        )
//    },
//    content = {
//        var title by remember { mutableStateOf(movie?.title) }
//        var description by remember { mutableStateOf(movie?.description) }
//
//        Column {
//            TextField(
//                value = title ?: "",
//                label = { Text(text = stringResource(id = R.string.title)) },
//                onValueChange = { value ->
//                    movie?.let { movie ->
//                        title = value
//                        onMovieChanged(Movie(id = movie.id, title = title ?: "", description = description ?: ""))
//                    }
//                },
//                modifier = Modifier
//                    .padding(8.dp)
//                    .fillMaxWidth()
//            )
//            TextField(
//                value = description ?: "",
//                label = { Text(text = stringResource(id = R.string.description)) },
//                onValueChange = { value ->
//                    movie?.let { movie ->
//                        description = value
//                        onMovieChanged(Movie(id = movie.id, title = title ?: "", description = description ?: ""))
//                    }
//                },
//                modifier = Modifier
//                    .padding(8.dp)
//                    .fillMaxWidth()
//            )
//        }
//    }
//)
//
