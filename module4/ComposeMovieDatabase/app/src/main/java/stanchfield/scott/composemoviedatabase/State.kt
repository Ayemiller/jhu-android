package stanchfield.scott.composemoviedatabase

import android.util.Log
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

sealed class MovieState
object MovieList: MovieState()
object MovieDisplay: MovieState()
object MovieEdit: MovieState()

class StateMachine {
    private val initialState = MovieList
    private val currentState0 = MutableStateFlow<MovieState?>(initialState)
    private var stack = listOf<MovieState>(initialState)

    val currentState: StateFlow<MovieState?> = currentState0

    fun push(state: MovieState) {
        stack = stack + state
        currentState0.value = state
    }
    fun peek() = stack.lastOrNull()
    fun pop() {
        stack = stack.dropLast(1)
        currentState0.value = peek()
    }
}
