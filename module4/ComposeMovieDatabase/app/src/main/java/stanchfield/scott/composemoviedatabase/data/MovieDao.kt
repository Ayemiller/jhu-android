package stanchfield.scott.composemoviedatabase.data

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import kotlinx.coroutines.flow.Flow

@Dao
abstract class MovieDao {

    @Query("SELECT * FROM Movie WHERE id = :id")
    abstract fun getMovieAsync(id : String) : Flow<Movie>

    @Query("SELECT * FROM Movie ORDER BY title")
    abstract fun allMoviesAsync() : Flow<List<Movie>>

    @Insert
    abstract suspend fun insert(movie : Movie)

    @Update
    abstract fun update(movie : Movie)

    @Delete
    abstract suspend fun delete(movie : Movie)

    @Query("DELETE FROM Movie WHERE id = :id")
    abstract suspend fun deleteMovie(id : String)
}