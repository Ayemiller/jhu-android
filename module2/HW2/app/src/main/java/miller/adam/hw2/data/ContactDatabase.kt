package miller.adam.hw2.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

// code inspired by DatabaseSample::Database from Module 2
@Database(version = 1, entities = [Contact::class, Address::class], exportSchema = false)
abstract class ContactDatabase : RoomDatabase() {
    abstract fun dao(): ContactDao
}