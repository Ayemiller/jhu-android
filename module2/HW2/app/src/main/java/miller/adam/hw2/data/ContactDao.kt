package miller.adam.hw2.data

import androidx.room.*
import kotlinx.coroutines.flow.Flow

@Dao
// code inspired by DatabaseSample::MovieDao from Module 2
abstract class ContactDao {

    @Query("SELECT * FROM Contact WHERE id = :id")
    abstract fun getContactAsync(id: String): Flow<Contact>

    @Query("SELECT * FROM Address WHERE id = :id")
    abstract fun getAddressAsync(id: String): Flow<Address>

    @Query("SELECT * FROM Contact ORDER BY lastName,firstName")
    abstract fun allContactsAsync(): Flow<List<Contact>>

    @Query("SELECT * FROM Address ORDER BY state,city,street,type")
    abstract fun allAddressesAsync(): Flow<List<Address>>

    @Transaction
    @Query("SELECT * FROM Contact c WHERE c.id = :id")
    abstract fun contactByIdAsync(id: String): Flow<Contact>

    @Transaction
    @Query("SELECT * FROM Address a WHERE a.id = :id")
    abstract fun addressByIdAsync(id: String): Flow<Address>

    @Transaction
    @Query("SELECT * FROM Contact c WHERE c.id = :id")
    abstract fun contactAndAddresses(id: String): Flow<ContactAndAddresses>

    @Insert
    abstract suspend fun insert(contact: Contact)

    @Insert
    abstract suspend fun insert(address: Address)

    @Update
    abstract suspend fun update(contact: Contact)

    @Update
    abstract suspend fun update(address: Address)

    @Delete
    abstract suspend fun delete(contact: Contact)

    @Delete
    abstract suspend fun delete(address: Address)

    @Query("DELETE FROM Contact WHERE id = :id")
    abstract suspend fun deleteContact(id: String)

    @Query("DELETE FROM Address WHERE id = :id")
    abstract suspend fun deleteAddress(id: String)
}