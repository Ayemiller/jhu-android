package miller.adam.hw2.data

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity
class Contact(
    @PrimaryKey var id: String = UUID.randomUUID().toString(),
    var firstName: String,
    var lastName: String,
    var homePhone: String,
    var workPhone: String,
    var mobilePhone: String,
    var email: String
)