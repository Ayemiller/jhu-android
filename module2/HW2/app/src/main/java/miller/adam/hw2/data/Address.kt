package miller.adam.hw2.data

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import java.util.*

// code inspired by lecture from ::MovieViewModel from Module 2
@Entity(
    foreignKeys = [
        ForeignKey(
            entity = Contact::class,
            parentColumns = ["id"],
            childColumns = ["contactId"],
            onUpdate = ForeignKey.CASCADE,
            onDelete = ForeignKey.CASCADE
        )
    ]
)
data class Address(
    @PrimaryKey var id: String = UUID.randomUUID().toString(),
    var contactId: String,
    var type: String,
    var street: String,
    var city: String,
    var state: String,
    var zip: String
) {

    /**
     * Provides formatted toString method for [Address]
     */
    override fun toString(): String =
        "$type Address: $street $city, $state $zip"
}
