package miller.adam.hw2

import android.os.Bundle
import android.widget.Button
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import miller.adam.hw2.data.Contact
import miller.adam.hw2.databinding.ActivityMainBinding

// borrowed significantly from DatabaseSample::MainActivity from Module 2
class MainActivity : AppCompatActivity() {
    private var addedContactNumber = 1
    private lateinit var binding: ActivityMainBinding       // the view binding, gives us fields for layout views
    private val viewModel by viewModels<ContactViewModel>()   // the view model - holds data across configuration changes

    @ExperimentalCoroutinesApi // because we call experimental functions in the view model
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.toolbar)
        // when a new contacts list is emitted from the database, update the namesOfAllContacts text view
        viewModel.allContacts.collectWithLifecycle { contacts ->
            withContext(Dispatchers.Main) { // UI changes can only happen on the Main thread
                binding.content.namesOfAllContacts.text = contacts.joinToString {
                    listOfNotNull(
                        it.firstName,
                        it.lastName
                    ).joinToString(" ")
                }
            }
        }

        // when a new address list is emitted from the database, update the all_addresses text view
        viewModel.allAddresses.collectWithLifecycle { addresses ->
            withContext(Dispatchers.Main) { // UI changes can only happen on the Main thread
                binding.content.streetsOfAllAddress.text = addresses.joinToString { it.street }
            }
        }

        // when a new selected address is emitted from the view model, update the address name text view
        viewModel.selectedAddress.collectWithLifecycle { address ->
            withContext(Dispatchers.Main) { // UI changes can only happen on the Main thread
                binding.content.selectedAddressType.text =
                    if (address != null) "Id: ${address.id} ContactId: ${address.contactId} Type: ${address.type}" else ""
                binding.content.selectedAddress.text =
                    if (address != null) "${address.street} ${address.city}, ${address.state} ${address.zip}" else ""

            }
        }

        // when a new selected contact is emitted from the view model, update the contact's addresses
        viewModel.selectedContact.collectWithLifecycle { contactAndAddresses ->
            withContext(Dispatchers.Main) { // UI changes can only happen on the Main thread
                binding.content.selectedContactId.text = contactAndAddresses?.contact?.id.orEmpty()
                binding.content.selectedContactName.text = listOfNotNull(
                    contactAndAddresses?.contact?.firstName,
                    contactAndAddresses?.contact?.lastName
                ).joinToString(" ")
                binding.content.selectedContactPhoneNumbers.text = listOfNotNull(
                    contactAndAddresses?.contact?.homePhone,
                    contactAndAddresses?.contact?.workPhone,
                    contactAndAddresses?.contact?.mobilePhone
                ).joinToString("\n")
                binding.content.selectedContactEmail.text =
                    contactAndAddresses?.contact?.email.orEmpty()
                binding.content.selectedContactAddresses.text =
                    contactAndAddresses?.addresses?.joinToString("\n")
            }
        }

        // when "+ Contact" is clicked, add a contact to the database
        binding.content.addContactButton.launchOnClick {
            viewModel.insert(
                Contact(
                    firstName = "fName$addedContactNumber",
                    lastName = "lName$addedContactNumber",
                    homePhone = "111-222-3333",
                    workPhone = "444-555-6666",
                    mobilePhone = "777-888-9999",
                    email = "contact$addedContactNumber@mail.com"
                )
            )
            addedContactNumber++
        }

        // when selection buttons clicked select the contact/address in the view model
        binding.content.selectContact1Button.selectContactOnClick("c1")
        binding.content.selectContact2Button.selectContactOnClick("c2")
        binding.content.selectAddress1Button.selectAddressOnClick("a1")
        binding.content.selectAddress2Button.selectAddressOnClick("a2")

        // when deletion buttons clicked, delete the contact/address in the view model
        binding.content.deleteContact1Button.deleteContactOnClick("c1")
        binding.content.deleteAddress1Button.deleteAddressOnClick("a1")
    }

    private fun <T> Flow<T>.collectWithLifecycle(block: suspend (T) -> Unit) =
        lifecycleScope.launch(Dispatchers.IO) {
            lifecycle.repeatOnLifecycle(Lifecycle.State.STARTED) {
                collect {
                    block(it)
                }
            }
        }

    // a helper function that launches a coroutine on button click. the coroutine will be canceled
    //   when this activity is destroyed
    private fun Button.launchOnClick(block: suspend () -> Unit) {
        setOnClickListener {
            lifecycleScope.launch(Dispatchers.IO) {
                block()
            }
        }
    }

    // a helper function that selects a contact when a button is clicked
    private fun Button.selectContactOnClick(id: String) = setOnClickListener {
        viewModel.selectedContactId.value = id
    }

    // a helper function that selects an address when a button is clicked
    private fun Button.selectAddressOnClick(id: String) = setOnClickListener {
        viewModel.selectedAddressId.value = id
    }

    // a helper function that deletes a contact when a button is clicked
    private fun Button.deleteContactOnClick(id: String) = launchOnClick {
        viewModel.deleteContact(id)
    }

    // a helper function that deletes an address when a button is clicked
    private fun Button.deleteAddressOnClick(id: String) = launchOnClick {
        viewModel.deleteAddress(id)
    }

}