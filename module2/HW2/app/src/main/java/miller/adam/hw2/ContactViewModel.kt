package miller.adam.hw2

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.room.Room
import androidx.room.Room.databaseBuilder
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flow
import miller.adam.hw2.data.Address
import miller.adam.hw2.data.Contact
import miller.adam.hw2.data.ContactAndAddresses
import miller.adam.hw2.data.ContactDatabase

// code inspired by DatabaseSample::MovieViewModel from Module 2
class ContactViewModel(application: Application) : AndroidViewModel(application) {

    // ref to the database
    private val db = databaseBuilder(application, ContactDatabase::class.java, "MOVIES")
        .addCallback(object : RoomDatabase.Callback() {
            override fun onCreate(db: SupportSQLiteDatabase) {
                super.onCreate(db)
                db.execSQL("INSERT INTO Contact (id, firstName, lastName, homePhone, workPhone, mobilePhone, email) VALUES('c1', 'Jane', 'Doe', '123-456-7890', 'x3933', '555-555-1234', 'jane.doe@mail.com')")
                db.execSQL("INSERT INTO Contact (id, firstName, lastName, homePhone, workPhone, mobilePhone, email) VALUES('c2', 'John', 'Doe', '789-812-3456', 'x9399', '444-444-1234', 'john.doe@mail.com')")
                db.execSQL("INSERT INTO Contact (id, firstName, lastName, homePhone, workPhone, mobilePhone, email) VALUES('c3', 'Jake', 'Doe', '890-123-4567', 'x9909', '554-454-1234', 'jake.doe@mail.com')")

                db.execSQL("INSERT INTO Address (id, contactId, type, street, city, state, zip) VALUES('a1', 'c1', 'home', '11 Walnut Street', 'Baltimore', 'Maryland', '21204')")
                db.execSQL("INSERT INTO Address (id, contactId, type, street, city, state, zip) VALUES('a2', 'c1', 'home', '22 Walnut Street', 'Baltimore', 'Maryland', '21205')")
                db.execSQL("INSERT INTO Address (id, contactId, type, street, city, state, zip) VALUES('a3', 'c2', 'home', '33 Fir Street', 'Baltimore', 'Maryland', '21206')")
                db.execSQL("INSERT INTO Address (id, contactId, type, street, city, state, zip) VALUES('a4', 'c2', 'home', '44 Chestnut Street', 'Baltimore', 'Maryland', '21207')")
                db.execSQL("INSERT INTO Address (id, contactId, type, street, city, state, zip) VALUES('a5', 'c3', 'home', '55 Elm Street', 'Baltimore', 'Maryland', '21208')")
                db.execSQL("INSERT INTO Address (id, contactId, type, street, city, state, zip) VALUES('a6', 'c3', 'home', '22 H Street', 'Baltimore', 'Maryland', '21209')")
            }
        })
        .build()

    // functions to return the contacts and addresses from the database
    val allContacts = db.dao().allContactsAsync()
    val allAddresses = db.dao().allAddressesAsync()

    // currently selected contactId and addressId in the UI
    val selectedContactId = MutableStateFlow<String?>(null)
    val selectedAddressId = MutableStateFlow<String?>(null)

    /**
     * When the [ContactViewModel.selectedAddressId] changes, get the [Address] object from the dao
     */
    @ExperimentalCoroutinesApi
    val selectedAddress: Flow<Address?>
        get() =
            selectedAddressId.flatMapLatest { addressId ->
                addressId?.let {
                    db.dao().getAddressAsync(it)
                } ?: flow { emit(null) }
            }

    /**
     * When [ContactViewModel.selectedContactId] changes, update the [ContactAndAddresses] object from the dao
     */
    @ExperimentalCoroutinesApi
    val selectedContact: Flow<ContactAndAddresses?>
        get() =
            selectedContactId.flatMapLatest { contactAndAddress ->
                contactAndAddress?.let {
                    db.dao().contactAndAddresses(it)
                } ?: flow { emit(null) }
            }


    /**
     * Delete the [Contact] for the provided [Contact.id] from the database
     */
    suspend fun deleteContact(id: String) {
        db.dao().deleteContact(id)
    }

    /**
     * Delete the [Address] for the provided [Address.id] from the database
     */
    suspend fun deleteAddress(id: String) {
        db.dao().deleteAddress(id)
    }

    /**
     * Insert the provided [Contact] into the database
     */
    suspend fun insert(contact: Contact) = db.dao().insert(contact)

    /**
     * Insert the provided [Address] into the database
     */
    suspend fun insert(address: Address) = db.dao().insert(address)

    /**
     * Update the provided [Contact] in the database
     */
    suspend fun update(contact: Contact) = db.dao().update(contact)

    /**
     * Update the provided [Address] in the database
     */
    suspend fun update(address: Address) = db.dao().update(address)

}