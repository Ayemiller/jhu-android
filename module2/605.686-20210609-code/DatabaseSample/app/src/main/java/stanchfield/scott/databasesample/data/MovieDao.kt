package stanchfield.scott.databasesample.data

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Transaction
import androidx.room.Update
import kotlinx.coroutines.flow.Flow

@Dao
abstract class MovieDao {
//    @Query("SELECT * FROM Movie ORDER BY title")
//    abstract fun allMovies() : List<Movie>
//
//    @Query("SELECT * FROM Actor ORDER BY name")
//    abstract fun allActors() : List<Actor>

    @Query("SELECT * FROM Actor WHERE id = :id")
    abstract fun getActorAsync(id : String) : Flow<Actor>

    @Query("SELECT * FROM Movie WHERE id = :id")
    abstract fun getMovieAsync(id : String) : Flow<Movie>

    @Query("SELECT * FROM Movie ORDER BY title")
    abstract fun allMoviesAsync() : Flow<List<Movie>>

    @Query("SELECT * FROM Actor ORDER BY name")
    abstract fun allActorsAsync() : Flow<List<Actor>>

    @Query("SELECT a.id, a.name, r.roleName, r.`order` FROM Actor a, Role r WHERE r.movieId = :movieId AND r.actorId = a.id ORDER BY r.`order`")
    abstract fun rolesForMovieAsync(movieId : String) : Flow<List<RoleInfo>>

    @Transaction @Query("SELECT * FROM Role WHERE movieId = :movieId ORDER BY `order`")
    abstract fun rolesForMovieAsync2(movieId : String) : Flow<List<RoleInfo2>>

    @Query("SELECT m.id, m.title, m.description FROM Movie m, Role r WHERE m.id = r.movieId AND r.actorId = :actorId")
    abstract fun moviesForActorAsync(actorId : String) : Flow<List<Movie>>

    @Insert
    abstract suspend fun insert(movie : Movie)
    @Insert
    abstract suspend fun insert(actor : Actor)
    @Insert
    abstract suspend fun insert(role : Role)

    @Update
    abstract suspend fun update(movie : Movie)
    @Update
    abstract suspend fun update(actor : Actor)
    @Update
    abstract suspend fun update(role : Role)

    @Delete
    abstract suspend fun delete(movie : Movie)
    @Delete
    abstract suspend fun delete(actor : Actor)
    @Delete
    abstract suspend fun delete(role : Role)

    @Query("DELETE FROM Movie WHERE id = :id")
    abstract suspend fun deleteMovie(id : String)
    @Query("DELETE FROM Actor WHERE id = :id")
    abstract suspend fun deleteActor(id : String)
}