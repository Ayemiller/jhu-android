package stanchfield.scott.databasesample.data

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.UUID

@Entity
class Movie(
    @PrimaryKey var id: String = UUID.randomUUID().toString(),
    var title : String,
    var description : String
)