package stanchfield.scott.databasesample

import android.os.Bundle
import android.widget.Button
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import stanchfield.scott.databasesample.data.Movie
import stanchfield.scott.databasesample.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding       // the view binding, gives us fields for layout views
    private val viewModel by viewModels<MovieViewModel>()   // the view model - holds data across configuration changes
    private var movieNum = 10

    // a helper function that wraps a function that will process elements emitted in a flow
    //   (this is the result of factoring out duplicated code)
    // this function does the following
    //   - it's an extension function on Flow<T> (you can call it as someFlow.collectWithLifecycle {...})
    //   - it launches a coroutine in the lifecycle scope; the coroutine will be canceled when
    //     this activity is destroyed
    //   - starts collection on the flow when we enter STARTED state (after onStart() is called)
    //   - stops collection on the flow when we leave STARTED state
    //   - will keep repeating that collection start/stop as often as we change in and out of STARTED
    //     state until the coroutine is canceled
    //   - whenever the flow emits a new element, we call the passed-in block on it
    private fun <T> Flow<T>.collectWithLifecycle(block: suspend (T) -> Unit) =
        lifecycleScope.launch(Dispatchers.IO) {
            lifecycle.repeatOnLifecycle(Lifecycle.State.STARTED) {
                collect {
                    block(it)
                }
            }
        }

    // a helper function that launches a coroutine on button click. the coroutine will be canceled
    //   when this activity is destroyed
    private fun Button.launchOnClick(block: suspend () -> Unit) {
        setOnClickListener {
            lifecycleScope.launch(Dispatchers.IO) {
                block()
            }
        }
    }

    // a helper function that selects a movie when a button is clicked
    private fun Button.selectMovieOnClick(id: String) = setOnClickListener {
        viewModel.selectedMovieId.value = id
    }
    // a helper function that selects an actor when a button is clicked
    private fun Button.selectActorOnClick(id: String) = setOnClickListener {
        viewModel.selectedActorId.value = id
    }

    // a helper function that deletes a movie when a button is clicked
    private fun Button.deleteMovieOnClick(id: String) = launchOnClick {
        viewModel.deleteMovie(id)
    }
    // a helper function that deletes an actor when a button is clicked
    private fun Button.deleteActorOnClick(id: String) = launchOnClick {
        viewModel.deleteActor(id)
    }

    @ExperimentalCoroutinesApi // because we call experimental functions in the view model
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.toolbar)

        // when a new movie list is emitted from the database, update the all_movies text view
        viewModel.allMovies.collectWithLifecycle { movies ->
            withContext(Dispatchers.Main) { // UI changes can only happen on the Main thread
                binding.content.allMovies.text = movies.joinToString { it.title }
            }
        }

        // when a new actor list is emitted from the database, update the all_actors text view
        viewModel.allActors.collectWithLifecycle { actors ->
            withContext(Dispatchers.Main) { // UI changes can only happen on the Main thread
                binding.content.allActors.text = actors.joinToString { it.name }
            }
        }

        // when a new selected movie is emitted from the view model, update the movie info text views
        viewModel.selectedMovie.collectWithLifecycle { movie ->
            withContext(Dispatchers.Main) { // UI changes can only happen on the Main thread
                binding.content.movieTitle.text = movie?.title ?: ""
                binding.content.movieDescription.text = movie?.description ?: ""
            }
        }

        // when a new selected actor is emitted from the view model, update the actor name text view
        viewModel.selectedActor.collectWithLifecycle { actor ->
            withContext(Dispatchers.Main) { // UI changes can only happen on the Main thread
                binding.content.actorName.text = actor?.name ?: ""
            }
        }

        // when a new cast list is emitted from the view model, update the cast section of the UI
        viewModel.cast.collectWithLifecycle { cast ->
            if (cast != null) {
                withContext(Dispatchers.Main) { // UI changes can only happen on the Main thread
                    binding.content.cast.text = cast.joinToString("\n") { "${it.roleName}: ${it.actor.name}" }
                }
            }
        }
//        viewModel.cast2.collectWithLifecycle { cast ->
//            withContext(Dispatchers.Main) {
//                binding.content.cast.text = cast?.joinToString("\n") { "${it.roleName}: ${it.actor.name}" } ?: ""
//            }
//        }

        // when a new filmography list is emitted from the view model, update the filmography section of the UI
        viewModel.filmography.collectWithLifecycle { filmography ->
            withContext(Dispatchers.Main) { // UI changes can only happen on the Main thread
                binding.content.filmography.text = filmography?.joinToString("\n") { it.title } ?: ""
            }
        }

        // when "+ Movie" is clicked, add a movie to the database
        binding.content.addMovieButton.launchOnClick {
            viewModel.insert(Movie("m$movieNum", "Movie $movieNum", "Some new movie #$movieNum"))
            movieNum++
        }

        // when selection buttons clicked select the movie/actor in the view model
        binding.content.selectMovie1Button.selectMovieOnClick("m1")
        binding.content.selectMovie2Button.selectMovieOnClick("m2")
        binding.content.selectActor1Button.selectActorOnClick("a1")
        binding.content.selectActor2Button.selectActorOnClick("a2")

        // when deletion buttons clicked, delete the movie/actor in the view model
        binding.content.deleteMovie1Button.deleteMovieOnClick("m1")
        binding.content.deleteMovie2Button.deleteMovieOnClick("m2")
        binding.content.deleteMovie3Button.deleteMovieOnClick("m3")
        binding.content.deleteActor1Button.deleteActorOnClick("a1")
        binding.content.deleteActor2Button.deleteActorOnClick("a2")
    }
}