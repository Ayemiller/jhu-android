package stanchfield.scott.databasesample

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flow
import stanchfield.scott.databasesample.data.Actor
import stanchfield.scott.databasesample.data.Database
import stanchfield.scott.databasesample.data.Movie
import stanchfield.scott.databasesample.data.RoleInfo
import stanchfield.scott.databasesample.data.RoleInfo2

class MovieViewModel(application: Application) : AndroidViewModel(application) {
    private val db = Room.databaseBuilder(application, Database::class.java, "MOVIES")
        .addCallback(object : RoomDatabase.Callback() {
            override fun onCreate(db: SupportSQLiteDatabase) {
                super.onCreate(db)

                db.execSQL("INSERT INTO Movie (id, title, description) VALUES('m1', 'The Transporter', 'Jason Statham kicks a guy in the face')")
                db.execSQL("INSERT INTO Movie (id, title, description) VALUES('m2', 'Transporter 2', 'Jason Statham kicks a bunch of guys in the face')")
                db.execSQL("INSERT INTO Movie (id, title, description) VALUES('m3', 'Hobbs and Shaw', 'Cars, Explosions and Stuff')")
                db.execSQL("INSERT INTO Movie (id, title, description) VALUES('m4', 'Jumanji', 'The Rock smolders')")


                db.execSQL("INSERT INTO Actor (id, name) VALUES('a1', 'Jason Statham')")
                db.execSQL("INSERT INTO Actor (id, name) VALUES('a2', 'The Rock')")
                db.execSQL("INSERT INTO Actor (id, name) VALUES('a3', 'Shu Qi')")
                db.execSQL("INSERT INTO Actor (id, name) VALUES('a4', 'Amber Valletta')")
                db.execSQL("INSERT INTO Actor (id, name) VALUES('a5', 'Kevin Hart')")


                db.execSQL("INSERT INTO Role (movieId, actorId, roleName, `order`) VALUES('m1', 'a1', 'Frank Martin', 1)")
                db.execSQL("INSERT INTO Role (movieId, actorId, roleName, `order`) VALUES('m1', 'a3', 'Lai', 2)")
                db.execSQL("INSERT INTO Role (movieId, actorId, roleName, `order`) VALUES('m2', 'a1', 'Frank Martin', 1)")
                db.execSQL("INSERT INTO Role (movieId, actorId, roleName, `order`) VALUES('m2', 'a4', 'Audrey Billings', 2)")
                db.execSQL("INSERT INTO Role (movieId, actorId, roleName, `order`) VALUES('m3', 'a2', 'Hobbs', 1)")
                db.execSQL("INSERT INTO Role (movieId, actorId, roleName, `order`) VALUES('m3', 'a1', 'Shaw', 2)")
                db.execSQL("INSERT INTO Role (movieId, actorId, roleName, `order`) VALUES('m4', 'a2', 'Spencer', 1)")
                db.execSQL("INSERT INTO Role (movieId, actorId, roleName, `order`) VALUES('m4', 'a5', 'Fridge', 2)")
            }
        })
        .build()

    // return Flow<List<Movie>> and Flow<List<Actor>> from the view model (which gets them from the database
    // exposed as functions to be consistent with the functions defined below that
    val allMovies = db.dao.allMoviesAsync()
    val allActors = db.dao.allActorsAsync()

    // keeps track of the current selection
    val selectedMovieId = MutableStateFlow<String?>(null)
    val selectedActorId = MutableStateFlow<String?>(null)

    // Flow transformations - watches for the latest value and transforms it into a new flow
    //   that's collected by the UI. This allows us to run another query when we see a parameter
    //   change; for example, when selectedMovieId changes, we run a query to fetch the actual Movie
    //   object. This could be done using just a map operator, but the result would only be fetched
    //   once, and if data in the database changes, we wouldn't update the UI.
    // Using flatMapLatest, we run a query to return a Flow that's collected automatically; if any
    //   of the resulting data in the flow changes, a new list is emitted to be collected by the UI
    // (flatMapLatest is experimental so we need to add the annotation to tell the compiler we're
    //   ok with using experimental code)

    // Note - we could factor out some duplication as follows, but I wanted to show you the explicit
    //   code to make it a little more clear what's going on
    //
    //    @ExperimentalCoroutinesApi
    //    fun <T> Flow<String?>.fetchFlow(query: (String) -> Flow<T>) =
    //            flatMapLatest { id ->
    //                id?.let {
    //                    db.dao.getMovieAsync(it)
    //                } ?: flow { emit(null) }
    //            }
    //
    //    @ExperimentalCoroutinesApi
    //    val selectedMovie: Flow<Movie?>
    //        get() =
    //            selectedMovieId.fetchFlow {
    //                db.dao.getMovieAsync(it)
    //            }

    // when the selected movie id changes, fetch an actual movie object
    @ExperimentalCoroutinesApi
    val selectedMovie: Flow<Movie?>
        get() =
            selectedMovieId.flatMapLatest { movieId ->
                movieId?.let {
                    db.dao.getMovieAsync(it)
                } ?: flow { emit(null) }
            }

    // when the selected actor id changes, fetch an actual actor object
    @ExperimentalCoroutinesApi
    val selectedActor: Flow<Actor?>
        get() =
            selectedActorId.flatMapLatest { actorId ->
                actorId?.let {
                    db.dao.getActorAsync(actorId)
                } ?: flow { emit(null) }
            }

    // when the selected movie id changes, fetch the roles for that movie
    @ExperimentalCoroutinesApi
    val cast: Flow<List<RoleInfo>?>
        get() =
            selectedMovieId.flatMapLatest { movie ->
                movie?.let {
                    db.dao.rolesForMovieAsync(it)
                } ?: flow { emit(null) }
            }

    // when the selected movie id changes, fetch the roles for that movie
    @ExperimentalCoroutinesApi
    val cast2: Flow<List<RoleInfo2>?>
        get() =
            selectedMovieId.flatMapLatest { movieId ->
                movieId?.let {
                    db.dao.rolesForMovieAsync2(it)
                } ?: flow { emit(null) }
            }

    // when the selected actor id changes, fetch the filmography for that movie
    @ExperimentalCoroutinesApi
    val filmography: Flow<List<Movie>?>
        get() =
            selectedActorId.flatMapLatest { actorId ->
                actorId?.let {
                    db.dao.moviesForActorAsync(it)
                } ?: flow { emit(null) }
            }

    // pass through the delete and insert calls to the viewmodel
    suspend fun deleteActor(id: String) {
        db.dao.deleteActor(id)
    }
    suspend fun deleteMovie(id: String) {
        db.dao.deleteMovie(id)
    }
    suspend fun insert(movie: Movie) = db.dao.insert(movie)
}